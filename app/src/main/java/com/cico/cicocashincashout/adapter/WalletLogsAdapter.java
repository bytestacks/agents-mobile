package com.cico.cicocashincashout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletlogsDatum;
import com.cico.cicocashincashout.utils.Utility;

import java.util.List;

public class WalletLogsAdapter extends RecyclerView.Adapter<WalletLogsAdapter.MyViewHolder> {

    private List<WalletlogsDatum> walletlogsDatumList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text_trans_type, text_trans_date, text_trans_amount,text_tran_ref;

        public MyViewHolder(View view) {
            super(view);
            text_trans_type = (TextView) view.findViewById(R.id.text_trans_type);
            text_trans_amount = (TextView) view.findViewById(R.id.text_trans_amount);
            text_trans_date = (TextView) view.findViewById(R.id.text_trans_date);
            text_tran_ref = (TextView) view.findViewById(R.id.text_tran_ref);
        }
    }


    public WalletLogsAdapter(List<WalletlogsDatum> walletlogsDatumList, Context context) {
        this.walletlogsDatumList = walletlogsDatumList;
        this.context = context;
    }

    @Override
    public WalletLogsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_trans_history_items, parent, false);

        return new WalletLogsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WalletLogsAdapter.MyViewHolder holder, int position) {
        WalletlogsDatum movie = walletlogsDatumList.get(position);
        String wallet_topup = movie.getType();
        if(wallet_topup.equalsIgnoreCase("credit")){
            holder.text_trans_amount.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_up_black_green_24dp,0);
            holder.text_trans_amount.setTextColor(context.getResources().getColor(R.color.cico_green));
        }else{
            holder.text_trans_amount.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_down_black_24dp,0);
            holder.text_trans_amount.setTextColor(context.getResources().getColor(R.color.cico_red));
        }
        holder.text_trans_type.setText(movie.getDescription());
        holder.text_trans_amount.setText(Utility.formatCurrency(Double.parseDouble(movie.getAmount().toString())));
        holder.text_trans_date.setText(movie.getCreatedAt());
        holder.text_tran_ref.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return walletlogsDatumList.size();
    }

}
