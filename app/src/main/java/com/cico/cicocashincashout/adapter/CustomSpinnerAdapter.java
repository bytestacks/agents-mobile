package com.cico.cicocashincashout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cico.cicocashincashout.R;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    String[] spinnerTitles;
    Context mContext;

    public CustomSpinnerAdapter(@NonNull Context context, String[] titles) {
        super(context, R.layout.spiner_images_item);
        this.spinnerTitles = titles;
        this.mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return spinnerTitles.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.spiner_images_item, parent, false);
            mViewHolder.mPopulation = (TextView) convertView.findViewById(R.id.tvPopulation);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mPopulation.setText(spinnerTitles[position]);

        return convertView;
    }

    private static class ViewHolder {
        TextView mPopulation;
    }
}
