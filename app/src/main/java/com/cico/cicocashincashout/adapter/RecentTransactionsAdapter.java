package com.cico.cicocashincashout.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.AgentInfo.response.Transaction;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Utility;

import java.util.List;

public class RecentTransactionsAdapter extends RecyclerView.Adapter<RecentTransactionsAdapter.MyViewHolder>  {

    private List<Transaction> moviesList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text_trans_type, text_trans_date, text_trans_amount,text_tran_ref;
        public ConstraintLayout trans_hist;

        public MyViewHolder(View view) {
            super(view);
            text_trans_type = (TextView) view.findViewById(R.id.text_trans_type);
            text_trans_amount = (TextView) view.findViewById(R.id.text_trans_amount);
            text_trans_date = (TextView) view.findViewById(R.id.text_trans_date);
            text_tran_ref = (TextView) view.findViewById(R.id.text_tran_ref);
            trans_hist = (ConstraintLayout) view.findViewById(R.id.trans_hist);
        }
    }


    public RecentTransactionsAdapter(List<Transaction> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public RecentTransactionsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_trans_history_items, parent, false);

        return new RecentTransactionsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecentTransactionsAdapter.MyViewHolder holder, int position) {
        Transaction movie = moviesList.get(position);
        //String wallet_topup = movie.getTransactionType();
       /* if(wallet_topup.equalsIgnoreCase("Wallet Topup")){
            holder.text_trans_amount.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_up_black_green_24dp,0);
            holder.text_trans_amount.setTextColor(context.getResources().getColor(R.color.cico_green));
        }else{*/
        holder.text_trans_amount.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_down_black_24dp,0);
        if(movie.getStatus().equalsIgnoreCase("paid") || movie.getStatus().equalsIgnoreCase("success")){
            holder.text_trans_amount.setTextColor(context.getResources().getColor(R.color.cico_green));
        }else{
            holder.text_trans_amount.setTextColor(context.getResources().getColor(R.color.cico_red));
        }
        //}
        holder.text_trans_type.setText(movie.getType());
        holder.text_trans_amount.setText(Utility.formatCurrency(Double.parseDouble(String.valueOf(movie.getAmount()))));
        holder.text_trans_date.setText(movie.getCreatedAt());
        holder.text_tran_ref.setText(movie.getReference());
        holder.trans_hist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("result",movie.getStatus());
                bundle.putString("tran_type",movie.getType());
                bundle.putString("reference", movie.getReference());
                bundle.putString("amount",String.valueOf(movie.getAmount()));
                new AppNavigator((Activity) context).navigateToSuccess(bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
