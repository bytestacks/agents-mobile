package com.cico.cicocashincashout.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import com.cico.cicocashincashout.cache.FontCache;

public class ButtomMedium extends AppCompatButton {


    public ButtomMedium(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public ButtomMedium(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public ButtomMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("font/Montserrat-Medium.ttf", context);
        setTypeface(customFont);
    }
}
