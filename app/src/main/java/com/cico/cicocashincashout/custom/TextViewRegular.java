package com.cico.cicocashincashout.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.cico.cicocashincashout.cache.FontCache;

public class TextViewRegular extends AppCompatTextView {

    public TextViewRegular(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("font/Montserrat-Regular.ttf", context);
        setTypeface(customFont);
    }
}
