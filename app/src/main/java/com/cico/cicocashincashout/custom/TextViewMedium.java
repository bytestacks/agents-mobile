package com.cico.cicocashincashout.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.cico.cicocashincashout.cache.FontCache;

public class TextViewMedium extends AppCompatTextView {

    public TextViewMedium(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public TextViewMedium(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public TextViewMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("font/Montserrat-Medium.ttf", context);
        setTypeface(customFont);
    }
}
