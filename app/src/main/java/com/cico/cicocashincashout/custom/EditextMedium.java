package com.cico.cicocashincashout.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class EditextMedium extends AppCompatEditText {

    private Context context;
    private AttributeSet attrs;
    private int defStyle;

    public EditextMedium(Context context) {
        super(context);
        this.context=context;
        init();
    }

    public EditextMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        init();
    }

    public EditextMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context=context;
        this.attrs=attrs;
        this.defStyle=defStyle;
        init();
    }

    private void init() {
        Typeface font=Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Medium.ttf");
        this.setTypeface(font);
    }
    @Override
    public void setTypeface(Typeface tf, int style) {
        tf=Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Medium.ttf");
        super.setTypeface(tf, style);
    }

    @Override
    public void setTypeface(Typeface tf) {
        tf=Typeface.createFromAsset(getContext().getAssets(), "font/Montserrat-Medium.ttf");
        super.setTypeface(tf);
    }
}
