package com.cico.cicocashincashout.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import com.cico.cicocashincashout.cache.FontCache;

public class ButtonLight extends AppCompatButton {


    public ButtonLight(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public ButtonLight(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public ButtonLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("font/Montserrat-Light.ttf", context);
        setTypeface(customFont);
    }
}
