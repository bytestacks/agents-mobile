package com.cico.cicocashincashout.fragment.transaction_history;

import android.content.Context;

import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;

public interface TransactionHistoryContract {


    interface TransactionHistoryView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void loadTransactionHistory(TransactionHistoryResponseModel transactionHistoryResponseModel);
    }

    interface TransactionHistoryListener{

        void onSuccess(Context context, TransactionHistoryResponseModel transactionHistoryResponseModel);

        void onFailure(String message);
    }
}
