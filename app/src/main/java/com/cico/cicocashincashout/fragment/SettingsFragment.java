package com.cico.cicocashincashout.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.cico.cicocashincashout.R;

public class SettingsFragment extends Fragment {

    private View prepareViews(LayoutInflater inflater, ViewGroup container) {

        final View mainView = inflater.inflate(R.layout.activity_settings, container, false);

        return mainView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = prepareViews(inflater, container);

        return rootView;
    }

}
