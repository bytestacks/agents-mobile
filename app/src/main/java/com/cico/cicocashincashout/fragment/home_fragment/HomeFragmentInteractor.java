package com.cico.cicocashincashout.fragment.home_fragment;


import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.AgentInfo.response.AgentInfoResponseModel;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeFragmentInteractor {

    private HomeFragmentContract.HomeFragmentListener homeFragmentListener;

    final String Home_Fragment = "trans_history";

    public HomeFragmentInteractor(HomeFragmentContract.HomeFragmentListener homeFragmentListener){

        this.homeFragmentListener = homeFragmentListener;
    }

    public void getInfo(Context context, String userToken,String agentUUIDCode) {

        transactionHistory(userToken,context,agentUUIDCode);
    }

    public void transactionHistory(String token, Context context,String agentUUIDCode) {

        transactionHistoryObserver(token,agentUUIDCode).subscribeWith(transactionHistoryObservable(context));
    }

    public Observable<AgentInfoResponseModel> transactionHistoryObserver(String token, String agentUUIDCode) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .agentInfo( "Token " + token,agentUUIDCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<AgentInfoResponseModel> transactionHistoryObservable(final Context context){
        return new DisposableObserver<AgentInfoResponseModel>() {
            @Override
            public void onNext(AgentInfoResponseModel transactionHistoryResponseModel) {

                Log.d(Home_Fragment, "DATA" + transactionHistoryResponseModel.getData());
                if(transactionHistoryResponseModel.getStatus().equalsIgnoreCase("Error")){
                    homeFragmentListener.onFailure(transactionHistoryResponseModel.getMessage());
                }else{
                    homeFragmentListener.onSuccess(context,transactionHistoryResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(Home_Fragment,"Error " + e);
                homeFragmentListener.onFailure(e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(Home_Fragment,"Completed");
            }
        };
    }
}
