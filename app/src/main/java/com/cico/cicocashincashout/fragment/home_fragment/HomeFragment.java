package com.cico.cicocashincashout.fragment.home_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.adapter.RecentTransactionsAdapter;
import com.cico.cicocashincashout.model.AgentInfo.response.AgentInfoResponseModel;
import com.cico.cicocashincashout.model.AgentInfo.response.Transaction;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

import java.util.List;

public class HomeFragment extends Fragment implements HomeFragmentContract.HomeFragmentView {

    TextView wallet_balance,fund_purse,transfer_acct,withdraw,
            transfer,bills,airtime,history,wallet_number;

    HomeFragmentPresenter homeFragmentPresenter;
    RecyclerView recent_transactions;
    RecentTransactionsAdapter recentTransactionsAdapter;
    List<Transaction> transactionList;
    LoginResponseModel loginResponseModel;
    private TextView empty_view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = prepareViews(inflater, container);

        wallet_balance = (TextView) rootView.findViewById(R.id.wallet_balance);
        wallet_balance.setText(Utility.formatCurrency(Double.parseDouble(Preferences.getAgentBalance(getContext()))));

        withdraw = (TextView) rootView.findViewById(R.id.withdraw);
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(getActivity()).navigateToCashwithdrawal();
            }
        });

        wallet_number = (TextView) rootView.findViewById(R.id.wallet_number);
        String walletId = "Wallet ID : " + Preferences.getWalletId(getContext());
        wallet_number.setText(walletId);

        transfer = (TextView) rootView.findViewById(R.id.transfer);
        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(getActivity()).navigateToFundTransfer();
            }
        });

        bills = (TextView) rootView.findViewById(R.id.bills);
        bills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(getActivity()).navigateToBillsPayment();
            }
        });

        airtime = (TextView) rootView.findViewById(R.id.airtime);
        airtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(getActivity()).navigateToAirtime();
            }
        });


        /*fund_purse = (TextView) rootView.findViewById(R.id.find_purse);
        fund_purse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(getActivity()).navigateToFundPurse();

            }
        });

        transfer_acct = (TextView) rootView.findViewById(R.id.transfer_acct);
        transfer_acct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(getActivity()).navigateToAgentTransferFund();


            }
        });*/

        empty_view = (TextView) rootView.findViewById(R.id.empty_view);
        recent_transactions = (RecyclerView) rootView.findViewById(R.id.recent_transactions);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recent_transactions.setLayoutManager(mLayoutManager);
        recent_transactions.setItemAnimator(new DefaultItemAnimator());

       // populateRecycler(transactionList);

        return rootView;
    }

    /*private void populateRecycler(List<Transaction> transactionsList){

        if(transactionsList.isEmpty()){
            empty_view.setVisibility(View.VISIBLE);
            empty_view.setText(getContext().getText(R.string.no_wallet_transaction));
            recent_transactions.setVisibility(View.GONE);
        }else{
            recentTransactionsAdapter = new RecentTransactionsAdapter(transactionsList, getContext());
            recent_transactions.setAdapter(recentTransactionsAdapter);
            recent_transactions.addItemDecoration(new DividerItemDecoration(recent_transactions.getContext(), DividerItemDecoration.VERTICAL));
        }

    }*/

    private View prepareViews(LayoutInflater inflater, ViewGroup container) {

        final View mainView = inflater.inflate(R.layout.fragment_home, container, false);

        homeFragmentPresenter = new HomeFragmentPresenter(this);
        String userToken = Preferences.getAgentToken(getContext());
        String agentUUIDCode = Preferences.getAgentUUIDCode(getContext());
        homeFragmentPresenter.getAgentInto(getContext(),userToken,agentUUIDCode);

        return mainView;
    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(getContext(),message);
    }

    @Override
    public void showProgress() {
        Utility.showRecentProgressDialog(getContext(),false);
    }

    @Override
    public void hideProgress() {
        Utility.hideRecentProgressDialog(getActivity());
    }

    @Override
    public void destroy() {
        homeFragmentPresenter.onDestroy();
    }

    @Override
    public void setRecyclerAdapter(AgentInfoResponseModel transactionHistoryResponseModel) {

        String walletBalance = transactionHistoryResponseModel.getData().getWallet().getCurrentBal();

        wallet_balance.setText(Utility.formatCurrency(Double.parseDouble(walletBalance)));

        List<Transaction> transactionHistoryDatumModelList = transactionHistoryResponseModel.getData().getTransaction();
        if(transactionHistoryDatumModelList.isEmpty()){
            empty_view.setVisibility(View.VISIBLE);
            empty_view.setText(getContext().getText(R.string.no_wallet_transaction));
            recent_transactions.setVisibility(View.GONE);
        }else{
            recentTransactionsAdapter = new RecentTransactionsAdapter(transactionHistoryDatumModelList, getContext());
            recent_transactions.setAdapter(recentTransactionsAdapter);
            recent_transactions.addItemDecoration(new DividerItemDecoration(recent_transactions.getContext(), DividerItemDecoration.VERTICAL));
        }

    }
}
