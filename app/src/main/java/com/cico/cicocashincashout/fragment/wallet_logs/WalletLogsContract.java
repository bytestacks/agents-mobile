package com.cico.cicocashincashout.fragment.wallet_logs;

import android.content.Context;

import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletLogResponse;

public interface WalletLogsContract {

    interface WalletLogView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void loadWalletLogs(WalletLogResponse walletLogResponse);
    }

    interface WalletLogListener{

        void onSuccess(Context context, WalletLogResponse walletLogResponse);

        void onFailure(String message);
    }
}
