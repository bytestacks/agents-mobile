package com.cico.cicocashincashout.fragment.home_fragment;

import android.content.Context;

import com.cico.cicocashincashout.model.AgentInfo.response.AgentInfoResponseModel;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;

public interface HomeFragmentContract {

    interface HomeFragmentView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void destroy();

        void setRecyclerAdapter(AgentInfoResponseModel transactionHistoryResponseModel);
    }

    interface HomeFragmentListener{

        void onSuccess(Context context, AgentInfoResponseModel transactionHistoryResponseModel);

        void onFailure(String message);
    }
}
