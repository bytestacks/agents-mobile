package com.cico.cicocashincashout.fragment.transaction_history;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class TransactionHistoryInteractor {

    private TransactionHistoryContract.TransactionHistoryListener transactionHistoryListener;

    final String TRANS_HISTORY = "Transaction History";

    public TransactionHistoryInteractor(TransactionHistoryContract.TransactionHistoryListener
                                                transactionHistoryListener){
        this.transactionHistoryListener = transactionHistoryListener;

    }

    public void transactionHistory(String token, Context context) {

        transactionHistoryObserver(token).subscribeWith(transactionHistoryObservable(context));
    }

    public Observable<TransactionHistoryResponseModel> transactionHistoryObserver(String token) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getAgentTransactionHistory( "Token " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<TransactionHistoryResponseModel> transactionHistoryObservable(final Context context){
        return new DisposableObserver<TransactionHistoryResponseModel>() {
            @Override
            public void onNext(TransactionHistoryResponseModel transactionHistoryResponseModel) {

                Log.d(TRANS_HISTORY, "DATA" + transactionHistoryResponseModel.getData());
                if(transactionHistoryResponseModel.getStatus().equalsIgnoreCase("Error")){
                    transactionHistoryListener.onFailure(transactionHistoryResponseModel.getMessage());
                }else{
                    transactionHistoryListener.onSuccess(context,transactionHistoryResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TRANS_HISTORY,"Error " + e);
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(TRANS_HISTORY,"Completed");
            }
        };
    }


}
