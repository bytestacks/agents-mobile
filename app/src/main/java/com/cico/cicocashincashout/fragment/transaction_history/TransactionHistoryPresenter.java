package com.cico.cicocashincashout.fragment.transaction_history;

import android.content.Context;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;

public class TransactionHistoryPresenter implements TransactionHistoryContract.TransactionHistoryListener {

    private TransactionHistoryContract.TransactionHistoryView transactionHistoryView;
    private TransactionHistoryInteractor transactionHistoryInteractor;

    public TransactionHistoryPresenter(TransactionHistoryContract.TransactionHistoryView transactionHistoryView){
        this.transactionHistoryView = transactionHistoryView;
        transactionHistoryInteractor = new TransactionHistoryInteractor(this);
    }
    @Override
    public void onSuccess(Context context, TransactionHistoryResponseModel transactionHistoryResponseModel) {

        transactionHistoryView.hideProgress();
        transactionHistoryView.loadTransactionHistory(transactionHistoryResponseModel);
    }

    @Override
    public void onFailure(String message) {

        transactionHistoryView.hideProgress();
        transactionHistoryView.showToast(message);
    }

    public void onDestroy(){
        transactionHistoryView = null;
    }

    public void getTransactionHistory(String token, Context context){

        transactionHistoryView.showProgress();
        transactionHistoryInteractor.transactionHistory(token, context);
    }
}
