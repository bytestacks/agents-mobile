package com.cico.cicocashincashout.fragment.wallet_logs;

import android.content.Context;

import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletLogResponse;

public class WalletLogsPresenter implements WalletLogsContract.WalletLogListener {

    private WalletLogsContract.WalletLogView walletLogView;
    private WalletsLogInteractor walletsLogInteractor;

    public WalletLogsPresenter(WalletLogsContract.WalletLogView walletLogView){
        this.walletLogView = walletLogView;
        walletsLogInteractor = new WalletsLogInteractor(this);
    }

    @Override
    public void onSuccess(Context context, WalletLogResponse walletLogResponse) {
        walletLogView.hideProgress();
        walletLogView.loadWalletLogs(walletLogResponse);
    }

    @Override
    public void onFailure(String message) {
        walletLogView.hideProgress();
        walletLogView.showToast(message);
    }

    public void onDestroy(){
        walletLogView = null;
    }


    public void getWalletLogs(String token, Context context){
        walletLogView.showProgress();
        walletsLogInteractor.getAgentWalletLogs(token, context);
    }
}
