package com.cico.cicocashincashout.fragment.home_fragment;

import android.content.Context;

import com.cico.cicocashincashout.model.AgentInfo.response.AgentInfoResponseModel;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;

public class HomeFragmentPresenter implements HomeFragmentContract.HomeFragmentListener {

    private HomeFragmentContract.HomeFragmentView homeFragmentView;
    private HomeFragmentInteractor homeFragmentInteractor;

    public HomeFragmentPresenter(HomeFragmentContract.HomeFragmentView homeFragmentView) {
        this.homeFragmentView = homeFragmentView;
        homeFragmentInteractor = new HomeFragmentInteractor(this);
    }

    @Override
    public void onSuccess(Context context, AgentInfoResponseModel transactionHistoryResponseModel) {
        homeFragmentView.hideProgress();
        homeFragmentView.setRecyclerAdapter(transactionHistoryResponseModel);
    }

    @Override
    public void onFailure(String message) {
        homeFragmentView.hideProgress();
        homeFragmentView.showToast(message);
    }

    public void onDestroy(){
        homeFragmentView = null;
    }

    public void getAgentInto(Context context,String userToken,String agentUUIDCode){
        homeFragmentView.showProgress();
        homeFragmentInteractor.getInfo(context, userToken,agentUUIDCode);
    }
}
