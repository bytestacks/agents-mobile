package com.cico.cicocashincashout.fragment.wallet_logs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.adapter.WalletLogsAdapter;
import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletLogResponse;
import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletlogsDatum;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

import java.util.List;


public class WalletLogsFragment extends Fragment implements WalletLogsContract.WalletLogView {

    private RecyclerView recycler_trans_history;
    private TextView empty_view;
    private WalletLogsAdapter walletLogsAdapter;
    WalletLogsPresenter walletLogsPresenter;
    //private TextView generate_statement;
   // private TextView date_from, date_to;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = prepareViews(inflater, container);

        walletLogsPresenter = new WalletLogsPresenter(this);

        String token = Preferences.getAgentToken(getContext());
        walletLogsPresenter.getWalletLogs(token,getContext());

        return rootView;
    }

    private View prepareViews(LayoutInflater inflater, ViewGroup container) {

        final View mainView = inflater.inflate(R.layout.wallet_logs_history , container, false);

        empty_view = (TextView) mainView.findViewById(R.id.empty_view);

        recycler_trans_history = (RecyclerView) mainView.findViewById(R.id.recycler_trans_history);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_trans_history.setLayoutManager(mLayoutManager);
        recycler_trans_history.setItemAnimator(new DefaultItemAnimator());

        /*generate_statement = (TextView) mainView.findViewById(R.id.generate_statement);
        generate_statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        date_from = (TextView) mainView.findViewById(R.id.date_from);
        date_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickers(date_from);
            }
        });

        date_to = (TextView) mainView.findViewById(R.id.date_to);
        date_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickers(date_to);
            }
        });*/

        return mainView;
    }

    /*private void showDatePickers(TextView view){

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        view.setText(i2 + "/" + i1 + "/" + i);
                    }
                },year,month,dayOfMonth);

        datePickerDialog.show();
    }*/


    @Override
    public void showToast(String message) {
        Utility.shortToast(getContext(), message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(getContext(),false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(getActivity());
    }

    @Override
    public void loadWalletLogs(WalletLogResponse walletLogResponse) {

        List<WalletlogsDatum> walletlogsDatumList = walletLogResponse.getData().getData();
        if(walletlogsDatumList.isEmpty()){
            empty_view.setVisibility(View.VISIBLE);
            empty_view.setText(getContext().getText(R.string.no_wallet_transaction));
            recycler_trans_history.setVisibility(View.GONE);
        }else{
            walletLogsAdapter = new WalletLogsAdapter(walletlogsDatumList, getContext());
            recycler_trans_history.setAdapter(walletLogsAdapter);
            recycler_trans_history.addItemDecoration(new DividerItemDecoration(recycler_trans_history.getContext(), DividerItemDecoration.VERTICAL));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        walletLogsPresenter.onDestroy();
    }
}
