package com.cico.cicocashincashout.fragment.wallet_logs;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletLogResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class WalletsLogInteractor {

    private WalletLogsContract.WalletLogListener walletLogListener;

    final String WALLET_LOGS = "Wallet_Logs";

    public WalletsLogInteractor(WalletLogsContract.WalletLogListener walletLogListener){
        this.walletLogListener = walletLogListener;
    }

    public void getAgentWalletLogs(String token,Context context) {
            getLogs(context,token);
    }

    private void getLogs(Context context,String token) {

        activateUserObserver(token).subscribeWith(getLoginUserObservable(context));
    }

    public Observable<WalletLogResponse> activateUserObserver(String token ) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getWalletLogs( "Token " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<WalletLogResponse> getLoginUserObservable(final Context context){
        return new DisposableObserver<WalletLogResponse>() {
            @Override
            public void onNext(WalletLogResponse walletLogResponse) {

                Log.d(WALLET_LOGS, "DATA" + walletLogResponse.getData());
                if(walletLogResponse.getStatus().equalsIgnoreCase("Error")){
                    walletLogListener.onFailure(walletLogResponse.getMessage());
                }else{
                    walletLogListener.onSuccess(context,walletLogResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(WALLET_LOGS,"Error " + e);
                e.printStackTrace();
                // activateAgentListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(WALLET_LOGS,"Completed");
            }
        };
    }
}
