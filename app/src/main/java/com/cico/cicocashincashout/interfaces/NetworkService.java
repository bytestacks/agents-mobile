package com.cico.cicocashincashout.interfaces;

import com.cico.cicocashincashout.model.AgentInfo.response.AgentInfoResponseModel;
import com.cico.cicocashincashout.model.Bank.request.DisburseFundRequest;
import com.cico.cicocashincashout.model.Bank.request.ValidateNumberRequest;
import com.cico.cicocashincashout.model.Bank.response.BankListResponse;
import com.cico.cicocashincashout.model.Bank.response.DisburseResponse;
import com.cico.cicocashincashout.model.Bank.response.ValidateNumberResponse;
import com.cico.cicocashincashout.model.agent.request.AgentActivateRequestModel;
import com.cico.cicocashincashout.model.agent.response.AgentActivateResponseModel;
import com.cico.cicocashincashout.model.agent_withdraw.request.AgentWithdrawRequest;
import com.cico.cicocashincashout.model.agent_withdraw.response.AgentWithdrawResponse;
import com.cico.cicocashincashout.model.airtime.request.VendingAirtimeRequestModel;
import com.cico.cicocashincashout.model.airtime.response.AirtimeVendingResponseModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.request.StartimesRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.response.StartimesResponseModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.request.StartimesVendRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.response.StartimesVendResponse;
import com.cico.cicocashincashout.model.cashout.request.CashoutRequest;
import com.cico.cicocashincashout.model.cashout.request.InitiateCashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.CashoutResponse;
import com.cico.cicocashincashout.model.cashout.response.InitiateCashoutResponse;
import com.cico.cicocashincashout.model.data.request.DataPlanRequest;
import com.cico.cicocashincashout.model.data.response.DataPlanResponse;
import com.cico.cicocashincashout.model.data.vending.request.DataVendingRequest;
import com.cico.cicocashincashout.model.data.vending.response.DataVendingResponseModel;
import com.cico.cicocashincashout.model.disco_list.response.DiscoListResponseModel;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListResponse;
import com.cico.cicocashincashout.model.dstv.validate_number.request.DstvValidateNumberRequest;
import com.cico.cicocashincashout.model.dstv.validate_number.response.DstvValidateNumberResponse;
import com.cico.cicocashincashout.model.dstv.vend.request.DstvVendRequestModel;
import com.cico.cicocashincashout.model.dstv.vend.response.DstvVendResponseModel;
import com.cico.cicocashincashout.model.electricity.request.ValidateMeterNumberRequest;
import com.cico.cicocashincashout.model.electricity.response.ValidateMeterNumberResponse;
import com.cico.cicocashincashout.model.electricity.vending.request.VendingRequestModel;
import com.cico.cicocashincashout.model.electricity.vending.response.VendingResponseModel;
import com.cico.cicocashincashout.model.fund_wallet.request.FundWalletRequest;
import com.cico.cicocashincashout.model.fund_wallet.response.FundWalletResponse;
import com.cico.cicocashincashout.model.login.request.LoginRequestModel;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;
import com.cico.cicocashincashout.model.user.update_password.request.ChangePasswordRequest;
import com.cico.cicocashincashout.model.user.update_password.response.ChangePasswordResponse;
import com.cico.cicocashincashout.model.wallets.wallet_logs.response.WalletLogResponse;
import com.cico.cicocashincashout.utils.UrlConstants;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface NetworkService {

    @PUT(UrlConstants.ACTIVATE_AGENT_URL)
    Observable<AgentActivateResponseModel> activateAgent(@Body AgentActivateRequestModel agentActivateRequestModel);

    @POST(UrlConstants.LOGIN_USER)
    Observable<LoginResponseModel> loginUser(@Body LoginRequestModel loginRequestModel);

    @GET(UrlConstants.AGENT_WALET_LOGS)
    Observable<WalletLogResponse> getWalletLogs( @Header("Authorization") String header);

    @GET(UrlConstants.AGENT_TRANSACTION_HISTORY)
    Observable<TransactionHistoryResponseModel> getAgentTransactionHistory(@Header("Authorization") String header);

    @GET(UrlConstants.DSTV_PLAN)
    Observable<DstvPriceListResponse> getDstvPriceList(@Header("Authorization") String header);

    @GET(UrlConstants.GOTV_PLAN)
    Observable<DstvPriceListResponse> getGotvPriceList(@Header("Authorization") String header);

    @PUT(UrlConstants.UPDATE_PASSWORD)
    Observable<ChangePasswordResponse> updateUserPassword(@Header("Authorization") String header,
                                                          @Body ChangePasswordRequest changePasswordRequest);;
    @PUT(UrlConstants.VALIDATE_METER_NUMBER)
    Observable<ValidateMeterNumberResponse> validateNumber(@Header("Authorization") String header,
                                                           @Body ValidateMeterNumberRequest validateMeterNumberRequest);

    @POST(UrlConstants.DSTV_VALIDATE_PLAN)
    Observable<DstvValidateNumberResponse> validateDstvNumber(@Header("Authorization") String header, @Body DstvValidateNumberRequest dstvValidateNumberRequest);

    @POST(UrlConstants.DSTV_VEND)
    Observable<DstvVendResponseModel> dstvVend(@Header("Authorization") String header, @Body DstvVendRequestModel dstvVendRequestModel);

    @POST(UrlConstants.VALIDATE_METER_NUMBER)
    Observable<VendingResponseModel> makePayment(@Header("Authorization") String header, @Body VendingRequestModel vendingRequestModel);

    @POST(UrlConstants.VEND_AIRTIME)
    Observable<AirtimeVendingResponseModel> vendAirtime(@Header("Authorization") String header, @Body VendingAirtimeRequestModel vendingAirtimeRequestModel);

    @POST(UrlConstants.DATA_PLAN)
    Observable<DataPlanResponse> getDataPLan(@Header("Authorization") String header,
                                                @Body DataPlanRequest dataPlanRequest);
    @POST(UrlConstants.VEND_DATA)
    Observable<DataVendingResponseModel> vendData(@Header("Authorization") String header,
                                             @Body DataVendingRequest dataVendingRequest);

    @POST(UrlConstants.VEND_DATA)
    Observable<DataVendingResponseModel> vendDataPLan(@Header("Authorization") String header,
                                                      @Body DataVendingRequest dataVendingRequest);

    @POST(UrlConstants.DISBURSE_FUND)
    Observable<DisburseResponse> disburseFund(@Header("Authorization") String header,
                                              @Body DisburseFundRequest disburseFundRequest);

    @GET(UrlConstants.DISCO_LIST)
    Observable<DiscoListResponseModel> getListOfDisco(@Header("Authorization") String header);

    @POST(UrlConstants.STARTIMES_VALIDATE)
    Observable<StartimesResponseModel> validateCustomerSmartCard(@Header("Authorization") String header, @Body StartimesRequestModel startimesRequestModel);

    @POST(UrlConstants.STARTIME_VEND)
    Observable<StartimesVendResponse> vendStartimes(@Header("Authorization") String header, @Body StartimesVendRequestModel startimesVendRequestModel);

    @GET(UrlConstants.BANK_LIST)
    Observable<BankListResponse> getBankList(@Header("Authorization") String header);

    @POST(UrlConstants.BANK_ACCT_VALIDATE)
    Observable<ValidateNumberResponse> validateAcctNumber(@Header("Authorization") String header, @Body ValidateNumberRequest validateNumberRequest);

    @POST(UrlConstants.CASHOUT_URL)
    Observable<CashoutResponse> logCashoutrequest(@Body CashoutRequest cashoutRequest, @Header("Authorization" ) String header);

    @POST(UrlConstants.INITIATE_CASHOUT)
    Observable<InitiateCashoutResponse> initiateCashout(@Body InitiateCashoutRequest initiateCashoutRequest, @Header("Authorization") String header);

    @POST(UrlConstants.AGENT_FUND_WALLET)
    Observable<FundWalletResponse> fundWallet(@Body FundWalletRequest fundWalletRequest, @Header("Authorization") String header);

    @POST(UrlConstants.AGENT_WITHDRAW)
    Observable<AgentWithdrawResponse> agentWithdrawal(@Body AgentWithdrawRequest agentWithdrawRequest, @Header("Authorization") String header);

    @GET(UrlConstants.AGENT_INFO_URL)
    Observable<AgentInfoResponseModel> agentInfo(@Header("Authorization") String header, @Path("id") String id);
}
