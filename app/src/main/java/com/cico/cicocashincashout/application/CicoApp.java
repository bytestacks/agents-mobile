package com.cico.cicocashincashout.application;

import android.app.Application;

public class CicoApp extends Application {

    private static CicoApp appInstance;

    @Override
    public void onCreate(){
        super.onCreate();
        appInstance = this;
    }

    public static synchronized CicoApp getInstance(){
        return appInstance;
    }
}
