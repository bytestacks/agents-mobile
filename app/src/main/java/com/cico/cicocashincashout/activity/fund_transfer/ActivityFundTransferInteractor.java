package com.cico.cicocashincashout.activity.fund_transfer;


import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.Bank.request.ValidateNumberRequest;
import com.cico.cicocashincashout.model.Bank.response.BankListResponse;
import com.cico.cicocashincashout.model.Bank.response.ValidateNumberResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class ActivityFundTransferInteractor {

    private ActivityFundTransferContract.ActivityFundTransferListener activityFundTransferListener;

    final String FUND_TRANSFER = "Fund_Transfer";

    public ActivityFundTransferInteractor(ActivityFundTransferContract.ActivityFundTransferListener
                                                  activityFundTransferListener){

        this.activityFundTransferListener = activityFundTransferListener;
    }

    public void getBanksList(Context context, String header){

        listBanksObserver(header).subscribeWith(listBanksObservable(context));
    }

    public Observable<BankListResponse> listBanksObserver(String header) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getBankList("Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<BankListResponse> listBanksObservable(final Context context){
        return new DisposableObserver<BankListResponse>() {
            @Override
            public void onNext(BankListResponse bankListResponse) {

                Log.d(FUND_TRANSFER, "DATA" + bankListResponse.getData());
                if(bankListResponse.getStatus().equalsIgnoreCase("Error")){
                    activityFundTransferListener.onFailure(bankListResponse.getMessage());
                }else{
                    activityFundTransferListener.onSuccess(context,bankListResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(FUND_TRANSFER,"Error " + e);
                if(e instanceof HttpException){
                    activityFundTransferListener.onFailure("Agent details not found!");
                }else{

                    activityFundTransferListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(FUND_TRANSFER,"Completed");
            }
        };
    }

    public void validateNumber(Context context, String header, ValidateNumberRequest validateNumberRequest) {
        validateAccountNumberObserver(header, validateNumberRequest).subscribeWith(validateAccountNumber(context));
    }

    public Observable<ValidateNumberResponse> validateAccountNumberObserver(String header, ValidateNumberRequest validateNumberRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .validateAcctNumber("Token " + header,validateNumberRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<ValidateNumberResponse> validateAccountNumber(final Context context){
        return new DisposableObserver<ValidateNumberResponse>() {
            @Override
            public void onNext(ValidateNumberResponse bankListResponse) {

                Log.d(FUND_TRANSFER, "DATA" + bankListResponse.getData());
                if(bankListResponse.getStatus().equalsIgnoreCase("Error")){
                    activityFundTransferListener.onFailure(bankListResponse.getMessage());
                }else{
                    activityFundTransferListener.validateAccountNumberSuccess(context,bankListResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(FUND_TRANSFER,"Error " + e);
                if(e instanceof HttpException){
                    activityFundTransferListener.onFailure("Invalid account number");
                }else{

                    activityFundTransferListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(FUND_TRANSFER,"Completed");
            }
        };
    }
}
