package com.cico.cicocashincashout.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;

public class PinpadSetupActivity extends AppCompatActivity {

    TextView proceed_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinpad_setup);
        initControls();
    }

    private void initControls() {

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // new AppNavigator(PinpadSetupActivity.this).navigateToSuccess(new Bundle());
            }
        });

    }
}
