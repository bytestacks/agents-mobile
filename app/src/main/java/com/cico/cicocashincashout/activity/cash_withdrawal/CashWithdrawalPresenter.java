package com.cico.cicocashincashout.activity.cash_withdrawal;

import android.content.Context;

import com.cico.cicocashincashout.model.cashout.request.InitiateCashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.InitiateCashoutResponse;

public class CashWithdrawalPresenter implements CashWithdrawalContract.CashWithdrawalListener {

    private CashWithdrawalContract.CashWithdrawalView cashWithdrawalView;
    private CashWithdrawalInteractor cashWithdrawalInteractor;

    public CashWithdrawalPresenter(CashWithdrawalContract.CashWithdrawalView cashWithdrawalView) {
        this.cashWithdrawalView = cashWithdrawalView;
        cashWithdrawalInteractor = new CashWithdrawalInteractor(this);
    }

    @Override
    public void onInitiateCashoutResponse(Context context, InitiateCashoutResponse initiateCashoutResponse) {
        cashWithdrawalView.hideProgress();
        cashWithdrawalView.onInitiateCashoutResponse(context, initiateCashoutResponse);
    }

    @Override
    public void onValidateSuccess(Context context) {

    }

    @Override
    public void onValidateFailure(String message) {

    }

    @Override
    public void onFailure(String message) {
        cashWithdrawalView.hideProgress();
        cashWithdrawalView.onFailure(message);
    }

    public void initiateCashout(InitiateCashoutRequest initiateCashoutRequest, String header, Context context){
        cashWithdrawalView.showProgress();
        cashWithdrawalInteractor.initiateCashout(initiateCashoutRequest,header,context);
    }
}
