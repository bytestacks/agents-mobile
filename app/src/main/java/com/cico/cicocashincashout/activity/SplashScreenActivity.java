package com.cico.cicocashincashout.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;

public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        handleSplashDelay();
    }

    private void handleSplashDelay(){

        imageView = findViewById(R.id.imageView);
        if(imageView !=null){
            imageView.setAnimation(AnimationUtils.loadAnimation(this,R.anim.splash_image_anim));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateToNextScreen();
            }
        },SPLASH_TIME_OUT);
    }

    private void navigateToNextScreen(){
        boolean isAgentActivated = Preferences.isAgentActivated(SplashScreenActivity.this);
        if(isAgentActivated){
            new AppNavigator(SplashScreenActivity.this).navigateToLoginScreen();
            finish();
        }else{
            new AppNavigator(SplashScreenActivity.this).navigateToActivateAgent();
            finish();
        }
    }
}
