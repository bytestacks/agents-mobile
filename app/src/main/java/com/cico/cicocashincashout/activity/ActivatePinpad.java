package com.cico.cicocashincashout.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.paypad.cardreader.facade.PinpadFacade;
import com.paypad.impl.Paypad;

public class ActivatePinpad extends AppCompatActivity {

    EditText txt_username;
    TextView proceed_button,initialize;

    PinpadFacade pinpadFacade;
    Handler handler;
    Paypad paypad;
    private MyBroadcastReceiver myBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinpad);

        initializeControls();
    }

    private void initializeControls() {

        txt_username = (EditText) findViewById(R.id.txt_username);
        /*initialize = (TextView) findViewById(R.id.initialize);
        initialize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Initialize().execute(DeviceActivity.class);
            }
        });*/
        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                String activation_Code = Preferences.getActivationCode(ActivatePinpad.this);
                if(!TextUtils.isEmpty(activation_Code)){
                    txt_username.setText(activation_Code);
                    activatePinPad(ActivatePinpad.this,activation_Code);
                }else{
                    Utility.shortToast(ActivatePinpad.this, ActivatePinpad.this.getString(R.string.activation_pin_code));
                }

            }
        });

        handler = new Handler();
        pinpadFacade = new PinpadFacade(this);

        paypad = new Paypad(this);

        myBroadcastReceiver = new MyBroadcastReceiver();

        registerReceiver(myBroadcastReceiver, new IntentFilter("com.esl.paypadlib"));
    }

    private void activatePinPad(Context context, String activationCode){

        if(TextUtils.isEmpty(activationCode)){
            Utility.shortToast(context,getString(R.string.input_activation_code));
        }else{
            Utility.showProgressDialog(context,false);
            new Activate().execute(activationCode);
        }
    }

    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.getAction() != null) {
                String s = intent.getAction();
                if (s.equals("com.esl.paypadlib")) {

                    String result = intent.getStringExtra("response");
                    String[] resultarray = intent
                            .getStringArrayExtra("responsearray");
                    String reversalResult = intent.getStringExtra("reversalResult");
                    String encryptedMessage = intent.getStringExtra("encryptedMessage");

                    if (result.equals("activatecomplete")) {
                        Utility.hideProgressDialog(ActivatePinpad.this);
                        Toast.makeText(getApplicationContext(),
                                "Activated Successfully", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("invalidcode")) {
                        Utility.hideProgressDialog(ActivatePinpad.this);
                        Toast.makeText(getApplicationContext(),
                                "Invalid Activation Code", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("failedactivation")) {
                        Utility.hideProgressDialog(ActivatePinpad.this);
                        Toast.makeText(getApplicationContext(),
                                "Activation not Successful", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("connected")) {
						Utility.hideProgressDialog(ActivatePinpad.this);
                        Toast.makeText(getApplicationContext(),
                                "Device is connected", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("startInitializeprogress")) {
                        Utility.showProgressDialog(context,false);

                    } else if (result.equals("Initializecomplete")) {
                        Utility.hideProgressDialog(ActivatePinpad.this);
                        Toast.makeText(getApplicationContext(),
                                "Initialized Successfully", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("initializenotcomplete")) {
                        Utility.hideProgressDialog(ActivatePinpad.this);
                        Toast.makeText(getApplicationContext(),
                                "Initialization not Successful",
                                Toast.LENGTH_LONG).show();

                    } else if (result.equals("pinpadProcessing")) {
                       // mProgressDialog.setMessage("Please wait...");

                    } else if (result.equals("enterPIN")) {
                       // mProgressDialog.setMessage("Please, Enter PIN");


                    } else if (result.equals("PINentered")) {
                       // mProgressDialog.setMessage("PIN Entered");

                    } else if (result.equals("nibssProcessing")) {
                       // mProgressDialog.setMessage("Please wait...\n Processing Transaction");

                    } else if (result.equals("errorTranx")) {
                       // mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),
                                "Critical error occured", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("transactionresponse")) {
                       // mProgressDialog.dismiss();
                        /*
                         * The returned String array has the following element
                         * with the indices below 0-Response Code 1-Response
                         * 2-Terminal ID 3-PAN 4-Card holder name 5-Date
                         * 6-Amount 7-Transaction ID 8-RRN
                         */
                        for (int i = 0; i < resultarray.length; i++) {
                            Toast.makeText(getApplicationContext(),
                                    resultarray[i], Toast.LENGTH_SHORT).show();
                        }
                    } else if (result.equals("reversal")) {
                       // mProgressDialog.dismiss();

                        Toast.makeText(getApplicationContext(),
                                reversalResult, Toast.LENGTH_SHORT).show();

                    } else if (result.equals("fcmbresponse")) {
                       // mProgressDialog.dismiss();

//                        byte[] dectryptArray = encryptedMessage.getBytes();
//                        byte[] decarray = Base64.decodeBase64(dectryptArray);
//                        String message = null;
//                        try {
//                            message = new String(decarray,"UTF-8");
//                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
//                        }
//
//                        Log.i("Enc:", encryptedMessage);
//                        Log.i("Msg:", message);
//
//                        Toast.makeText(getApplicationContext(),
//                                message, Toast.LENGTH_LONG).show();

                    }
                }
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pinpadFacade.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    protected void onResume() {

        super.onResume();
        registerReceiver(myBroadcastReceiver, new IntentFilter(
                "com.esl.paypadlib"));
    }

    @Override
    protected void onPause() {

        super.onPause();
        unregisterReceiver(myBroadcastReceiver);
    }


    class Activate extends AsyncTask<String, String, Long> {

        @Override
        protected Long doInBackground(String... params) {
            String regActivationCode = params[0];

            paypad.activate(regActivationCode);

            return null;

        }

        // This is executed in the context of the main GUI thread
        protected void onPostExecute(Long result) {

        }
    }

    class Initialize extends AsyncTask<Class<?>, String, Long> {

        @Override
        protected Long doInBackground(Class<?>... params) {
            Class<?> classactivity = params[0];
            paypad.initialize(classactivity);

            return null;

        }

        // This is executed in the context of the main GUI thread
        protected void onPostExecute(Long result) {

        }
    }
}
