package com.cico.cicocashincashout.activity.bills_payment;

import android.content.Context;

import com.cico.cicocashincashout.model.disco_list.response.DiscoListResponseModel;
import com.cico.cicocashincashout.utils.Preferences;
import com.google.gson.Gson;

public class BillsPaymentPresenter implements BillsPaymentContract.BillsPaymentListener {

    private BillsPaymentContract.BillsPaymentlView billsPaymentlView;
    private BillsPaymentInteractor billsPaymentInteractor;

    public BillsPaymentPresenter(BillsPaymentContract.BillsPaymentlView billsPaymentlView){
        this.billsPaymentlView = billsPaymentlView;
        billsPaymentInteractor = new BillsPaymentInteractor(this);
    }

    @Override
    public void onSuccess(Context context,DiscoListResponseModel discoListResponseModel) {

        billsPaymentlView.hideProgress();
        saveDiscolist(context, discoListResponseModel);
        billsPaymentlView.onSuccess(discoListResponseModel);

    }

    @Override
    public void onFailure(String message) {
        billsPaymentlView.hideProgress();
        billsPaymentlView.showToast(message);
    }

    public void getDiscoList(Context context, String header){
        billsPaymentlView.showProgress();
        billsPaymentInteractor.getList(context, header);
    }

    private void saveDiscolist(Context context,DiscoListResponseModel discoListResponseModel){
        Gson gson = new Gson();
        String discoList = gson.toJson(discoListResponseModel);
        Preferences.setDiscoList(context, discoList);
    }
}
