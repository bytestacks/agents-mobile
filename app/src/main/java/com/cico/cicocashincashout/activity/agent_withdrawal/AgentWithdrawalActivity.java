package com.cico.cicocashincashout.activity.agent_withdrawal;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.agent_withdraw.request.AgentWithdrawRequest;
import com.cico.cicocashincashout.model.agent_withdraw.response.AgentWithdrawResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

public class AgentWithdrawalActivity extends AppCompatActivity implements AgentWithdrawalContract.AgentWithdrawalView{

    EditText fund_wallet;
    TextView proceed_button;

    String amount;

    AgentWithdrawalPresenter agentWithdrawalPresenter;

    private void initializeControls(){

        fund_wallet = (EditText) findViewById(R.id.wallet_amount);
        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                amount = fund_wallet.getText().toString();

                String header = Preferences.getAgentToken(AgentWithdrawalActivity.this);

                AgentWithdrawRequest agentWithdrawRequest = new AgentWithdrawRequest();
                agentWithdrawRequest.setAmount(Integer.parseInt(amount));

                agentWithdrawalPresenter.withdrawnFund(AgentWithdrawalActivity.this,
                        header, agentWithdrawRequest);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_agent_withdrawal);

        initializeControls();

        agentWithdrawalPresenter = new AgentWithdrawalPresenter(this);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(AgentWithdrawalActivity.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(AgentWithdrawalActivity.this);
    }

    @Override
    public void onFailure(String token) {
        Bundle bundle = new Bundle();
        bundle.putString("result","fail");
        bundle.putString("amount",amount);
        bundle.putString("tran_type","Wallet Withdraw");
        new AppNavigator(AgentWithdrawalActivity.this).navigateToSuccess(bundle);
    }

    @Override
    public void onSuccess(AgentWithdrawResponse agentWithdrawResponse) {

        Bundle bundle = new Bundle();
        bundle.putString("result","Success");
        bundle.putString("amount",amount);
        bundle.putString("tran_type","Wallet Withdraw");
        new AppNavigator(AgentWithdrawalActivity.this).navigateToSuccess(bundle);

    }
}
