package com.cico.cicocashincashout.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;

public class PinpadProcessActivity extends AppCompatActivity {

    ImageView img_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_withdrawal_how);
        initControls();
    }

    private void initControls() {

        img_view = (ImageView) findViewById(R.id.imageView10);
        img_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(PinpadProcessActivity.this).navigateToPaymentVerification(new Bundle());

            }
        });

    }
}
