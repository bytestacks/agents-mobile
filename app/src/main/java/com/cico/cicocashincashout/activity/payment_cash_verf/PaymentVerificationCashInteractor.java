package com.cico.cicocashincashout.activity.payment_cash_verf;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.cashout.request.CashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.CashoutResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class PaymentVerificationCashInteractor {

    private PaymentVerificationCashContract.PaymentVerificationCashListener paymentVerificationListener;

    final String FUND_TRANSFER = "CASHOUT";

    public PaymentVerificationCashInteractor(PaymentVerificationCashContract.PaymentVerificationCashListener paymentVerificationListener){

        this.paymentVerificationListener = paymentVerificationListener;
    }

    public void logFund(Context context, String token, CashoutRequest cashoutRequest) {

        validateAccountNumberObserver(token, cashoutRequest).subscribeWith(validateAccountNumber(context));
    }


    public Observable<CashoutResponse> validateAccountNumberObserver(String header, CashoutRequest cashoutRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .logCashoutrequest(cashoutRequest,"Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<CashoutResponse> validateAccountNumber(final Context context){
        return new DisposableObserver<CashoutResponse>() {
            @Override
            public void onNext(CashoutResponse cashoutResponse) {

                Log.d(FUND_TRANSFER, "DATA" + cashoutResponse.getData());
                if(cashoutResponse.getStatus().equalsIgnoreCase("Error")){
                    paymentVerificationListener.onFailure(cashoutResponse.getMessage());
                }else{
                    paymentVerificationListener.onSuccess(context,cashoutResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(FUND_TRANSFER,"Error " + e);
                if(e instanceof HttpException){
                    paymentVerificationListener.onFailure("An error occurred please try again later");
                }else{

                    paymentVerificationListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(FUND_TRANSFER,"Completed");
            }
        };
    }
}
