package com.cico.cicocashincashout.activity.payment_verification;

import android.content.Context;

import com.cico.cicocashincashout.model.Bank.request.DisburseFundRequest;
import com.cico.cicocashincashout.model.Bank.response.DisburseResponse;

public class PaymentVerificationPresenter implements PaymentVerificationContract.PaymentVerificationListener {

    private PaymentVerificationContract.PaymentVerificationView paymentVerificationView;
    private PaymentVerificationInteractor paymentVerificationInteractor;

    public PaymentVerificationPresenter(PaymentVerificationContract.PaymentVerificationView paymentVerificationView){
        this.paymentVerificationView = paymentVerificationView;
        paymentVerificationInteractor = new PaymentVerificationInteractor(this);
    }

    @Override
    public void onSuccess(Context context,DisburseResponse disburseResponse) {
        paymentVerificationView.hideProgress();
        paymentVerificationView.onTransferSuccess(disburseResponse);
    }

    @Override
    public void onFailure(String message) {
        paymentVerificationView.hideProgress();
        paymentVerificationView.showError("");
        //paymentVerificationView.showToast(message);
    }

    public void disbursefund(Context context, String token, DisburseFundRequest disburseFundRequest){
        paymentVerificationView.showProgress();
        paymentVerificationInteractor.disburseFund(context, token, disburseFundRequest);
    }
}
