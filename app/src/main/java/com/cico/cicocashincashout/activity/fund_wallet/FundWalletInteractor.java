package com.cico.cicocashincashout.activity.fund_wallet;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.fund_wallet.request.FundWalletRequest;
import com.cico.cicocashincashout.model.fund_wallet.response.FundWalletResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class FundWalletInteractor {

    private FundWalletContract.FundWalletListener fundWalletListener;

    final String FUND_TRANSFER = "FUND_WALLET";

    public FundWalletInteractor(FundWalletContract.FundWalletListener
                                        fundWalletListener){

        this.fundWalletListener = fundWalletListener;
    }

    public void fundWallet(Context context, String header, FundWalletRequest fundWalletRequest){
        creditWalletObserver(fundWalletRequest,header).subscribeWith(creditWalletObservable(context));
    }

    public Observable<FundWalletResponse> creditWalletObserver(FundWalletRequest fundWalletRequest, String header) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .fundWallet(fundWalletRequest,"Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<FundWalletResponse> creditWalletObservable(final Context context){
        return new DisposableObserver<FundWalletResponse>() {
            @Override
            public void onNext(FundWalletResponse bankListResponse) {

                Log.d(FUND_TRANSFER, "DATA" + bankListResponse.getData());
                if(bankListResponse.getStatus().equalsIgnoreCase("Error")){
                    fundWalletListener.onFailure(bankListResponse.getMessage());
                }else{
                    fundWalletListener.onSuccess(context,bankListResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(FUND_TRANSFER,"Error " + e);
                if(e instanceof HttpException){
                    fundWalletListener.onFailure(context.getString(R.string.login_error));
                }else{

                    fundWalletListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(FUND_TRANSFER,"Completed");
            }
        };
    }
}
