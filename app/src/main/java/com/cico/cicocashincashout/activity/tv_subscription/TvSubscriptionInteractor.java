package com.cico.cicocashincashout.activity.tv_subscription;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.cable_tv.startimes.request.StartimesRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.response.StartimesResponseModel;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListResponse;
import com.cico.cicocashincashout.model.dstv.validate_number.request.DstvValidateNumberRequest;
import com.cico.cicocashincashout.model.dstv.validate_number.response.DstvValidateNumberResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class TvSubscriptionInteractor {

    private TvSubscriptionContract.TvSubscriptionListener tvSubscriptionListener;

    final String STARTIME_VALIDATE = "startime_validate";

    public TvSubscriptionInteractor(TvSubscriptionContract.TvSubscriptionListener tvSubscriptionListener){

        this.tvSubscriptionListener = tvSubscriptionListener;
    }


    public void validateNumber(Context context, String header, StartimesRequestModel startimesRequestModel) {
        validateStratimeNumberObserver(header,startimesRequestModel).subscribeWith(validateStratimeNumberObservable(context));
    }

    public void getDstvPriceList(Context context, String header){
        getDstvPriceListObserver(header).subscribeWith(getDstvPriceListObservable(context));
    }

    public Observable<DstvPriceListResponse> getDstvPriceListObserver(String header) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getDstvPriceList("Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<DstvPriceListResponse> getGotvPriceListObserver(String header) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getGotvPriceList("Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<StartimesResponseModel> validateStratimeNumberObserver(String header, StartimesRequestModel startimesRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .validateCustomerSmartCard("Token " + header,startimesRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<StartimesResponseModel> validateStratimeNumberObservable(final Context context){
        return new DisposableObserver<StartimesResponseModel>() {
            @Override
            public void onNext(StartimesResponseModel discoListResponseModel) {

                Log.d(STARTIME_VALIDATE, "DATA" + discoListResponseModel.getData());
                if(discoListResponseModel.getStatus().equalsIgnoreCase("Error")){
                    tvSubscriptionListener.onValidateFailure(discoListResponseModel.getMessage());
                }else{
                    tvSubscriptionListener.onValidateSuccess(context,discoListResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(STARTIME_VALIDATE,"Error " + e);
                e.printStackTrace();
                tvSubscriptionListener.onValidateFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(STARTIME_VALIDATE,"Completed");
            }
        };
    }

    public DisposableObserver<DstvPriceListResponse> getDstvPriceListObservable(final Context context){
        return new DisposableObserver<DstvPriceListResponse>() {
            @Override
            public void onNext(DstvPriceListResponse dstvPriceListResponse) {

                Log.d(STARTIME_VALIDATE, "DATA" + dstvPriceListResponse.getData());
                if(dstvPriceListResponse.getStatus().equalsIgnoreCase("Error")){
                    tvSubscriptionListener.onValidateFailure(dstvPriceListResponse.getMessage());
                }else{
                    tvSubscriptionListener.dstvPriceListSuccess(context,"Dstv",dstvPriceListResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(STARTIME_VALIDATE,"Error " + e);
                e.printStackTrace();
                tvSubscriptionListener.onValidateFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(STARTIME_VALIDATE,"Completed");
            }
        };
    }

    public DisposableObserver<DstvPriceListResponse> getGotvPriceListObservable(final Context context){
        return new DisposableObserver<DstvPriceListResponse>() {
            @Override
            public void onNext(DstvPriceListResponse dstvPriceListResponse) {

                Log.d(STARTIME_VALIDATE, "DATA" + dstvPriceListResponse.getData());
                if(dstvPriceListResponse.getStatus().equalsIgnoreCase("Error")){
                    tvSubscriptionListener.onValidateFailure(dstvPriceListResponse.getMessage());
                }else{
                    tvSubscriptionListener.dstvPriceListSuccess(context,"Gotv",dstvPriceListResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(STARTIME_VALIDATE,"Error " + e);
                e.printStackTrace();
                tvSubscriptionListener.onValidateFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(STARTIME_VALIDATE,"Completed");
            }
        };
    }

    public void getGotvPriceList(Context context, String header) {

        getGotvPriceListObserver(header).subscribeWith(getGotvPriceListObservable(context));
    }

    public void validateDstvNumber(Context context, String header, DstvValidateNumberRequest dstvValidateNumberRequest) {
        validateDstvNumberObserver(header,dstvValidateNumberRequest).subscribeWith(validateDstvNumberObservable(context));
    }

    public Observable<DstvValidateNumberResponse> validateDstvNumberObserver(String header, DstvValidateNumberRequest dstvValidateNumberRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .validateDstvNumber("Token " + header,dstvValidateNumberRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<DstvValidateNumberResponse> validateDstvNumberObservable(final Context context){
        return new DisposableObserver<DstvValidateNumberResponse>() {
            @Override
            public void onNext(DstvValidateNumberResponse discoListResponseModel) {

                Log.d(STARTIME_VALIDATE, "DATA" + discoListResponseModel.getData());
                if(discoListResponseModel.getStatus().equalsIgnoreCase("Error")){
                    tvSubscriptionListener.onValidateFailure(discoListResponseModel.getMessage());
                }else{
                    tvSubscriptionListener.onValidateDstvSuccess(context,discoListResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(STARTIME_VALIDATE,"Error " + e);
                e.printStackTrace();
                tvSubscriptionListener.onValidateFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(STARTIME_VALIDATE,"Completed");
            }
        };
    }
}
