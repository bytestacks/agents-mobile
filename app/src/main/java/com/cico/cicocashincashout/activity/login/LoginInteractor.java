package com.cico.cicocashincashout.activity.login;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.login.request.LoginRequestModel;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class LoginInteractor {

    private LoginContract.LoginListener loginListener;

    final String LOG_IN_USER = "login_user";

    public LoginInteractor(LoginContract.LoginListener loginListener){

        this.loginListener = loginListener;
    }

    public void validateUserLoginDetails(Context context, final LoginRequestModel loginRequestModel){

        if(hasErrors(context,loginRequestModel)){
            return;
        }
        loginAgent(context, loginRequestModel);

    }

    private void loginAgent(Context context, LoginRequestModel loginRequestModel) {
        loginUserObserver(loginRequestModel).subscribeWith(getLoginUserObservable(context));
    }

    public DisposableObserver<LoginResponseModel> getLoginUserObservable(final Context context){
        return new DisposableObserver<LoginResponseModel>() {
            @Override
            public void onNext(LoginResponseModel loginResponseModel) {

                Log.d(LOG_IN_USER, "DATA" + loginResponseModel.getData());
                if(loginResponseModel.getStatus().equalsIgnoreCase("Successful")){
                    loginListener.onSuccess(context,loginResponseModel);
                }else{
                    loginListener.onFailure(loginResponseModel.getMessage());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(LOG_IN_USER,"Error " + e);
                if(e instanceof HttpException){

                    loginListener.onFailure("Invalid Credentials");
                }else{

                    loginListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(LOG_IN_USER,"Completed");
            }
        };
    }

    private Observable loginUserObserver(LoginRequestModel loginRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .loginUser(loginRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private boolean hasErrors(Context context, LoginRequestModel loginRequestModel) {

        String phone = loginRequestModel.getUser().getPhone();
        String code = loginRequestModel.getUser().getPassword();

        if(TextUtils.isEmpty(phone)){
            loginListener.onFailure(context.getString(R.string.username_empty));
            return true;
        }else if(TextUtils.isEmpty(code)){
            loginListener.onFailure(context.getString(R.string.password_empty));
            return true;
        }
        return false;
    }
}
