package com.cico.cicocashincashout.activity.tv_subscription;

import android.content.Context;

import com.cico.cicocashincashout.model.cable_tv.startimes.response.StartimesResponseModel;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListResponse;
import com.cico.cicocashincashout.model.dstv.validate_number.response.DstvValidateNumberResponse;

public interface TvSubscriptionContract {

    interface TvSubscriptionView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void startimesValidateNumberSuccess(StartimesResponseModel startimesResponseModel);

        void dstvValidateNumberSuccess(DstvValidateNumberResponse dstvValidateNumberResponse);

        void dstvPriceListSuccess(DstvPriceListResponse dstvPriceListResponse);
    }

    interface TvSubscriptionListener{

        void onValidateSuccess(Context context, StartimesResponseModel startimesResponseModel);

        void onValidateDstvSuccess(Context context, DstvValidateNumberResponse dstvValidateNumberResponse);

        void onValidateFailure(String message);

        void dstvPriceListSuccess(Context context,String cableName,DstvPriceListResponse dstvPriceListResponse);
    }
}
