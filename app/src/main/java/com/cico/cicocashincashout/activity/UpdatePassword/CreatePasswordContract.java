package com.cico.cicocashincashout.activity.UpdatePassword;

import android.content.Context;

import com.cico.cicocashincashout.model.user.update_password.response.ChangePasswordResponse;

public interface CreatePasswordContract {

    interface CreatePasswordView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void updateSuccess();
    }

    interface CreatePasswordListener{

        void onSuccess(Context context, ChangePasswordResponse changePasswordResponse);

        void onFailure(String message);
    }
}
