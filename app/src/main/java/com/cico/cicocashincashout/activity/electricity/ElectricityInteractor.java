package com.cico.cicocashincashout.activity.electricity;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.electricity.request.ValidateMeterNumberRequest;
import com.cico.cicocashincashout.model.electricity.response.ValidateMeterNumberResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class  ElectricityInteractor {

    private ElectricityContract.ElectricityListener electricityListener;

    final String ELECTRICTY = "electricity_vending";


    public ElectricityInteractor(ElectricityContract.ElectricityListener electricityListener){

        this.electricityListener = electricityListener;
    }
    public void validateUserLoginDetails(Context context, final ValidateMeterNumberRequest validateMeterNumberRequest, String token){

        if(hasErrors(context,validateMeterNumberRequest)){
            return;
        }
        validateNumberObserver(token,validateMeterNumberRequest).subscribeWith(getLoginUserObservable(context));
    }

    private boolean hasErrors(Context context, ValidateMeterNumberRequest validateMeterNumberRequest) {

        String phone = validateMeterNumberRequest.getMeterNumber();
        if(TextUtils.isEmpty(phone)){
            electricityListener.onValidateFailure(context.getString(R.string.meter_number_error));
            return true;
        }
        return false;
    }

    private Observable validateNumberObserver(String token, ValidateMeterNumberRequest validateMeterNumberRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .validateNumber("Token " + token, validateMeterNumberRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<ValidateMeterNumberResponse> getLoginUserObservable(final Context context){
        return new DisposableObserver<ValidateMeterNumberResponse>() {
            @Override
            public void onNext(ValidateMeterNumberResponse validateMeterNumberResponse) {

                Log.d(ELECTRICTY, "DATA" + validateMeterNumberResponse.getData());
                if(validateMeterNumberResponse.getStatus().equalsIgnoreCase("Successful")){
                    electricityListener.onValidateSuccess(context,validateMeterNumberResponse);
                }else{
                    electricityListener.onValidateFailure(validateMeterNumberResponse.getMessage());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(ELECTRICTY,"Error " + e);
                if(e instanceof HttpException){
                    electricityListener.onValidateFailure("Invalid Meter Number!");
                }else{
                    electricityListener.onValidateFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();

            }

            @Override
            public void onComplete() {
                Log.d(ELECTRICTY,"Completed");
            }
        };
    }
}
