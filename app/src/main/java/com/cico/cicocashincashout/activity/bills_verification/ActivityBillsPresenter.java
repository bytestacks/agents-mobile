package com.cico.cicocashincashout.activity.bills_verification;

import android.content.Context;

import com.cico.cicocashincashout.model.cable_tv.startimes.vend.request.StartimesVendRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.response.StartimesVendResponse;
import com.cico.cicocashincashout.model.dstv.vend.request.DstvVendRequestModel;
import com.cico.cicocashincashout.model.dstv.vend.response.DstvVendResponseModel;

public class ActivityBillsPresenter implements ActivityBillsContract.ActivityBillsListener {

    private ActivityBillsContract.ActivityBillsView activityBillsView;
    private ActivityBillsInteractor activityBillsInteractor;

    public ActivityBillsPresenter(ActivityBillsContract.ActivityBillsView activityBillsView){
        this.activityBillsView = activityBillsView;
        activityBillsInteractor = new ActivityBillsInteractor(this);
    }


    @Override
    public void onSuccess(Context context, StartimesVendResponse startimesResponseData) {
        activityBillsView.hideProgress();
        activityBillsView.onSuccess(startimesResponseData);
    }

    @Override
    public void onDstvSuccess(Context context, DstvVendResponseModel dstvVendResponseModel) {
        activityBillsView.hideProgress();
        activityBillsView.onDstvSuccess(dstvVendResponseModel);

    }

    @Override
    public void onFailure(String message) {
        activityBillsView.hideProgress();
        activityBillsView.onFailure("");
        //activityBillsView.showToast(message);
    }

    public void vendStartime(Context context, String header, StartimesVendRequestModel startimesVendRequestModel){
        activityBillsView.showProgress();
        activityBillsInteractor.vend(context,header,startimesVendRequestModel);
    }

    public void vendDstv(Context context, String header, DstvVendRequestModel dstvVendRequestModel){
        activityBillsView.showProgress();
        activityBillsInteractor.vendDstv(context,header,dstvVendRequestModel);


    }
}
