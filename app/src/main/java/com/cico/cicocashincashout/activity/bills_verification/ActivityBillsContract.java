package com.cico.cicocashincashout.activity.bills_verification;

import android.content.Context;

import com.cico.cicocashincashout.model.cable_tv.startimes.vend.response.StartimesVendResponse;
import com.cico.cicocashincashout.model.dstv.vend.response.DstvVendResponseModel;

public interface ActivityBillsContract {

    interface ActivityBillsView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void onFailure(String token);

        void onSuccess(StartimesVendResponse startimesResponseData);

        void onDstvSuccess(DstvVendResponseModel dstvVendResponseModel);
    }

    interface ActivityBillsListener{

        void onSuccess(Context context, StartimesVendResponse startimesResponseData);

        void onDstvSuccess(Context context, DstvVendResponseModel dstvVendResponseModel);

        void onFailure(String message);
    }
}
