package com.cico.cicocashincashout.activity.fund_wallet;

import android.content.Context;

import com.cico.cicocashincashout.model.fund_wallet.response.FundWalletResponse;

public interface FundWalletContract {

    interface FundWalletView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void showError(String token);

        void onSuccess(FundWalletResponse fundWalletResponse);
    }

    interface FundWalletListener{

        void onSuccess(Context context, FundWalletResponse fundWalletResponse);

        void onFailure(String message);
    }
}
