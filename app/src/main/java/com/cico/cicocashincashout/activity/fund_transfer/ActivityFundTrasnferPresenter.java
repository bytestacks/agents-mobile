package com.cico.cicocashincashout.activity.fund_transfer;

import android.content.Context;

import com.cico.cicocashincashout.model.Bank.request.ValidateNumberRequest;
import com.cico.cicocashincashout.model.Bank.response.BankListResponse;
import com.cico.cicocashincashout.model.Bank.response.ValidateNumberResponse;
import com.cico.cicocashincashout.model.Bank.vfdresponse.response.VFDResponse;
import com.cico.cicocashincashout.model.Bank.vfdresponse.validate_number_response.VFDValidateNumberResponse;
import com.cico.cicocashincashout.utils.Preferences;
import com.google.gson.Gson;

public class ActivityFundTrasnferPresenter implements ActivityFundTransferContract.ActivityFundTransferListener {

    private ActivityFundTransferContract.ActivityFundTransferView activityFundTransferView;
    private ActivityFundTransferInteractor activityFundTransferInteractor;

    public ActivityFundTrasnferPresenter(ActivityFundTransferContract.ActivityFundTransferView activityFundTransferView){
        this.activityFundTransferView = activityFundTransferView;
        activityFundTransferInteractor = new ActivityFundTransferInteractor(this);
    }

    @Override
    public void onSuccess(Context context, BankListResponse bankListResponse) {
        activityFundTransferView.hideProgress();
        if(bankListResponse.getData() != null && !bankListResponse.getData().isEmpty()){

            saveBanklist(context,bankListResponse);
            activityFundTransferView.populateBankList(bankListResponse);
        }
    }

    @Override
    public void validateAccountNumberSuccess(Context context, ValidateNumberResponse validateNumberResponse) {
        activityFundTransferView.hideProgress();
        activityFundTransferView.validateAccountNumberSuccess(validateNumberResponse);
    }

    @Override
    public void onFailure(String message) {
        activityFundTransferView.hideProgress();
        activityFundTransferView.showToast(message);
    }

    private void saveBanklist(Context context, BankListResponse bankListResponse){
        if(!(bankListResponse.getData() == null || bankListResponse.getData().isEmpty())){

            Gson gson = new Gson();
            String bankList = gson.toJson(bankListResponse);
            Preferences.setBankList(context, bankList);
        }
    }

    public void getBankList(Context context, String header){
        activityFundTransferView.showProgress();
        activityFundTransferInteractor.getBanksList(context,header);
    }

    public void valdiateAccountNumber(Context context, String header, ValidateNumberRequest validateNumberRequest){
        activityFundTransferView.showProgress();
        activityFundTransferInteractor.validateNumber(context,header,validateNumberRequest);

    }
}
