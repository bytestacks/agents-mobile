package com.cico.cicocashincashout.activity.payment_cash_verf;

import android.content.Context;
import com.cico.cicocashincashout.model.cashout.response.CashoutResponse;

public interface PaymentVerificationCashContract {

    interface PaymentVerificationCashView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void showError(String token);

        void onSuccess(CashoutResponse cashoutResponse);
    }

    interface PaymentVerificationCashListener{

        void onSuccess(Context context, CashoutResponse cashoutResponse);

        void onFailure(String message);
    }
}
