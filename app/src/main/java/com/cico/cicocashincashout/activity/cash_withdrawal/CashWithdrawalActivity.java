package com.cico.cicocashincashout.activity.cash_withdrawal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.ActivatePinpad;
import com.cico.cicocashincashout.activity.DeviceActivity;
import com.cico.cicocashincashout.model.cashout.request.InitiateCashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.InitiateCashoutResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.paypad.cardreader.facade.PinpadFacade;
import com.paypad.impl.Paypad;

public class CashWithdrawalActivity extends AppCompatActivity implements CashWithdrawalContract.CashWithdrawalView {

    private TextView proceed_button;
    ImageView back_btn;
    EditText amount,phone_num;
    Spinner spinner_bank;
    String selectedItem = "";
    int trans_cost,stamp_duty;

    private CashWithdrawalPresenter cashWithdrawalPresenter;

    PinpadFacade pinpadFacade;
    Handler handler;
    Paypad paypad;
    private MyBroadcastReceiver myBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_withdrawal);

        cashWithdrawalPresenter = new CashWithdrawalPresenter(this);

        initControls();
    }

    private void initControls() {


        amount = (EditText) findViewById(R.id.editText_start_date);
        phone_num = (EditText)findViewById(R.id.editText_end_date);

        spinner_bank = (Spinner) findViewById(R.id.spinner_bank);
        spinner_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItem = adapterView.getSelectedItem().toString();
                if(selectedItem.equalsIgnoreCase("Card Withdrawal")){
                    new Initialize().execute(DeviceActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String amounts = amount.getText().toString();
                String phoneNumber = phone_num.getText().toString();

                if(validateControls(CashWithdrawalActivity.this, amounts,phoneNumber)){
                    if(selectedItem.equalsIgnoreCase("Card withdrawal")){
                        getTransactionsCost(Integer.parseInt(amounts), CashWithdrawalActivity.this);
                        Bundle bundle = new Bundle();
                        bundle.putString("amount",amounts);
                        bundle.putString("phone",phoneNumber);
                        bundle.putString("trans_cost",String.valueOf(trans_cost));
                        bundle.putString("stamp_duty",String.valueOf(stamp_duty));
                        new AppNavigator(CashWithdrawalActivity.this).navigateToPaymentVerification(bundle);
                    }else{
                        new AppNavigator(CashWithdrawalActivity.this).navigateToVerificationCode();
                    }
                }

               /* String token = Preferences.getAgentToken(CashWithdrawalActivity.this);
                InitiateCashoutRequest initiateCashoutRequest = new InitiateCashoutRequest();
                initiateCashoutRequest.setAmount(Integer.parseInt(amount.getText().toString()));
                cashWithdrawalPresenter.initiateCashout(initiateCashoutRequest,token,CashWithdrawalActivity.this);*/

            }
        });

        handler = new Handler();
        pinpadFacade = new PinpadFacade(this);

        paypad = new Paypad(this);

        myBroadcastReceiver = new MyBroadcastReceiver();

        registerReceiver(myBroadcastReceiver, new IntentFilter("com.esl.paypadlib"));

    }

    private void getTransactionsCost(int amount, Context context){

        int rangeOne = Preferences.getCashoutRangeOne(context);
        int rangeTwo = Preferences.getCashoutRangeTwo(context);
        int rangeThree = Preferences.getCashoutRangeThree(context);

        if(amount <= rangeOne){
            trans_cost = Preferences.getCashoutRangeOneVal(context);
            stamp_duty = Preferences.getCashoutStampDutyOne(context);
        }else if(amount >rangeOne && amount<= rangeTwo){
            trans_cost = Preferences.getCashoutRangeTwoVal(context);
            stamp_duty = Preferences.getCashoutStampDutyTwo(context);
        }else if(amount >rangeTwo && amount <= rangeThree){
            trans_cost = Preferences.getCashoutRangeThreeVal(context);
            stamp_duty = Preferences.getCashoutStampDutyThree(context);
        }
    }

    private boolean validateControls(Context context, String amount, String phoneNumber){

        if(TextUtils.isEmpty(amount)){
            Utility.shortToast(context,getString(R.string.please_input_amount));
            return false;
        }else if(TextUtils.isEmpty(phoneNumber)){
            Utility.shortToast(context,getString(R.string.please_input_phone));
            return false;
        }

        return true;
    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(CashWithdrawalActivity.this,message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(CashWithdrawalActivity.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(CashWithdrawalActivity.this);
    }

    @Override
    public void onInitiateCashoutResponse(Context context,InitiateCashoutResponse initiateCashoutResponse) {
        String reference = initiateCashoutResponse.getData().getReference();
        String trans_cost = String.valueOf(initiateCashoutResponse.getData().getTransCost());
        String stamp_duty = String.valueOf(initiateCashoutResponse.getData().getStampDuty());
        String amounts = amount.getText().toString();
        String phoneNumber = phone_num.getText().toString();

        if(validateControls(CashWithdrawalActivity.this, amounts,phoneNumber)){
            if(selectedItem.equalsIgnoreCase("Card withdrawal")){
                Bundle bundle = new Bundle();
                bundle.putString("amount",amounts);
                bundle.putString("phone",phoneNumber);
                bundle.putString("reference",reference);
                bundle.putString("trans_cost",trans_cost);
                bundle.putString("stamp_duty",stamp_duty);
                new AppNavigator(CashWithdrawalActivity.this).navigateToPaymentVerification(bundle);
            }else{
                new AppNavigator(CashWithdrawalActivity.this).navigateToVerificationCode();
            }
        }
    }

    @Override
    public void onFailure(String message) {
        Utility.shortToast(CashWithdrawalActivity.this,message);
    }


    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.getAction() != null) {
                String s = intent.getAction();
                if (s.equals("com.esl.paypadlib")) {

                    String result = intent.getStringExtra("response");
                    String[] resultarray = intent
                            .getStringArrayExtra("responsearray");
                    String reversalResult = intent.getStringExtra("reversalResult");
                    String encryptedMessage = intent.getStringExtra("encryptedMessage");

                    if (result.equals("activatecomplete")) {
                        Utility.hideProgressDialog(CashWithdrawalActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Activated Successfully", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("invalidcode")) {
                        Utility.hideProgressDialog(CashWithdrawalActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Invalid Activation Code", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("failedactivation")) {
                        Utility.hideProgressDialog(CashWithdrawalActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Activation not Successful", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("connected")) {
                        Utility.hideProgressDialog(CashWithdrawalActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Device is connected", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("startInitializeprogress")) {
                        Utility.showProgressDialog(context,false);

                    } else if (result.equals("Initializecomplete")) {
                        Utility.hideProgressDialog(CashWithdrawalActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Initialized Successfully", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("initializenotcomplete")) {
                        Utility.hideProgressDialog(CashWithdrawalActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Initialization not Successful",
                                Toast.LENGTH_LONG).show();

                    } else if (result.equals("pinpadProcessing")) {
                        // mProgressDialog.setMessage("Please wait...");

                    } else if (result.equals("enterPIN")) {
                        // mProgressDialog.setMessage("Please, Enter PIN");


                    } else if (result.equals("PINentered")) {
                        // mProgressDialog.setMessage("PIN Entered");

                    } else if (result.equals("nibssProcessing")) {
                        // mProgressDialog.setMessage("Please wait...\n Processing Transaction");

                    } else if (result.equals("errorTranx")) {
                        // mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),
                                "Critical error occured", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("transactionresponse")) {
                        // mProgressDialog.dismiss();
                        /*
                         * The returned String array has the following element
                         * with the indices below 0-Response Code 1-Response
                         * 2-Terminal ID 3-PAN 4-Card holder name 5-Date
                         * 6-Amount 7-Transaction ID 8-RRN
                         */
                        for (int i = 0; i < resultarray.length; i++) {
                            Toast.makeText(getApplicationContext(),
                                    resultarray[i], Toast.LENGTH_SHORT).show();
                        }
                    } else if (result.equals("reversal")) {
                        // mProgressDialog.dismiss();

                        Toast.makeText(getApplicationContext(),
                                reversalResult, Toast.LENGTH_SHORT).show();

                    } else if (result.equals("fcmbresponse")) {
                        // mProgressDialog.dismiss();

//                        byte[] dectryptArray = encryptedMessage.getBytes();
//                        byte[] decarray = Base64.decodeBase64(dectryptArray);
//                        String message = null;
//                        try {
//                            message = new String(decarray,"UTF-8");
//                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
//                        }
//
//                        Log.i("Enc:", encryptedMessage);
//                        Log.i("Msg:", message);
//
//                        Toast.makeText(getApplicationContext(),
//                                message, Toast.LENGTH_LONG).show();

                    }
                }
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pinpadFacade.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    protected void onResume() {

        super.onResume();
        registerReceiver(myBroadcastReceiver, new IntentFilter(
                "com.esl.paypadlib"));
    }

    @Override
    protected void onPause() {

        super.onPause();
        unregisterReceiver(myBroadcastReceiver);
    }


    class Initialize extends AsyncTask<Class<?>, String, Long> {

        @Override
        protected Long doInBackground(Class<?>... params) {
            Class<?> classactivity = params[0];
            paypad.initialize(classactivity);

            return null;

        }

        // This is executed in the context of the main GUI thread
        protected void onPostExecute(Long result) {

        }
    }
}
