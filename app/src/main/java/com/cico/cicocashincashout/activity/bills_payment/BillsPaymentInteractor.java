package com.cico.cicocashincashout.activity.bills_payment;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.disco_list.response.DiscoListResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class BillsPaymentInteractor {

    private BillsPaymentContract.BillsPaymentListener billsPaymentListener;

    final String DISCO_LIST = "disco_list";

    public BillsPaymentInteractor(BillsPaymentContract.BillsPaymentListener billsPaymentListener){

        this.billsPaymentListener = billsPaymentListener;
    }

    public void getList(Context context, String header) {
        getDiscolistObserver(header).subscribeWith(getDiscoListObservable(context));
    }


    public Observable<DiscoListResponseModel> getDiscolistObserver(String header) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getListOfDisco("Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<DiscoListResponseModel> getDiscoListObservable(final Context context){
        return new DisposableObserver<DiscoListResponseModel>() {
            @Override
            public void onNext(DiscoListResponseModel discoListResponseModel) {

                Log.d(DISCO_LIST, "DATA" + discoListResponseModel.getData());
                if(discoListResponseModel.getStatus().equalsIgnoreCase("Error")){
                    billsPaymentListener.onFailure(discoListResponseModel.getMessage());
                }else{
                    billsPaymentListener.onSuccess(context,discoListResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(DISCO_LIST,"Error " + e);
                e.printStackTrace();
                billsPaymentListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(DISCO_LIST,"Completed");
            }
        };
    }

}
