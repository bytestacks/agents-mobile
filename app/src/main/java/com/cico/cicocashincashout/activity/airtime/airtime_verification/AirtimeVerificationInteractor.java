package com.cico.cicocashincashout.activity.airtime.airtime_verification;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.airtime.request.VendingAirtimeRequestModel;
import com.cico.cicocashincashout.model.airtime.response.AirtimeVendingResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class AirtimeVerificationInteractor {

    private AirtimeVerificationContract.AirtimeVerificationListener airtimeVerificationListener;

    final String AIRTIME_PAYMENT = "electricity_payment";


    public AirtimeVerificationInteractor(AirtimeVerificationContract.AirtimeVerificationListener airtimeVerificationListener){

        this.airtimeVerificationListener = airtimeVerificationListener;
    }

    public void makeAirtimePayment(Context context, String token, VendingAirtimeRequestModel vendingAirtimeRequestModel) {
        makePaymentObserver(token,vendingAirtimeRequestModel).subscribeWith(getPaymentObservable(context));
    }

    private Observable makePaymentObserver(String token, VendingAirtimeRequestModel vendingAirtimeRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .vendAirtime("Token " + token, vendingAirtimeRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<AirtimeVendingResponseModel> getPaymentObservable(final Context context){
        return new DisposableObserver<AirtimeVendingResponseModel>() {
            @Override
            public void onNext(AirtimeVendingResponseModel airtimeVendingResponseModel) {

                Log.d(AIRTIME_PAYMENT, "DATA" + airtimeVendingResponseModel.getData());
                if(airtimeVendingResponseModel.getStatus().equalsIgnoreCase("Successful")){
                    airtimeVerificationListener.onValidateSuccess(context,airtimeVendingResponseModel);
                }else{
                    airtimeVerificationListener.onValidateFailure(airtimeVendingResponseModel.getMessage());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(AIRTIME_PAYMENT,"Error " + e);
                e.printStackTrace();
                airtimeVerificationListener.onValidateFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(AIRTIME_PAYMENT,"Completed");
            }
        };
    }
}
