package com.cico.cicocashincashout.activity.electricity.bill_verification;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.payment_verification.PaymentVerificationFinal;
import com.cico.cicocashincashout.model.electricity.vending.request.VendingRequestModel;
import com.cico.cicocashincashout.model.electricity.vending.response.VendingResponseModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

public class ElectricityBillVerification extends AppCompatActivity implements ElectricBillContract.ElectricBillView{

    TextView proceed_button,text_amount,text_amount_phone,text_amount_total,custname,customer_address;
    ImageView back_btn;

    ElectricBillPresenter electricBillPresenter;
    String token,phone,amount,disco,meterNum,type,custName,custAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills_payment_verification);

        electricBillPresenter = new ElectricBillPresenter(this);

        getBundleExtras();

        initControls();
    }

    private void initControls() {

        token = Preferences.getAgentToken(ElectricityBillVerification.this);
        phone = Preferences.getAgentPhone(ElectricityBillVerification.this);

        customer_address = (TextView) findViewById(R.id.customer_address);
        customer_address.setText(custAddress);

        custname = (TextView) findViewById(R.id.custname);
        custname.setText(custName);

        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(amount);

        text_amount_phone = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_phone.setText(meterNum);

        text_amount_total = (TextView) findViewById(R.id.text_airtime_network);
        text_amount_total.setText(amount);

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VendingRequestModel vendingRequestModel = new VendingRequestModel();
                vendingRequestModel.setAmount(Integer.parseInt(amount));
                vendingRequestModel.setDisco(disco);
                vendingRequestModel.setMeterNumber(meterNum);
                vendingRequestModel.setPhone(phone);
                vendingRequestModel.setType(type);
                electricBillPresenter.makePayment(ElectricityBillVerification.this,token,vendingRequestModel);
            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ElectricityBillVerification.this, message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(ElectricityBillVerification.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(ElectricityBillVerification.this);
    }

    @Override
    public void onVendingSuccess(VendingResponseModel vendingResponseModel) {
        String transactionToken = vendingResponseModel.getData().getToken();
        Bundle bundle = new Bundle();
        bundle.putString("reference",transactionToken);
        bundle.putString("tran_type","Eletricity Payment");
        bundle.putString("amount",amount);
        bundle.putString("result","Success");
        new AppNavigator(ElectricityBillVerification.this).navigateToSuccess(bundle);
    }

    @Override
    public void onVendingFailed(String token) {

        Bundle bundle = new Bundle();
        bundle.putString("reference", token);
        bundle.putString("tran_type","Mobile Data");
        bundle.putString("amount",amount);
        bundle.putString("result","fail");
        new AppNavigator(ElectricityBillVerification.this).navigateToSuccess(bundle);

    }

    private void getBundleExtras(){

        amount = getIntent().getStringExtra("amount");
        disco = getIntent().getStringExtra("companyname");
        meterNum = getIntent().getStringExtra("meternumber");
        type = getIntent().getStringExtra("type");
        custName = getIntent().getStringExtra("custname");
        custAddress = getIntent().getStringExtra("custAdd");
    }
}
