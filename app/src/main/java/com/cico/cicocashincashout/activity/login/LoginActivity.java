package com.cico.cicocashincashout.activity.login;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.login.request.LoginRequestModel;
import com.cico.cicocashincashout.model.login.request.User;
import com.cico.cicocashincashout.utils.AppConstants;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Utility;

public class LoginActivity extends AppCompatActivity implements LoginContract.LoginView {

    EditText txt_username,text_password;
    Bitmap bitmap;

    LoginPresenter loginPresenter;
    CircularProgressButton btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenter = new LoginPresenter(this);

        initControls();
    }


    @Override
    public void showToast(String message) {
        Utility.shortToast(LoginActivity.this,message);
    }

    @Override
    public void showProgress() {
        // Utility.showProgressDialog(LoginActivity.this,false);
        btn.startAnimation();
    }

    @Override
    public void hideProgress() {
        // Utility.hideProgressDialog(LoginActivity.this);
        btn.revertAnimation();
    }

    @Override
    public void loginSuccess() {
        btn.doneLoadingAnimation(R.color.cico_grey, bitmap);
        new AppNavigator(LoginActivity.this).navigateToMainScreen();
    }

    @Override
    public void changePassword() {
       new AppNavigator(LoginActivity.this).navigateToCreatePassword();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.onDestroy();
    }

    private void initControls(){

        text_password = (EditText) findViewById(R.id.text_password);
        txt_username = (EditText) findViewById(R.id.txt_username);

        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_done_white_48dp);

        btn = (CircularProgressButton) findViewById(R.id.btn_id);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = txt_username.getText().toString();
                String password = text_password.getText().toString();

                User user = new User();
                user.setPassword(password);
                user.setPhone(phone);

                LoginRequestModel loginRequestModel = new LoginRequestModel();
                loginRequestModel.setUser(user);
                loginRequestModel.setType(AppConstants.USER_TYPE);

                loginPresenter.handleLoginAction(LoginActivity.this, loginRequestModel);
            }
        });


    }
}
