package com.cico.cicocashincashout.activity.fund_wallet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.DeviceActivity;
import com.cico.cicocashincashout.model.fund_wallet.request.FundWalletRequest;
import com.cico.cicocashincashout.model.fund_wallet.response.FundWalletResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.paypad.cardreader.facade.PinpadFacade;
import com.paypad.impl.Paypad;


public class FundWalletActivity extends AppCompatActivity implements FundWalletContract.FundWalletView {

    EditText fund_wallet;
    TextView proceed_button;

    FundWalletPresenter fundWalletPresenter;
    String reference,amount;

    PinpadFacade pinpadFacade;
    Handler handler;
    Paypad paypad;
    private MyBroadcastReceiver myBroadcastReceiver;


    private void initializeControls(){

        fund_wallet = (EditText) findViewById(R.id.wallet_amount);
        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                amount = fund_wallet.getText().toString();
                new FundWalletActivity.Initialize().execute(DeviceActivity.class);
            }
        });

        handler = new Handler();
        pinpadFacade = new PinpadFacade(this);

        paypad = new Paypad(this);

        myBroadcastReceiver = new MyBroadcastReceiver();

        registerReceiver(myBroadcastReceiver, new IntentFilter("com.esl.paypadlib"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fund_wallet_activity);

        initializeControls();

        fundWalletPresenter = new FundWalletPresenter(this);
    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(FundWalletActivity.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(FundWalletActivity.this);
    }

    @Override
    public void showError(String token) {
        Bundle bundle = new Bundle();
        bundle.putString("result","fail");
        bundle.putString("reference", reference);
        bundle.putString("tran_type","Fund Wallet");
        bundle.putString("amount",amount);
        new AppNavigator(FundWalletActivity.this).navigateToSuccess(bundle);
    }

    @Override
    public void onSuccess(FundWalletResponse fundWalletResponse) {
        Bundle bundle = new Bundle();
        bundle.putString("result","success");
        bundle.putString("reference", reference);
        bundle.putString("tran_type","Fund Wallet");
        bundle.putString("amount",amount);
        new AppNavigator(FundWalletActivity.this).navigateToSuccess(bundle);

    }


    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.getAction() != null) {
                String s = intent.getAction();
                if (s.equals("com.esl.paypadlib")) {

                    String result = intent.getStringExtra("response");
                    String[] resultarray = intent
                            .getStringArrayExtra("responsearray");
                    String reversalResult = intent.getStringExtra("reversalResult");
                    String encryptedMessage = intent.getStringExtra("encryptedMessage");

                    if (result.equals("activatecomplete")) {
                        Utility.hideProgressDialog(FundWalletActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Activated Successfully", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("invalidcode")) {
                        Utility.hideProgressDialog(FundWalletActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Invalid Activation Code", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("failedactivation")) {
                        Utility.hideProgressDialog(FundWalletActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Activation not Successful", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("connected")) {
                        Utility.hideProgressDialog(FundWalletActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Device is connected", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("startInitializeprogress")) {
                        Utility.showProgressDialog(context,false);

                    } else if (result.equals("Initializecomplete")) {
                        Utility.hideProgressDialog(FundWalletActivity.this);
                        Utility.shortToast(FundWalletActivity.this,getString(R.string.please_insert_card));
                        paypad.makePayment(amount, "savings");
                    } else if (result.equals("initializenotcomplete")) {
                        Utility.hideProgressDialog(FundWalletActivity.this);
                        Toast.makeText(getApplicationContext(),
                                "Initialization not Successful",
                                Toast.LENGTH_LONG).show();

                    } else if (result.equals("pinpadProcessing")) {
                        // mProgressDialog.setMessage("Please wait...");

                    } else if (result.equals("enterPIN")) {
                        // mProgressDialog.setMessage("Please, Enter PIN");


                    } else if (result.equals("PINentered")) {
                        // mProgressDialog.setMessage("PIN Entered");

                    } else if (result.equals("nibssProcessing")) {
                        // mProgressDialog.setMessage("Please wait...\n Processing Transaction");

                    } else if (result.equals("errorTranx")) {
                        // mProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),
                                "Critical error occured", Toast.LENGTH_LONG)
                                .show();
                    } else if (result.equals("  ")) {
                        String header = Preferences.getAgentToken(FundWalletActivity.this);
                        reference = Utility.randomNumber(10);

                        FundWalletRequest fundWalletRequest = new FundWalletRequest();
                        fundWalletRequest.setAmount(Integer.parseInt(amount));
                        fundWalletRequest.setReference("CICO_" + reference);

                        fundWalletPresenter.fundWallet(FundWalletActivity.this,header, fundWalletRequest);
                    } else if (result.equals("reversal")) {
                        // mProgressDialog.dismiss();

                        Toast.makeText(getApplicationContext(),
                                reversalResult, Toast.LENGTH_SHORT).show();

                    } else if (result.equals("fcmbresponse")) {

                    }
                }
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pinpadFacade.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    protected void onResume() {

        super.onResume();
        registerReceiver(myBroadcastReceiver, new IntentFilter(
                "com.esl.paypadlib"));
    }

    @Override
    protected void onPause() {

        super.onPause();
        unregisterReceiver(myBroadcastReceiver);
    }

    class Initialize extends AsyncTask<Class<?>, String, Long> {

        @Override
        protected Long doInBackground(Class<?>... params) {
            Class<?> classactivity = params[0];
            paypad.initialize(classactivity);

            return null;

        }

        // This is executed in the context of the main GUI thread
        protected void onPostExecute(Long result) {

        }
    }
}
