package com.cico.cicocashincashout.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;

public class AgentFundTransferActivity extends AppCompatActivity {

    EditText amount_text,account_number;
    TextView proceed_button;
    ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_fund_transfer);

        initControls();
    }

    private void initControls() {

        amount_text = (EditText) findViewById(R.id.amount_text);
        account_number = (EditText) findViewById(R.id.account_number);

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(AgentFundTransferActivity.this).navigateToAgentTransferFundVerification();
            }
        });
        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
