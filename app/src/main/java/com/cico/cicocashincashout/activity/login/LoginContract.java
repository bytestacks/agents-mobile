package com.cico.cicocashincashout.activity.login;

import android.content.Context;

import com.cico.cicocashincashout.model.login.response.LoginResponseModel;

public interface LoginContract {

    interface LoginView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void loginSuccess();

        void changePassword();
    }

    interface LoginListener{

        void onSuccess(Context context, LoginResponseModel loginResponseModel);

        void onFailure(String message);
    }
}
