package com.cico.cicocashincashout.activity.electricity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.electricity.request.ValidateMeterNumberRequest;
import com.cico.cicocashincashout.model.electricity.response.ValidateMeterNumberResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

public class ActivityElectricPayment extends AppCompatActivity implements ElectricityContract.ElectricityView {

    ImageView back_btn,imageView;
    EditText meter_number,cust_text;
    Spinner plan_type;
    CircularProgressButton proceed_button;

    ElectricityPresenter electricityPresenter;
    String userToken,meterNum,planType,discoName,amount,companyName,discoCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills_electricity );

        electricityPresenter = new ElectricityPresenter(this);

        initControls();
    }

    private void initControls() {

        discoCode = getIntent().getStringExtra("disco_code");
        companyName = getIntent().getStringExtra("disco");
        int drawableId = getDrawableLogo(companyName);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(drawableId);

        meter_number = (EditText) findViewById(R.id.meter_number);
        cust_text = (EditText) findViewById(R.id.cust_text);

        plan_type = (Spinner) findViewById(R.id.plan_type);

        proceed_button = (CircularProgressButton) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                meterNum = meter_number.getText().toString().trim();
                planType= plan_type.getSelectedItem().toString();
                discoName = discoCode;
                amount = cust_text.getText().toString();
                userToken = Preferences.getAgentToken(ActivityElectricPayment.this);

                ValidateMeterNumberRequest validateMeterNumberRequest = new ValidateMeterNumberRequest();
                validateMeterNumberRequest.setDisco(discoName);
                validateMeterNumberRequest.setMeterNumber(meterNum);
                validateMeterNumberRequest.setType(planType);

                electricityPresenter.validateMeterNumber(ActivityElectricPayment.this,
                        validateMeterNumberRequest,userToken);

            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityElectricPayment.this,message);
    }

    @Override
    public void showProgress() {
        //Utility.showProgressDialog(ActivityElectricPayment.this, false);
        proceed_button.startAnimation();
    }

    @Override
    public void hideProgress() {
        //Utility.hideProgressDialog(ActivityElectricPayment.this);
        proceed_button.revertAnimation();
    }

    @Override
    public void validateMeterNumberSuccess(ValidateMeterNumberResponse validateMeterNumberResponse) {
        Bundle bundle = new Bundle();
        bundle.putString("amount", amount);
        bundle.putString("meternumber",meterNum);
        bundle.putString("companyname",discoName);
        bundle.putString("type",planType);
        bundle.putString("custname", validateMeterNumberResponse.getData().getName());
        bundle.putString("custAdd",validateMeterNumberResponse.getData().getAddress());
        new AppNavigator(ActivityElectricPayment.this).navigateToElectricBillVerification(bundle);
    }

    private int getDrawableLogo(String companyName){
        if(companyName.equalsIgnoreCase("Ibadan Electric")){
            return R.drawable.ibedc;
        }else if(companyName.equalsIgnoreCase("Ikeja Electric")){
            return R.drawable.ikedc;
        }else if(companyName.equalsIgnoreCase("Eko Electric")){
            return R.drawable.ekedc;
        }

        return 0;
    }
}
