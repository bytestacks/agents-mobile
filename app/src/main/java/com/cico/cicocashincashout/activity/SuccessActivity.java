package com.cico.cicocashincashout.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppConstants;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Utility;

public class SuccessActivity extends AppCompatActivity {

    private ImageView img_view,imageView12;
    private TextView transaction_token,text_message;
    private String message, bundle_amount,bundle_terminal_id,bundle_pan,
            bundle_rrn,bundle_reference,bundle_card_no,
            bundle_provider_ref,bundle_holder_name,bundle_message,bundle_type,bundle_stan;

    private TextView response_code,text_rrn,text_stan,text_pan,text_terminal_id,
            transaction_id,transaction_date,text_cash_out,proceed_button,text_amount,text_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_success);
        initControls();
    }

    @SuppressLint("SetTextI18n")
    private void initControls() {

        Bundle extras = getIntent().getExtras();
        setBundleValues(extras);

        response_code = (TextView) findViewById(R.id.response_code);

        text_rrn = (TextView) findViewById(R.id.text_rrn);
        text_rrn.setText(bundle_rrn);

        text_stan = (TextView) findViewById(R.id.text_stan);
        text_stan.setText(bundle_stan);

        text_pan = (TextView) findViewById(R.id.text_pan);
        text_pan.setText(bundle_pan);

        text_terminal_id = (TextView) findViewById(R.id.text_terminal_id);
        text_terminal_id.setText(bundle_terminal_id);

        transaction_id = (TextView) findViewById(R.id.transaction_id);
        transaction_id.setText(bundle_reference);

        transaction_date = (TextView) findViewById(R.id.transaction_date);
        transaction_date.setText(Utility.getPresentDate());

        text_cash_out = (TextView) findViewById(R.id.text_cash_out);
        text_cash_out.setText(bundle_type);

        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(AppConstants.CURRENCY_NAIRA + bundle_amount + ".00");

        text_details = (TextView) findViewById(R.id.text_details);
        text_details.setText("");

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AppNavigator(SuccessActivity.this).navigateToMainScreen();
            }
        });

        imageView12  = (ImageView) findViewById(R.id.imageView12);

        if(bundle_message.equalsIgnoreCase("Success")){
            text_details.setText(R.string.trans_success);
            text_details.setTextColor(getResources().getColor(R.color.cico_green));
            imageView12.setImageResource(R.drawable.ic_check_circle_black_green_24dp);
        }else{
            text_details.setText(R.string.trnas_failed);
            text_details.setTextColor(getResources().getColor(R.color.cico_red));
            imageView12.setImageResource(R.drawable.ic_cancel_black_wrong_24dp);
        }
    }

    private void setBundleValues(Bundle bundleValues){

        if(bundleValues == null){
            bundle_message = "";
            bundle_terminal_id = "";
            bundle_amount = "";
            bundle_pan = "";
            bundle_rrn ="";
            bundle_reference = "";
            bundle_card_no = "";
            bundle_provider_ref = "";
            bundle_holder_name = "";
        }else{
            getValues(bundleValues);
        }
    }

    private void getValues(Bundle bundle){
        getResult(bundle);
        getTerminalId(bundle);
        getAmount(bundle);
        getPan(bundle);
        getRrn(bundle);
        getReference(bundle);
        getCardNo(bundle);
        getProviderRef(bundle);
        getHolderName(bundle);
        getBuildType(bundle);

    }

    private void getResult(Bundle bundle){
        if(bundle.containsKey("result")){
            bundle_message =  bundle.getString("result");
        }else{
            bundle_message = "";
        }
    }

    private void getTerminalId(Bundle bundle){
        if(bundle.containsKey("terminal_id")){
            bundle_terminal_id =  bundle.getString("terminal_id");
        }else{
            bundle_terminal_id = "";
        }
    }

    private void getAmount(Bundle bundle){
        if(bundle.containsKey("amount")){
            bundle_amount =  bundle.getString("amount");
        }else{
            bundle_amount = "";
        }
    }

    private void getPan(Bundle bundle){
        if(bundle.containsKey("pan")){
            bundle_pan =  bundle.getString("pan");
        }else{
            bundle_pan = "";
        }
    }

    private void getRrn(Bundle bundle){
        if(bundle.containsKey("rrn")){
            bundle_rrn =  bundle.getString("rrn");
        }else{
            bundle_rrn = "";
        }
    }

    private void getReference(Bundle bundle){
        if(bundle.containsKey("reference")){
            bundle_reference =  bundle.getString("reference");
        }else{
            bundle_reference = "";
        }
    }

    private void getCardNo(Bundle bundle){
        if(bundle.containsKey("card_no")){
            bundle_card_no =  bundle.getString("card_no");
        }else{
            bundle_card_no = "";
        }
    }

    private void getProviderRef(Bundle bundle){
        if(bundle.containsKey("provider_ref")){
            bundle_provider_ref =  bundle.getString("provider_ref");
        }else{
            bundle_provider_ref = "";
        }
    }

    private void getHolderName(Bundle bundle){
        if(bundle.containsKey("holder_name")){
            bundle_holder_name =  bundle.getString("holder_name");
        }else{
            bundle_holder_name = "";
        }
    }

    private void getBuildType(Bundle bundle){
        if(bundle.containsKey("tran_type")){
            bundle_type =  bundle.getString("tran_type");
        }else{
            bundle_type = "";
        }
    }
}
