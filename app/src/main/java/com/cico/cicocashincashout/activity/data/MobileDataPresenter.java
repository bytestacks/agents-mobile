package com.cico.cicocashincashout.activity.data;

import android.content.Context;

import com.cico.cicocashincashout.model.data.request.DataPlanRequest;
import com.cico.cicocashincashout.model.data.response.DataPlanResponse;
import com.cico.cicocashincashout.utils.Preferences;
import com.google.gson.Gson;

public class MobileDataPresenter implements MobileDataContract.DataVerificationListener{


    private MobileDataContract.DataVerificationView dataVerificationView;
    private MobileDataInteractor mobileDataInteractor;

    MobileDataPresenter(MobileDataContract.DataVerificationView dataVerificationView){
        this.dataVerificationView = dataVerificationView;
        mobileDataInteractor = new MobileDataInteractor(this);
    }


    @Override
    public void onSuccess(Context context, DataPlanResponse dataPlanResponse) {
        dataVerificationView.hideProgress();
        savePlanPriceList(context,dataPlanResponse);
    }

    @Override
    public void onFailure(String message) {
        dataVerificationView.hideProgress();
        dataVerificationView.showToast(message);

    }

    public void getDataPlan(Context context, String token, DataPlanRequest dataPlanRequest){
        dataVerificationView.showProgress();
        mobileDataInteractor.getDataPlan(context,token, dataPlanRequest);
    }

    private void savePlanPriceList(Context context, DataPlanResponse dataPlanResponse){
        Gson gson = new Gson();
        String dataList = gson.toJson(dataPlanResponse);
        Preferences.setDataPlan(context, dataList);
    }

}
