package com.cico.cicocashincashout.activity.bills_payment;

import android.content.Context;

import com.cico.cicocashincashout.model.disco_list.response.DiscoListResponseModel;

public interface BillsPaymentContract {

    interface BillsPaymentlView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void onSuccess(DiscoListResponseModel discoListResponseModel);
    }

    interface BillsPaymentListener{

        void onSuccess(Context context, DiscoListResponseModel discoListResponseModel);

        void onFailure(String message);
    }
}
