package com.cico.cicocashincashout.activity.data;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.data.vending.DataVerificationContract;
import com.cico.cicocashincashout.activity.tv_subscription.ActivityTvSubscription;
import com.cico.cicocashincashout.model.data.request.DataPlanRequest;
import com.cico.cicocashincashout.model.data.response.DataPlanDatumModel;
import com.cico.cicocashincashout.model.data.response.DataPlanResponse;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListDatum;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class ActivityMobileData extends AppCompatActivity implements MobileDataContract.DataVerificationView{

    MobileDataPresenter mobileDataPresenter;
    ImageView back_btn,imageView;
    EditText meter_number;
    Spinner plan_type;
    TextView proceed_button;
    String telcoName, telcoCode, dataAmount, dataPlan, display_amount, phone;


    private boolean isSpinnerTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_data);

        mobileDataPresenter = new MobileDataPresenter(this);

        initControls();
    }

    private void initControls() {

        meter_number = (EditText) findViewById(R.id.meter_number);
        plan_type = (Spinner) findViewById(R.id.plan_type);

        plan_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isSpinnerTouched = true;
                return false;
            }
        });

        plan_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(isSpinnerTouched) {

                    String selectedItem = adapterView.getItemAtPosition(i).toString();
                    if (!selectedItem.equalsIgnoreCase("--Select DataPlan--")) {
                        display_amount = selectedItem;
                        DataPlanResponse dataPlanResponse = null;
                        Gson gson = new Gson();
                        dataPlanResponse = gson.fromJson(dataPlan,DataPlanResponse.class);
                        List<DataPlanDatumModel> dataPlanDatumModelList = dataPlanResponse.getData();
                        DataPlanDatumModel dataPlanDatumModel = dataPlanDatumModelList.get(i-1);
                        dataAmount = dataPlanDatumModel.getAmount();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone = meter_number.getText().toString().trim();

                if (phone.length() > 0 && phone.length() < 11) {
                    showToast("Invalid phone number");

                }else if(display_amount.equalsIgnoreCase("--Select DataPlan--")){
                    showToast("Select a Data Plan");
                }else {
                Bundle bundle = new Bundle();
                bundle.putString("amount", dataAmount);
                bundle.putString("display_amount", display_amount);
                bundle.putString("phone",phone);
                bundle.putString("telco_code",telcoCode);
                bundle.putString("telco",telcoName);
                new AppNavigator(ActivityMobileData.this).navigateToDataVerification(bundle);
                }
            }
        });

        imageView = (ImageView) findViewById(R.id.imageView);

        telcoName = getIntent().getStringExtra("telco");
        telcoCode = getIntent().getStringExtra("telcoCode");


        int drawableId = getDrawableLogo(telcoName);

        imageView.setImageResource(drawableId);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        loadDataPlan(telcoCode, ActivityMobileData.this);

    }


    private void loadDataPlan(String telcoCode, Context context) {
        String dataPlanPackage = Preferences.getDataPlan(context);
        String token = Preferences.getAgentToken(ActivityMobileData.this);
        if (TextUtils.isEmpty(dataPlanPackage)) {
            DataPlanRequest dataPlanRequest = new DataPlanRequest();
            dataPlanRequest.setTelco(telcoCode);
            mobileDataPresenter.getDataPlan(ActivityMobileData.this, token, dataPlanRequest);
        } else {
            loadDataPlanFromPrefences(context);
        }
    }

    private void loadDataPlanFromPrefences(Context context){

        DataPlanResponse dataPlanResponse = null;
        Gson gson = new Gson();
        dataPlan = Preferences.getDataPlan(context);
        if(!TextUtils.isEmpty(dataPlan)){
            dataPlanResponse = gson.fromJson(dataPlan,DataPlanResponse.class);
            List<String> dataPlanList = new ArrayList<String>();
            List<DataPlanDatumModel> dataPlanDatumModelList = dataPlanResponse.getData();
            for(int i = 0; i<dataPlanDatumModelList.size(); i++){
                DataPlanDatumModel dataPlanDatumModel = dataPlanDatumModelList.get(i);
                String planName = dataPlanDatumModel.getDatabundle() + "("+ dataPlanDatumModel.getValidity() +") - " + dataPlanDatumModel.getAmount();
                dataPlanList.add(planName);
            }
            dataPlanList.add(0,"--Select DataPlan--");
            ArrayAdapter planAdapter = new ArrayAdapter(this,R.layout.spiner_images_item, dataPlanList);
            plan_type.setAdapter(planAdapter);
        }

    }

    private int getDrawableLogo(String companyName){
        if(companyName.equalsIgnoreCase("MTN")){
            return R.drawable.mtn;
        }else if(companyName.equalsIgnoreCase("Glo")){
            return R.drawable.glo;
        }else if(companyName.equalsIgnoreCase("9Mobile")){
            return R.drawable.etisalat;
        }else if(companyName.equalsIgnoreCase("Airtel")){
            return R.drawable.airtel;
        }

        return 0;
    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityMobileData.this, message);

    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(ActivityMobileData.this,false);

    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(ActivityMobileData.this);

    }

    @Override
    public void dataPlanSuccess(DataPlanResponse dataPlanResponse) {

    }
}
