package com.cico.cicocashincashout.activity.cash_withdrawal;

import android.content.Context;

import com.cico.cicocashincashout.model.cashout.response.InitiateCashoutResponse;

public interface CashWithdrawalContract {

    interface CashWithdrawalView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void onFailure(String message);

        void onInitiateCashoutResponse(Context context,InitiateCashoutResponse initiateCashoutResponse);

        //void validateMeterNumberSuccess(ValidateMeterNumberResponse validateMeterNumberResponse);
    }

    interface CashWithdrawalListener{

        void onInitiateCashoutResponse(Context context,InitiateCashoutResponse initiateCashoutResponse);

        void onValidateSuccess(Context context);

        void onValidateFailure(String message);

        void onFailure(String message);
    }
}
