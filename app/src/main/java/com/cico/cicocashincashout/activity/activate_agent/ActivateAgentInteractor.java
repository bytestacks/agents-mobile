package com.cico.cicocashincashout.activity.activate_agent;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.agent.request.AgentActivateRequestModel;
import com.cico.cicocashincashout.model.agent.response.AgentActivateResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class ActivateAgentInteractor {

    private ActivateAgentContract.ActivateAgentListener activateAgentListener;

    final String ACTIVATE_AGENT = "activate_agent";

    public ActivateAgentInteractor(ActivateAgentContract.ActivateAgentListener activateAgentListener){

        this.activateAgentListener = activateAgentListener;
    }

    public void validateAgentDetails(Context context, final AgentActivateRequestModel agentActivateRequestModel){

        if(hasErrors(context,agentActivateRequestModel)){
            return;
        }
        activateAgent(context, agentActivateRequestModel);
    }

    private void activateAgent(Context context, AgentActivateRequestModel agentActivateRequestModel) {

        activateUserObserver(agentActivateRequestModel).subscribeWith(getLoginUserObservable(context));
    }

    public DisposableObserver<AgentActivateResponseModel> getLoginUserObservable(final Context context){
        return new DisposableObserver<AgentActivateResponseModel>() {
            @Override
            public void onNext(AgentActivateResponseModel agentActivateResponseModel) {

                Log.d(ACTIVATE_AGENT, "DATA" + agentActivateResponseModel.getData());
                if(agentActivateResponseModel.getStatus().equalsIgnoreCase("Error")){
                    activateAgentListener.onFailure(agentActivateResponseModel.getMessage());
                }else{
                    activateAgentListener.onSuccess(context,agentActivateResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(ACTIVATE_AGENT,"Error " + e);
                if(e instanceof HttpException){
                    activateAgentListener.onFailure("Agent details not found!");
                }else{

                    activateAgentListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(ACTIVATE_AGENT,"Completed");
            }
        };
    }

    public Observable<AgentActivateResponseModel> activateUserObserver(AgentActivateRequestModel agentActivateRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .activateAgent(agentActivateRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    private boolean hasErrors(Context context, AgentActivateRequestModel agentActivateRequestModel) {

        String phone = agentActivateRequestModel.getPhone();
        String code = agentActivateRequestModel.getCode();

        if(TextUtils.isEmpty(phone)){
            activateAgentListener.onFailure(context.getString(R.string.phone_empty));
            return false;
        }else if(TextUtils.isEmpty(code)){
            activateAgentListener.onFailure(context.getString(R.string.code_empty));
            return true;
        }
        return false;
    }
}
