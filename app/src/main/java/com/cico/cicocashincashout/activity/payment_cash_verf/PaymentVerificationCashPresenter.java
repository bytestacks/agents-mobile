package com.cico.cicocashincashout.activity.payment_cash_verf;

import android.content.Context;
import com.cico.cicocashincashout.model.cashout.request.CashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.CashoutResponse;

public class PaymentVerificationCashPresenter implements PaymentVerificationCashContract.PaymentVerificationCashListener{

    private PaymentVerificationCashContract.PaymentVerificationCashView paymentVerificationView;
    private PaymentVerificationCashInteractor paymentVerificationInteractor;

    public PaymentVerificationCashPresenter(PaymentVerificationCashContract.PaymentVerificationCashView paymentVerificationView){
        this.paymentVerificationView = paymentVerificationView;
        paymentVerificationInteractor = new PaymentVerificationCashInteractor(this);
    }


    @Override
    public void onSuccess(Context context, CashoutResponse cashoutResponse) {
        paymentVerificationView.hideProgress();
        paymentVerificationView.onSuccess(cashoutResponse);
    }

    @Override
    public void onFailure(String message) {
        paymentVerificationView.hideProgress();
        paymentVerificationView.showError(message);
    }

    public void logCashout(Context context, String token, CashoutRequest cashoutRequest){
        paymentVerificationView.showProgress();
        paymentVerificationInteractor.logFund(context, token, cashoutRequest);
    }
}
