package com.cico.cicocashincashout.activity.fund_wallet;

import android.content.Context;

import com.cico.cicocashincashout.model.fund_wallet.request.FundWalletRequest;
import com.cico.cicocashincashout.model.fund_wallet.response.FundWalletResponse;

public class FundWalletPresenter implements FundWalletContract.FundWalletListener {

    private FundWalletContract.FundWalletView fundWalletView;
    private FundWalletInteractor fundWalletInteractor;

    public FundWalletPresenter(FundWalletContract.FundWalletView fundWalletView){

        this.fundWalletView = fundWalletView;
        fundWalletInteractor = new FundWalletInteractor(this);
    }

    @Override
    public void onSuccess(Context context, FundWalletResponse fundWalletResponse) {

        fundWalletView.hideProgress();
        fundWalletView.onSuccess(fundWalletResponse);
    }

    @Override
    public void onFailure(String message) {

        fundWalletView.hideProgress();
        fundWalletView.showError(message);

    }

    public void fundWallet(Context context, String header, FundWalletRequest fundWalletRequest){
        fundWalletView.showProgress();
        fundWalletInteractor.fundWallet(context,header,fundWalletRequest);
    }
}
