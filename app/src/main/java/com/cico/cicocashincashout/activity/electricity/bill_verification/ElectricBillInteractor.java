package com.cico.cicocashincashout.activity.electricity.bill_verification;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.electricity.vending.request.VendingRequestModel;
import com.cico.cicocashincashout.model.electricity.vending.response.VendingResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ElectricBillInteractor {

    private ElectricBillContract.ElectricBillListener electricBillListener;

    final String ELECTRICTY_PAYMENT = "electricity_payment";


    public ElectricBillInteractor(ElectricBillContract.ElectricBillListener electricityListener){

        this.electricBillListener = electricityListener;
    }

    public void makeElectricPayment(Context context,String token, VendingRequestModel vendingRequestModel) {
        makePaymentObserver(token,vendingRequestModel).subscribeWith(getPaymentObservable(context));
    }

    private Observable makePaymentObserver(String token, VendingRequestModel vendingRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .makePayment("Token " + token, vendingRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<VendingResponseModel> getPaymentObservable(final Context context){
        return new DisposableObserver<VendingResponseModel>() {
            @Override
            public void onNext(VendingResponseModel vendingResponseModel) {

                Log.d(ELECTRICTY_PAYMENT, "DATA" + vendingResponseModel.getData());
                if(vendingResponseModel.getStatus().equalsIgnoreCase("Successful")){
                    electricBillListener.onValidateSuccess(context,vendingResponseModel);
                }else{
                    electricBillListener.onValidateFailure(vendingResponseModel.getMessage());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(ELECTRICTY_PAYMENT,"Error " + e);
                e.printStackTrace();
                electricBillListener.onValidateFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(ELECTRICTY_PAYMENT,"Completed");
            }
        };
    }
}
