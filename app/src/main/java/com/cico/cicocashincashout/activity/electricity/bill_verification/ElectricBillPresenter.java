package com.cico.cicocashincashout.activity.electricity.bill_verification;

import android.content.Context;
import com.cico.cicocashincashout.model.electricity.vending.request.VendingRequestModel;
import com.cico.cicocashincashout.model.electricity.vending.response.VendingResponseModel;

public class ElectricBillPresenter implements ElectricBillContract.ElectricBillListener {

    private ElectricBillContract.ElectricBillView electricityView;
    private ElectricBillInteractor electricityInteractor;

    public ElectricBillPresenter(ElectricBillContract.ElectricBillView electricityView){
        this.electricityView = electricityView;
        electricityInteractor = new ElectricBillInteractor(this);
    }

    @Override
    public void onValidateSuccess(Context context, VendingResponseModel vendingResponseModel) {
        electricityView.hideProgress();
        electricityView.onVendingSuccess(vendingResponseModel);
    }

    @Override
    public void onValidateFailure(String message) {
        electricityView.hideProgress();
        electricityView.onVendingFailed("");
        //electricityView.showToast(message);
    }

    public void makePayment(Context context,String token, VendingRequestModel vendingRequestModel){
        electricityView.showProgress();
        electricityInteractor.makeElectricPayment(context,token, vendingRequestModel);
    }
}
