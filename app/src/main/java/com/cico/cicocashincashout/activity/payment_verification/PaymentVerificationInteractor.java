package com.cico.cicocashincashout.activity.payment_verification;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.Bank.request.DisburseFundRequest;
import com.cico.cicocashincashout.model.Bank.response.DisburseResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class PaymentVerificationInteractor {

    private PaymentVerificationContract.PaymentVerificationListener paymentVerificationListener;

    final String FUND_TRANSFER = "Fund_Transfer";

    public PaymentVerificationInteractor(PaymentVerificationContract.PaymentVerificationListener paymentVerificationListener){

        this.paymentVerificationListener = paymentVerificationListener;
    }

    public void disburseFund(Context context,String token, DisburseFundRequest disburseFundRequest) {

        validateAccountNumberObserver(token, disburseFundRequest).subscribeWith(validateAccountNumber(context));
    }


    public Observable<DisburseResponse> validateAccountNumberObserver(String header, DisburseFundRequest disburseFundRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .disburseFund("Token " + header,disburseFundRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<DisburseResponse> validateAccountNumber(final Context context){
        return new DisposableObserver<DisburseResponse>() {
            @Override
            public void onNext(DisburseResponse bankListResponse) {

                Log.d(FUND_TRANSFER, "DATA" + bankListResponse.getData());
                if(bankListResponse.getStatus().equalsIgnoreCase("Error")){
                    paymentVerificationListener.onFailure(bankListResponse.getMessage());
                }else{
                    paymentVerificationListener.onSuccess(context,bankListResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(FUND_TRANSFER,"Error " + e);
                if(e instanceof HttpException){
                    paymentVerificationListener.onFailure("Invalid account number");
                }else{

                    paymentVerificationListener.onFailure(context.getString(R.string.login_error));
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(FUND_TRANSFER,"Completed");
            }
        };
    }
}
