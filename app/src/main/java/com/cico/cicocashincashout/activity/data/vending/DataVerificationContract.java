package com.cico.cicocashincashout.activity.data.vending;

import android.content.Context;

import com.cico.cicocashincashout.model.data.vending.response.DataVendingResponseModel;

public interface DataVerificationContract {

    interface DataVerificationView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void vendDataSuccess(DataVendingResponseModel dataVendingResponseModel);
    }

    interface DataVerificationListener{

        void onSuccess(Context context, DataVendingResponseModel dataVendingResponseModel);

        void onFailure(String message);
    }
}
