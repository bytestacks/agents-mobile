package com.cico.cicocashincashout.activity.data.vending;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.tv_subscription.ActivityTvSubscription;
import com.cico.cicocashincashout.model.data.response.DataPlanResponse;
import com.cico.cicocashincashout.model.data.vending.request.DataVendingRequest;
import com.cico.cicocashincashout.model.data.vending.response.DataVendingResponseModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

public class ActivityDataVerification extends AppCompatActivity implements DataVerificationContract.DataVerificationView {

    TextView text_amount,text_amount_phone,text_amount_total;
    CircularProgressButton proceed_button;
    ImageView back_btn;
    DataVerificationPresenter dataVerificationPresenter;
    String token, amount, phone, telcoCode, telco, disply_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills_payment_verification_data);

        dataVerificationPresenter = new DataVerificationPresenter(this);
        getBundleExtras();
        initControls();
    }

    private void initControls() {

        token = Preferences.getAgentToken(ActivityDataVerification.this);


        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(disply_amount);
        text_amount_phone = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_phone.setText(phone);
        text_amount_total = (TextView) findViewById(R.id.text_airtime_network);
        text_amount_total.setText(telco);

        proceed_button = (CircularProgressButton) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataVendingRequest dataVendingRequest = new DataVendingRequest();
                dataVendingRequest.setAmount(Integer.parseInt(amount));
                dataVendingRequest.setPhone(phone);
                dataVendingRequest.setTelco(telcoCode);
                dataVerificationPresenter.vendData(ActivityDataVerification.this, token, dataVendingRequest);
                //new AppNavigator(ActivityDataVerification.this).navigateToSuccess(new Bundle() );
            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void getBundleExtras(){

        amount = getIntent().getStringExtra("amount");
        disply_amount = getIntent().getStringExtra("display_amount");
        phone = getIntent().getStringExtra("phone");
        telcoCode = getIntent().getStringExtra("telco_code");
        telco = getIntent().getStringExtra("telco");
    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityDataVerification.this, message);

    }

    @Override
    public void showProgress() {
        proceed_button.startAnimation();
    }

    @Override
    public void hideProgress() {
        proceed_button.revertAnimation();

    }

    @Override
    public void vendDataSuccess(DataVendingResponseModel dataVendingResponseModel) {
        String transactionToken = dataVendingResponseModel.getData().getMReference();
        Bundle bundle = new Bundle();
        bundle.putString("token",transactionToken);
        bundle.putString("tran_type","Mobile Data");
        bundle.putString("amount",amount);
        bundle.putString("result","success");
        new AppNavigator(ActivityDataVerification.this).navigateToSuccess(bundle);

    }


}
