package com.cico.cicocashincashout.activity.data.vending;

import android.content.Context;

import com.cico.cicocashincashout.model.data.vending.request.DataVendingRequest;
import com.cico.cicocashincashout.model.data.vending.response.DataVendingResponseModel;

public class DataVerificationPresenter implements DataVerificationContract.DataVerificationListener{


    private DataVerificationContract.DataVerificationView dataVerificationView;
    private DataVerificationInteractor dataVerificationInteractor;

    DataVerificationPresenter (DataVerificationContract.DataVerificationView dataVerificationView){
        this.dataVerificationView = dataVerificationView;
        dataVerificationInteractor = new DataVerificationInteractor(this);
    }


    @Override
    public void onSuccess(Context context, DataVendingResponseModel dataVendingResponseModel) {
        dataVerificationView.hideProgress();
        dataVerificationView.vendDataSuccess(dataVendingResponseModel);
    }

    @Override
    public void onFailure(String message) {
        dataVerificationView.hideProgress();
        dataVerificationView.showToast(message);

    }

    public void vendData(Context context, String token, DataVendingRequest dataVendingRequest){
        dataVerificationView.showProgress();
        dataVerificationInteractor.vendData(context,token, dataVendingRequest);
    }

}
