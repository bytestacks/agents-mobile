package com.cico.cicocashincashout.activity.tv_subscription;

import android.content.Context;

import com.cico.cicocashincashout.model.cable_tv.startimes.request.StartimesRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.response.StartimesResponseModel;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListResponse;
import com.cico.cicocashincashout.model.dstv.validate_number.request.DstvValidateNumberRequest;
import com.cico.cicocashincashout.model.dstv.validate_number.response.DstvValidateNumberResponse;
import com.cico.cicocashincashout.utils.Preferences;
import com.google.gson.Gson;

public class TvSubscriptionPresenter implements TvSubscriptionContract.TvSubscriptionListener {

    private TvSubscriptionContract.TvSubscriptionView tvSubscriptionView;
    private TvSubscriptionInteractor tvSubscriptionInteractor;

    public TvSubscriptionPresenter(TvSubscriptionContract.TvSubscriptionView tvSubscriptionView){
        this.tvSubscriptionView = tvSubscriptionView;
        tvSubscriptionInteractor = new TvSubscriptionInteractor(this);
    }

    @Override
    public void onValidateSuccess(Context context, StartimesResponseModel startimesResponseModel) {
        tvSubscriptionView.hideProgress();
        tvSubscriptionView.startimesValidateNumberSuccess(startimesResponseModel);
    }

    @Override
    public void onValidateDstvSuccess(Context context, DstvValidateNumberResponse dstvValidateNumberResponse) {
        tvSubscriptionView.hideProgress();
        tvSubscriptionView.dstvValidateNumberSuccess(dstvValidateNumberResponse);
    }

    @Override
    public void onValidateFailure(String message) {
        tvSubscriptionView.hideProgress();
        tvSubscriptionView.showToast(message);
    }

    @Override
    public void dstvPriceListSuccess(Context context,String cableName, DstvPriceListResponse dstvPriceListResponse) {
        tvSubscriptionView.hideProgress();
        if(cableName.equalsIgnoreCase("Dstv")){
            saveDstvPriceList(context,dstvPriceListResponse);
        }else{
            saveGotvPriceList(context,dstvPriceListResponse);
        }
        tvSubscriptionView.dstvPriceListSuccess(dstvPriceListResponse);
    }

    public void validateStartimeNumber(Context context, String header, StartimesRequestModel startimesRequestModel){
        tvSubscriptionView.showProgress();
        tvSubscriptionInteractor.validateNumber(context,header,startimesRequestModel);
    }

    public void getDstvPriceList(Context context, String header){
        tvSubscriptionView.showProgress();
        tvSubscriptionInteractor.getDstvPriceList(context, header);
    }

    public void getGotvPriceList(Context context, String header) {
        tvSubscriptionView.showProgress();
        tvSubscriptionInteractor.getGotvPriceList(context,header);
    }

    private void saveDstvPriceList(Context context, DstvPriceListResponse dstvPriceListResponse){
        Gson gson = new Gson();
        String discoList = gson.toJson(dstvPriceListResponse);
        Preferences.setDstvList(context, discoList);
    }

    private void saveGotvPriceList(Context context, DstvPriceListResponse dstvPriceListResponse){
        Gson gson = new Gson();
        String discoList = gson.toJson(dstvPriceListResponse);
        Preferences.setGotvList(context, discoList);
    }

    public void validateDstvNumber(Context context, String header, DstvValidateNumberRequest dstvValidateNumberRequest) {

        tvSubscriptionView.showProgress();
        tvSubscriptionInteractor.validateDstvNumber(context,header,dstvValidateNumberRequest);
    }
}
