package com.cico.cicocashincashout.activity.bills_verification;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.payment_verification.PaymentVerificationFinal;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.request.StartimesVendRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.response.StartimesVendResponse;
import com.cico.cicocashincashout.model.dstv.vend.request.DstvVendRequestModel;
import com.cico.cicocashincashout.model.dstv.vend.response.DstvVendResponseModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

public class ActivityBillsVerification extends AppCompatActivity implements ActivityBillsContract.ActivityBillsView{

    TextView text_amount,text_amount_phone,text_amount_total,custname;
    ImageView back_btn;

    CircularProgressButton proceed_button;

    String amount,meterNum,customerName,vendType,period,planCode;

    private ActivityBillsPresenter activityBillsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills_payment_verification_cable_tv);

        activityBillsPresenter = new ActivityBillsPresenter(this);

        initControls();
    }

    private void initControls() {

        customerName = getIntent().getStringExtra("cust_name");
        meterNum = getIntent().getStringExtra("meter_num");
        amount = getIntent().getStringExtra("amount");
        vendType = getIntent().getStringExtra("type");
        period = getIntent().getStringExtra("period");
        planCode = getIntent().getStringExtra("code");


        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(amount);

        text_amount_phone = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_phone.setText(meterNum);

        custname = (TextView) findViewById(R.id.custname);
        custname.setText(customerName);

        text_amount_total = (TextView) findViewById(R.id.text_airtime_network);
        text_amount_total.setText(amount);

        proceed_button = (CircularProgressButton) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String header = Preferences.getAgentToken(ActivityBillsVerification.this);

                if(vendType.equalsIgnoreCase("Startimes")){
                    StartimesVendRequestModel  startimesVendRequestModel = new StartimesVendRequestModel();
                    startimesVendRequestModel.setAmount(Integer.parseInt(amount));
                    startimesVendRequestModel.setSmartcard(meterNum);

                    activityBillsPresenter.vendStartime(ActivityBillsVerification.this,header,startimesVendRequestModel);
                }else{
                    DstvVendRequestModel dstvVendRequestModel = new DstvVendRequestModel();
                    dstvVendRequestModel.setAmount(Integer.parseInt(amount));
                    dstvVendRequestModel.setSmartcard(meterNum);
                    dstvVendRequestModel.setPeriod(Integer.parseInt(period));
                    dstvVendRequestModel.setService("dstv");
                    dstvVendRequestModel.setProductCode(planCode);
                    dstvVendRequestModel.setCustomerName(customerName);

                    activityBillsPresenter.vendDstv(ActivityBillsVerification.this, header, dstvVendRequestModel);
                }

            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityBillsVerification.this, message);
    }

    @Override
    public void showProgress() {
        //Utility.showProgressDialog(ActivityBillsVerification.this,false);
        proceed_button.startAnimation();
    }

    @Override
    public void hideProgress() {
        //Utility.hideProgressDialog(ActivityBillsVerification.this);
        proceed_button.revertAnimation();
    }

    @Override
    public void onFailure(String token) {
        Bundle bundle = new Bundle();
        bundle.putString("result","fail");
        bundle.putString("amount",amount);
        bundle.putString("tran_type", "CableTv Payment");
        bundle.putString("token", token);
        new AppNavigator(ActivityBillsVerification.this).navigateToSuccess(bundle);
    }

    @Override
    public void onSuccess(StartimesVendResponse startimesResponseData) {
        String transactionToken = String.valueOf(startimesResponseData.getData().getTransactionNo());
        Bundle bundle = new Bundle();
        bundle.putString("reference",transactionToken);
        bundle.putString("amount",amount);
        bundle.putString("tran_type", "CableTv Payment");
        bundle.putString("result","success");
        new AppNavigator(ActivityBillsVerification.this).navigateToSuccess(bundle);
    }

    @Override
    public void onDstvSuccess(DstvVendResponseModel dstvVendResponseModel) {
        String transactionToken = dstvVendResponseModel.getData().getStatusDescription().getTransactionRef();
        Bundle bundle = new Bundle();
        bundle.putString("reference",transactionToken);
        bundle.putString("amount",amount);
        bundle.putString("tran_type", "CableTv Payment");
        bundle.putString("result","success");
        new AppNavigator(ActivityBillsVerification.this).navigateToSuccess(bundle);
    }
}
