package com.cico.cicocashincashout.activity.airtime.airtime_verification;

import android.content.Context;
import com.cico.cicocashincashout.model.airtime.request.VendingAirtimeRequestModel;
import com.cico.cicocashincashout.model.airtime.response.AirtimeVendingResponseModel;

public class AirtimeVerificationPresenter implements AirtimeVerificationContract.AirtimeVerificationListener{

    private AirtimeVerificationContract.AirtimeVerificationView airtimeView;
    private AirtimeVerificationInteractor airtimeInteractor;

    public AirtimeVerificationPresenter(AirtimeVerificationContract.AirtimeVerificationView airtimeView){
        this.airtimeView = airtimeView;
        airtimeInteractor = new AirtimeVerificationInteractor(this);
    }

    @Override
    public void onValidateSuccess(Context context, AirtimeVendingResponseModel airtimeVendingResponseModel) {
        airtimeView.hideProgress();
        airtimeView.onVendingSuccess(airtimeVendingResponseModel);
    }

    @Override
    public void onValidateFailure(String message) {
        airtimeView.hideProgress();
        airtimeView.showToast(message);
    }

    public void makePayment(Context context, String token, VendingAirtimeRequestModel vendingAirtimeRequestModel){
        airtimeView.showProgress();
        airtimeInteractor.makeAirtimePayment(context,token, vendingAirtimeRequestModel);
    }
}
