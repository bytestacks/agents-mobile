package com.cico.cicocashincashout.activity.agent_withdrawal;

import android.content.Context;

import com.cico.cicocashincashout.model.agent_withdraw.response.AgentWithdrawResponse;

public interface AgentWithdrawalContract {

    interface AgentWithdrawalView {

        void showProgress();

        void hideProgress();

        void onFailure(String token);

        void onSuccess(AgentWithdrawResponse agentWithdrawResponse);
    }

    interface AgentWithdrawalListener{

        void onSuccess(Context context, AgentWithdrawResponse agentWithdrawResponse);

        void onFailure(String message);
    }
}
