package com.cico.cicocashincashout.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;

public class ActivityVerificationCode extends AppCompatActivity {

    TextView proceed_button,text_amount,text_amount_phone,text_amount_total,text_amount_convi;
    ImageView back_btn;
    EditText code_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_cardless);

        initControls();
    }

    private void initControls() {

        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount_phone = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_total = (TextView) findViewById(R.id.text_airtime_network);
        text_amount_convi = (TextView) findViewById(R.id.text_amount_convi);

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // new AppNavigator(ActivityVerificationCode.this).navigateToSuccess(new Bundle());
            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        code_edit = (EditText) findViewById(R.id.code_edit);

    }
}
