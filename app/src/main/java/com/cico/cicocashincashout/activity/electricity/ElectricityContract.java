package com.cico.cicocashincashout.activity.electricity;

import android.content.Context;

import com.cico.cicocashincashout.model.electricity.response.ValidateMeterNumberResponse;

public interface ElectricityContract {


    interface ElectricityView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void validateMeterNumberSuccess(ValidateMeterNumberResponse validateMeterNumberResponse);
    }

    interface ElectricityListener{

        void onValidateSuccess(Context context, ValidateMeterNumberResponse validateMeterNumberResponse);

        void onValidateFailure(String message);
    }
}
