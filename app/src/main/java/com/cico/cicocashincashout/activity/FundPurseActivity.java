package com.cico.cicocashincashout.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;

public class FundPurseActivity extends AppCompatActivity {

    private TextView proceed_button;
    ImageView back_btn;
    EditText amount,phone_num;
    Spinner spinner_bank;
    String selectedItem = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_purse);
        initControls();
    }

    private void initControls() {

        amount = (EditText) findViewById(R.id.editText_start_date);
        phone_num = (EditText)findViewById(R.id.editText_end_date);

        spinner_bank = (Spinner) findViewById(R.id.spinner_bank);
        spinner_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedItem = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedItem.equalsIgnoreCase("Card withdrawal")){

                    new AppNavigator(FundPurseActivity.this).navigateToPaymentVerification(new Bundle());
                }else{
                    new AppNavigator(FundPurseActivity.this).navigateToVerificationCode();
                }
                //loginPresenter.handleLoginAction(LoginActivity.this);
            }
        });

    }
}
