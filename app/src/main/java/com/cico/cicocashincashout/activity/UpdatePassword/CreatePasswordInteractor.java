package com.cico.cicocashincashout.activity.UpdatePassword;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.transaction_history.response.TransactionHistoryResponseModel;
import com.cico.cicocashincashout.model.user.update_password.request.ChangePasswordRequest;
import com.cico.cicocashincashout.model.user.update_password.response.ChangePasswordResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class CreatePasswordInteractor {

    private CreatePasswordContract.CreatePasswordListener createPasswordListener;

    final String UPDATE_PASSWORD = "Change_Password";

    public CreatePasswordInteractor(CreatePasswordContract.CreatePasswordListener createPasswordListener){
        this.createPasswordListener = createPasswordListener;
    }


    public void changeAgentPassword(Context context, ChangePasswordRequest changePasswordRequest, String token) {

        createPasswordObserver(token,changePasswordRequest).subscribeWith(createPasswordObservable(context));
    }

    public Observable<ChangePasswordResponse> createPasswordObserver(String token,ChangePasswordRequest changePasswordRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .updateUserPassword("Token " + token, changePasswordRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<ChangePasswordResponse> createPasswordObservable(final Context context){
        return new DisposableObserver<ChangePasswordResponse>() {
            @Override
            public void onNext(ChangePasswordResponse changePasswordResponse) {

                Log.d(UPDATE_PASSWORD, "DATA" + changePasswordResponse.getData());
                if(changePasswordResponse.getStatus().equalsIgnoreCase("Error")){
                    createPasswordListener.onFailure(changePasswordResponse.getMessage());
                }else{
                    createPasswordListener.onSuccess(context,changePasswordResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(UPDATE_PASSWORD,"Error " + e);
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.d(UPDATE_PASSWORD,"Completed");
            }
        };
    }
}
