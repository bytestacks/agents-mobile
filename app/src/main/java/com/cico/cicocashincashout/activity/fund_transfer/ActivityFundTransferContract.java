package com.cico.cicocashincashout.activity.fund_transfer;

import android.content.Context;

import com.cico.cicocashincashout.model.Bank.response.BankListResponse;
import com.cico.cicocashincashout.model.Bank.response.ValidateNumberResponse;
import com.cico.cicocashincashout.model.Bank.vfdresponse.response.VFDResponse;
import com.cico.cicocashincashout.model.Bank.vfdresponse.validate_number_response.VFDValidateNumberResponse;

public interface ActivityFundTransferContract {

    interface ActivityFundTransferView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void populateBankList(BankListResponse bankListResponse);

        void validateAccountNumberSuccess(ValidateNumberResponse validateNumberResponse);
    }

    interface ActivityFundTransferListener{

        void onSuccess(Context context, BankListResponse bankListResponse);

        void validateAccountNumberSuccess(Context context, ValidateNumberResponse validateNumberResponse);

        void onFailure(String message);
    }
}
