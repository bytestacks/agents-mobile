package com.cico.cicocashincashout.activity.agent_withdrawal;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.agent_withdraw.request.AgentWithdrawRequest;
import com.cico.cicocashincashout.model.agent_withdraw.response.AgentWithdrawResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class AgentWithdrawalInteractor {

    private AgentWithdrawalContract.AgentWithdrawalListener agentWithdrawalListener;

    final String AGENT_WITHDRAWAL = "AGENT_WITHDRAWAL";


    public AgentWithdrawalInteractor(AgentWithdrawalContract.AgentWithdrawalListener agentWithdrawalListener){

        this.agentWithdrawalListener = agentWithdrawalListener;
    }

    public void agentWithdrawal(AgentWithdrawRequest agentWithdrawRequest, String header, Context context) {

        agentWithdrawalObserver(header,agentWithdrawRequest).subscribeWith(initiateCashoutObservable(context));
    }

    public Observable<AgentWithdrawResponse> agentWithdrawalObserver(String header, AgentWithdrawRequest agentWithdrawRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .agentWithdrawal(agentWithdrawRequest,"Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<AgentWithdrawResponse> initiateCashoutObservable(final Context context){
        return new DisposableObserver<AgentWithdrawResponse>() {
            @Override
            public void onNext(AgentWithdrawResponse initiateCashoutResponse) {

                Log.d(AGENT_WITHDRAWAL, "DATA" + initiateCashoutResponse.getData());
                if(initiateCashoutResponse.getStatus().equalsIgnoreCase("Error")){
                    agentWithdrawalListener.onFailure(initiateCashoutResponse.getMessage());
                }else{
                    agentWithdrawalListener.onSuccess(context,initiateCashoutResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(AGENT_WITHDRAWAL,"Error " + e);
                e.printStackTrace();
                agentWithdrawalListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(AGENT_WITHDRAWAL,"Completed");
            }
        };
    }
}
