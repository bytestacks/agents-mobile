package com.cico.cicocashincashout.activity.activate_agent;

import android.content.Context;

import com.cico.cicocashincashout.model.agent.response.AgentActivateResponseModel;

public interface ActivateAgentContract {


    interface ActivateAgentView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void loginSuccess();
    }

    interface ActivateAgentListener{

        void onSuccess(Context context, AgentActivateResponseModel agentActivateResponseModel);

        void onFailure(String message);
    }
}
