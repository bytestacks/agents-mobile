package com.cico.cicocashincashout.activity.activate_agent;

import android.content.Context;

import com.cico.cicocashincashout.model.agent.request.AgentActivateRequestModel;
import com.cico.cicocashincashout.model.agent.response.AgentActivateResponseModel;
import com.cico.cicocashincashout.utils.Preferences;

public class ActivateAgentPresenter implements ActivateAgentContract.ActivateAgentListener {

    private ActivateAgentContract.ActivateAgentView activateAgentView;
    private ActivateAgentInteractor activateAgentInteractor;

    public ActivateAgentPresenter(ActivateAgentContract.ActivateAgentView activateAgentView){
        this.activateAgentView = activateAgentView;
        activateAgentInteractor = new ActivateAgentInteractor(this);
    }

    @Override
    public void onSuccess(Context context, AgentActivateResponseModel agentActivateResponseModel) {
        activateAgentView.hideProgress();
        Preferences.setAgentActivated(context, true);
        activateAgentView.loginSuccess();
    }

    @Override
    public void onFailure(String message) {
        activateAgentView.hideProgress();
        activateAgentView.showToast(message);
    }

    public void handleAgentActivateAction(Context context, AgentActivateRequestModel agentActivateRequestModel){
        activateAgentView.showProgress();
        activateAgentInteractor.validateAgentDetails(context,agentActivateRequestModel);
    }

    public void onDestroy(){
        activateAgentView = null;
    }
}
