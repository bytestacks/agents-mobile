package com.cico.cicocashincashout.activity.data;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.data.request.DataPlanRequest;
import com.cico.cicocashincashout.model.data.response.DataPlanResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MobileDataInteractor {

    private MobileDataContract.DataVerificationListener dataVerificationListener;

    final String DATA_PLAN = "list data plan";


    public MobileDataInteractor(MobileDataContract.DataVerificationListener dataVerificationListener){

        this.dataVerificationListener = dataVerificationListener;
    }

    public void getDataPlan(Context context, String token, DataPlanRequest dataPlanRequest) {
        getDataPlanObserver(token,dataPlanRequest).subscribeWith(getPaymentObservable(context));
    }

    private Observable getDataPlanObserver(String token, DataPlanRequest dataPlanRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .getDataPLan("Token " + token, dataPlanRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<DataPlanResponse> getPaymentObservable(final Context context){
        return new DisposableObserver<DataPlanResponse>() {
            @Override
            public void onNext(DataPlanResponse dataPlanResponse) {

                Log.d(DATA_PLAN, "DATA" + dataPlanResponse.getData());
                if(dataPlanResponse.getStatus().equalsIgnoreCase("Successful")){
                    dataVerificationListener.onSuccess(context,dataPlanResponse);
                }else{
                    dataVerificationListener.onFailure(dataPlanResponse.getMessage());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(DATA_PLAN,"Error " + e);
                e.printStackTrace();
                dataVerificationListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(DATA_PLAN,"Completed");
            }
        };
    }
}
