package com.cico.cicocashincashout.activity.fund_transfer;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.Bank.request.ValidateNumberRequest;
import com.cico.cicocashincashout.model.Bank.response.BankListData;
import com.cico.cicocashincashout.model.Bank.response.BankListResponse;
import com.cico.cicocashincashout.model.Bank.response.ValidateNumberResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class ActivityFundTransfer extends AppCompatActivity implements ActivityFundTransferContract.ActivityFundTransferView {

    ImageView back_btn;
    Spinner spinner_bank;
    EditText bank_account_number,text_amount,cust_text,text_narration,bank_account_name,bank_account_mail;
    TextView proceed_button;
    String accountCode,selectedItem;
    int trans_cost,stamp_duty;

    ActivityFundTrasnferPresenter activityFundTrasnferPresenter;
    private boolean isSpinnerTouched = false;
    List<BankListData> bankListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer);

        activityFundTrasnferPresenter = new ActivityFundTrasnferPresenter(this);

        initControls();
    }

    private void initControls() {

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        spinner_bank = (Spinner) findViewById(R.id.spinner_bank);
        spinner_bank.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isSpinnerTouched = true;
                return false;
            }
        });
        spinner_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if(isSpinnerTouched){
                        selectedItem = adapterView.getItemAtPosition(i).toString();
                        accountCode = getBankCode(selectedItem);
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bank_account_number = (EditText) findViewById(R.id.bank_account_number);
        bank_account_name = (EditText) findViewById(R.id.bank_account_name);
        text_amount = (EditText) findViewById(R.id.text_amount);
        cust_text = (EditText) findViewById(R.id.cust_text);
        text_narration = (EditText) findViewById(R.id.text_narration);
        bank_account_mail = (EditText) findViewById(R.id.bank_account_mail);

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String header = Preferences.getAgentToken(ActivityFundTransfer.this);
                ValidateNumberRequest validateNumberRequest = new ValidateNumberRequest();
                validateNumberRequest.setAccountNumber(bank_account_number.getText().toString());
                validateNumberRequest.setBankCode(accountCode);
                validateNumberRequest.setAmount(Integer.parseInt(text_amount.getText().toString()));

                activityFundTrasnferPresenter.valdiateAccountNumber(ActivityFundTransfer.this,
                        header, validateNumberRequest);
            }
        });

        String discoList = Preferences.getBankList(ActivityFundTransfer.this);
        if(TextUtils.isEmpty(discoList)){
            getBankList(ActivityFundTransfer.this);
        }else{
            loadDiscoFromPreferences(ActivityFundTransfer.this);
        }

    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityFundTransfer.this,message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(ActivityFundTransfer.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(ActivityFundTransfer.this);
    }

    @Override
    public void populateBankList(BankListResponse bankListResponse) {

        List<String> stringList = new ArrayList<String>();
        bankListData = bankListResponse.getData();
        if(bankListData != null && bankListData.size()>0){

            for(int i = 0; i<bankListData.size(); i++){
                BankListData bank = bankListData.get(i);
                stringList.add(bank.getBankName());
            }
            stringList.add(0,"--Select Bank--");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,stringList);
            spinner_bank.setAdapter(arrayAdapter);
        }
    }

    private void getTransactionsCost(int amount, Context context){

        int rangeOne = Preferences.getCashoutRangeOne(context);
        int rangeTwo = Preferences.getCashoutRangeTwo(context);
        int rangeThree = Preferences.getCashoutRangeThree(context);

        if(amount <= rangeOne){
            trans_cost = Preferences.getCashoutRangeOneVal(context);
            stamp_duty = Preferences.getCashoutStampDutyOne(context);
        }else if(amount >rangeOne && amount<= rangeTwo){
            trans_cost = Preferences.getCashoutRangeTwoVal(context);
            stamp_duty = Preferences.getCashoutStampDutyTwo(context);
        }else if(amount >rangeTwo && amount <= rangeThree){
            trans_cost = Preferences.getCashoutRangeThreeVal(context);
            stamp_duty = Preferences.getCashoutStampDutyThree(context);
        }
    }

    @Override
    public void validateAccountNumberSuccess(ValidateNumberResponse validateNumberResponse) {
        getTransactionsCost(Integer.parseInt(text_amount.getText().toString()),ActivityFundTransfer.this);
        Bundle bundle = new Bundle();
        bundle.putString("bank",selectedItem);
        bundle.putString("bankCode",accountCode);
        bundle.putString("accountNumber",bank_account_number.getText().toString());
        bundle.putString("accountName",bank_account_name.getText().toString());
        bundle.putString("amount",text_amount.getText().toString());
        bundle.putString("custNumber",cust_text.getText().toString());
        bundle.putString("narration",text_narration.getText().toString());
        bundle.putString("email",bank_account_mail.getText().toString());
        bundle.putString("account_name",validateNumberResponse.getData().getData().getAccountInfo().getAccountName());
        bundle.putString("trans_cost",String.valueOf(trans_cost));
        bundle.putString("stamp_duty",String.valueOf(stamp_duty));
        new AppNavigator(ActivityFundTransfer.this).navigateToFinalPaymentVerification(bundle);

    }

    private void getBankList(Context context){
        String token = Preferences.getAgentToken(context);
        activityFundTrasnferPresenter.getBankList(context, token);
    }

    private void loadDiscoFromPreferences(Context context){

        BankListResponse bankListResponse = null;
        List<String> stringList = new ArrayList<String>();
        Gson gson = new Gson();
        String discoList = Preferences.getBankList(context);
        if(!TextUtils.isEmpty(discoList)){
            bankListResponse = gson.fromJson(discoList,BankListResponse.class);
            bankListData = bankListResponse.getData();
            for(int i = 0; i<bankListData.size(); i++){
                BankListData bank = bankListData.get(i);
                stringList.add(bank.getBankName());
            }
            stringList.add(0,"--Select Bank--");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,stringList);
            spinner_bank.setAdapter(arrayAdapter);
        }

    }

    private String getBankCode(String bankName){
        String bankCode = "";
        if(!bankListData.isEmpty()){
            for(int i=0; i<bankListData.size(); i++){
                BankListData bankData = bankListData.get(i);
                if(bankData.getBankName().equalsIgnoreCase(bankName)){
                    bankCode = String.valueOf(bankData.getBankCode());
                    break;
                }
            }
        }

        return bankCode;
    }
}
