package com.cico.cicocashincashout.activity.payment_verification;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.Bank.request.DisburseFundRequest;
import com.cico.cicocashincashout.model.Bank.response.DisburseResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

public class PaymentVerificationFinal extends AppCompatActivity implements PaymentVerificationContract.PaymentVerificationView{

    TextView proceed_button,text_phone,text_amount,text_amount_phone,text_airtime_network,text_acct_name,stamp_duty_text;
    ImageView back_btn;
    String bank,bankCode,accountNumber,accountName,amount,custNumber,narration,email,account_name,trans_cost,
    stamp_duty;
    private PaymentVerificationPresenter paymentVerificationPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_verification);

        paymentVerificationPresenter = new PaymentVerificationPresenter(this);

        initControls();
    }

    private void initControls() {

        getBundleValues();
        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String token = Preferences.getAgentToken(PaymentVerificationFinal.this);
                DisburseFundRequest disburseFundRequest = new DisburseFundRequest();
                disburseFundRequest.setAccountNumber(accountNumber);
                disburseFundRequest.setAmount(amount);
                disburseFundRequest.setBankCode(bankCode);
                disburseFundRequest.setEmail(email);
                disburseFundRequest.setFirstName(accountName);
                disburseFundRequest.setLastName(accountName);
                disburseFundRequest.setPhoneNumber(custNumber);
                paymentVerificationPresenter.disbursefund(PaymentVerificationFinal.this,token,disburseFundRequest);
            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        text_acct_name = (TextView) findViewById(R.id.text_acct_name);
        text_acct_name.setText(account_name);

        stamp_duty_text = (TextView) findViewById(R.id.stamp_duty_text);
        stamp_duty_text.setText(stamp_duty);

        text_phone = (TextView) findViewById(R.id.text_phone);
        text_phone.setText(custNumber);

        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(amount);

        text_amount_phone = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_phone.setText(trans_cost);

        text_airtime_network = (TextView) findViewById(R.id.text_airtime_network);
        String val = String.valueOf(Integer.parseInt(text_amount.getText().toString()) + Integer.parseInt(text_amount_phone.getText().toString())
        + Integer.parseInt(stamp_duty_text.getText().toString()));
        text_airtime_network.setText(val);
    }

    private void getBundleValues(){
        bank = String.valueOf(getIntent().getStringExtra("bank"));
        bankCode = String.valueOf(getIntent().getStringExtra("bankCode"));
        accountNumber = String.valueOf(getIntent().getStringExtra("accountNumber"));
        accountName = String.valueOf(getIntent().getStringExtra("accountName"));
        amount = String.valueOf(getIntent().getStringExtra("amount"));
        custNumber = String.valueOf(getIntent().getStringExtra("custNumber"));
        narration = String.valueOf(getIntent().getStringExtra("narration"));
        email = String.valueOf(getIntent().getStringExtra("email"));
        account_name = (String.valueOf(getIntent().getStringExtra("account_name")));
        trans_cost = String.valueOf(getIntent().getStringExtra("trans_cost"));
        stamp_duty = String.valueOf(getIntent().getStringExtra("stamp_duty"));


    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(PaymentVerificationFinal.this,message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(PaymentVerificationFinal.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(PaymentVerificationFinal.this);

    }

    @Override
    public void showError(String token) {
        Bundle bundle = new Bundle();
        bundle.putString("result","fail");
        bundle.putString("reference", token);
        bundle.putString("tran_type","Electricity Payment");
        bundle.putString("result","fail");
        bundle.putString("amount",amount);
        new AppNavigator(PaymentVerificationFinal.this).navigateToSuccess(bundle);
    }

    @Override
    public void onTransferSuccess(DisburseResponse disburseResponse) {
        String transactionToken ="";// disburseResponse.getData().getStatusDescription().getTransactionRef();
        Bundle bundle = new Bundle();
        bundle.putString("reference",transactionToken);
        bundle.putString("result","success");
        bundle.putString("tran_type","Electricity Payment");
        bundle.putString("amount",amount);
        new AppNavigator(PaymentVerificationFinal.this).navigateToSuccess(bundle);
    }
}
