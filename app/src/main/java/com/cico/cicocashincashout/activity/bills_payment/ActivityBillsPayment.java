package com.cico.cicocashincashout.activity.bills_payment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.disco_list.response.DiscoListResponseData;
import com.cico.cicocashincashout.model.disco_list.response.DiscoListResponseModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class ActivityBillsPayment extends AppCompatActivity implements BillsPaymentContract.BillsPaymentlView {

    ImageView back_btn;
    Spinner spinner_electricity, spinner_data,spinner_cable;
    String[] telcoCode = {"-","D02D", "D01D", "D03D","D04D"};

    BillsPaymentPresenter billsPaymentPresenter;

    private boolean isSpinnerTouched = false;
    private boolean isAirtimeTouched = false;
    private boolean isCableTouched = false;

    List<DiscoListResponseData> discoListResponseDataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills_payment);

        billsPaymentPresenter = new BillsPaymentPresenter(this);

        initControls();
    }

    private void initializeElectricityControls(){

        spinner_electricity = (Spinner) findViewById(R.id.spinner_electricity);
        spinner_electricity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isSpinnerTouched = true;
                return false;
            }
        });
        spinner_electricity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(isSpinnerTouched){

                    String selectedItem = adapterView.getItemAtPosition(i).toString();
                    if(!selectedItem.equalsIgnoreCase("--Select Disco--")){
                        String discoCode = getDiscoCode(selectedItem);
                        Bundle bundle = new Bundle();
                        bundle.putString("disco",selectedItem);
                        bundle.putString("disco_code",discoCode);
                        new AppNavigator(ActivityBillsPayment.this).navigateToelectricPayment(bundle);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        String discoList = Preferences.getDiscoList(ActivityBillsPayment.this);
        if(TextUtils.isEmpty(discoList)){
            getDiscoList(ActivityBillsPayment.this);
        }else{
            loadDiscoFromPreferences(ActivityBillsPayment.this);
        }

    }

    private String getDiscoCode(String discoName){
        String discoCode = "";
        if(!discoListResponseDataList.isEmpty()){
            for(int i=0; i<discoListResponseDataList.size(); i++){
                DiscoListResponseData discoListResponseData = discoListResponseDataList.get(i);
                if(discoListResponseData.getName().equalsIgnoreCase(discoName)){
                    discoCode = discoListResponseData.getBuypowerCode();
                    break;
                }
            }
        }

        return discoCode;
    }

    private void initializeDataControls(){

        spinner_data = (Spinner) findViewById(R.id.spinner_data);
        spinner_data.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isAirtimeTouched = true;
                return false;
            }
        });
        spinner_data.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(isAirtimeTouched){
                    String selectedItem = (String) adapterView.getItemAtPosition(i);
                    String code = telcoCode[i];
                    if(!selectedItem.equalsIgnoreCase("--Select Telco--")){

                        Bundle bundle = new Bundle();
                        bundle.putString("telco",selectedItem);
                        bundle.putString("telcoCode",code);
                        new AppNavigator(ActivityBillsPayment.this).navigateToMobileData(bundle);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void initializeCableControls(){

        spinner_cable = (Spinner) findViewById(R.id.spinner_cable);
        spinner_cable.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isCableTouched = true;
                return false;
            }
        });
        spinner_cable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(isCableTouched){

                    String selectedItem = adapterView.getItemAtPosition(i).toString();
                    if(!selectedItem.equalsIgnoreCase("--Select Cable Tv--")){


                        Bundle bundle = new Bundle();
                        bundle.putString("cable_tv",selectedItem);

                        new AppNavigator(ActivityBillsPayment.this).navigateToTvSubs(bundle);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void initControls() {

        initializeElectricityControls();
        initializeDataControls();
        initializeCableControls();

    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityBillsPayment.this, message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(ActivityBillsPayment.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(ActivityBillsPayment.this);
    }

    @Override
    public void onSuccess(DiscoListResponseModel discoListResponseModel) {

        List<String> stringList = new ArrayList<String>();
        discoListResponseDataList = discoListResponseModel.getData();
        for(int i = 0; i<discoListResponseDataList.size(); i++){
            DiscoListResponseData discoListResponseData = discoListResponseDataList.get(i);
            stringList.add(discoListResponseData.getName());
        }
        stringList.add(0,"--Select Disco--");
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,stringList);
        spinner_electricity.setAdapter(arrayAdapter);
    }

    private void getDiscoList(Context context){

        String token = Preferences.getAgentToken(context);

        billsPaymentPresenter.getDiscoList(context,token);
    }

    private void loadDiscoFromPreferences(Context context){

        DiscoListResponseModel discoListResponseModel = null;
        List<String> stringList = new ArrayList<String>();
        Gson gson = new Gson();
        String discoList = Preferences.getDiscoList(context);
        if(!TextUtils.isEmpty(discoList)){
            discoListResponseModel = gson.fromJson(discoList,DiscoListResponseModel.class);
            discoListResponseDataList = discoListResponseModel.getData();
            for(int i = 0; i<discoListResponseDataList.size(); i++){
                DiscoListResponseData discoListResponseData = discoListResponseDataList.get(i);
                stringList.add(discoListResponseData.getName());
            }
            stringList.add(0,"--Select Disco--");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,stringList);
            spinner_electricity.setAdapter(arrayAdapter);
        }

    }

}
