package com.cico.cicocashincashout.activity.cash_withdrawal;


import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.cashout.request.InitiateCashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.InitiateCashoutResponse;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class CashWithdrawalInteractor {

    private CashWithdrawalContract.CashWithdrawalListener cashWithdrawalListener;

    final String CASHLESS = "cashless_withdrawal";

    public CashWithdrawalInteractor(CashWithdrawalContract.CashWithdrawalListener cashWithdrawalListener){

        this.cashWithdrawalListener = cashWithdrawalListener;
    }

    public void initiateCashout(InitiateCashoutRequest initiateCashoutRequest, String header,  Context context) {

        initiateCashoutObserver(header,initiateCashoutRequest).subscribeWith(initiateCashoutObservable(context));
    }

    public Observable<InitiateCashoutResponse> initiateCashoutObserver(String header, InitiateCashoutRequest initiateCashoutRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .initiateCashout(initiateCashoutRequest,"Token " + header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<InitiateCashoutResponse> initiateCashoutObservable(final Context context){
        return new DisposableObserver<InitiateCashoutResponse>() {
            @Override
            public void onNext(InitiateCashoutResponse initiateCashoutResponse) {

                Log.d(CASHLESS, "DATA" + initiateCashoutResponse.getData());
                if(initiateCashoutResponse.getStatus().equalsIgnoreCase("Error")){
                    cashWithdrawalListener.onFailure(initiateCashoutResponse.getMessage());
                }else{
                    cashWithdrawalListener.onInitiateCashoutResponse(context,initiateCashoutResponse);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(CASHLESS,"Error " + e);
                e.printStackTrace();
                cashWithdrawalListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(CASHLESS,"Completed");
            }
        };
    }
}
