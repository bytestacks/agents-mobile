package com.cico.cicocashincashout.activity.airtime.airtime_verification;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.airtime.request.VendingAirtimeRequestModel;
import com.cico.cicocashincashout.model.airtime.response.AirtimeVendingResponseModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

public class ActivityAirtimeVerification extends AppCompatActivity implements AirtimeVerificationContract.AirtimeVerificationView {

    TextView text_amount,text_amount_phone,text_airtime_network;
    ImageView back_btn;

    CircularProgressButton proceed_button;

    AirtimeVerificationPresenter airtimeVerificationPresenter;
    String token,phone,amount,telco, network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_payment_verification);

        airtimeVerificationPresenter = new AirtimeVerificationPresenter(this);

        getBundleExtras();

        initControls();
    }

    private void initControls() {

        token = Preferences.getAgentToken(ActivityAirtimeVerification.this);

        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(amount);
        text_amount_phone = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_phone.setText(phone);
        text_airtime_network = (TextView) findViewById(R.id.text_airtime_network);
        text_airtime_network.setText(network);

        proceed_button = (CircularProgressButton) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VendingAirtimeRequestModel vendingAirtimeRequestModel = new VendingAirtimeRequestModel();
                vendingAirtimeRequestModel.setAmount(Integer.parseInt(amount));
                vendingAirtimeRequestModel.setPhone(phone);
                vendingAirtimeRequestModel.setTelco(telco);
                airtimeVerificationPresenter.makePayment(ActivityAirtimeVerification.this, token, vendingAirtimeRequestModel);
                //new AppNavigator(ActivityAirtimeVerification.this).navigateToSuccess();
            }
        });

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void getBundleExtras(){

        amount = getIntent().getStringExtra("amount");
        phone = getIntent().getStringExtra("phone");
        telco = getIntent().getStringExtra("telco_code");
        network = getIntent().getStringExtra("telco");
    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(ActivityAirtimeVerification.this, message);
    }

    @Override
    public void showProgress() {
        //Utility.showProgressDialog(ActivityAirtimeVerification.this,false);
        proceed_button.startAnimation();

    }

    @Override
    public void hideProgress() {
        //Utility.hideProgressDialog(ActivityAirtimeVerification.this);
        proceed_button.revertAnimation();

    }

    @Override
    public void onVendingSuccess(AirtimeVendingResponseModel vendingResponseModel) {
        String transactionToken = vendingResponseModel.getData().getTranxReference();
        Bundle bundle = new Bundle();
        bundle.putString("reference",transactionToken);
        bundle.putString("amount",amount);

        new AppNavigator(ActivityAirtimeVerification.this).navigateToSuccess(bundle);

    }
}
