package com.cico.cicocashincashout.activity.payment_verification;

import android.content.Context;

import com.cico.cicocashincashout.model.Bank.response.DisburseResponse;

public interface PaymentVerificationContract {

    interface PaymentVerificationView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void showError(String token);

        void onTransferSuccess(DisburseResponse disburseResponse);
    }

    interface PaymentVerificationListener{

        void onSuccess(Context context, DisburseResponse disburseResponse);

        void onFailure(String message);
    }
}
