package com.cico.cicocashincashout.activity.airtime.airtime_verification;

import android.content.Context;

import com.cico.cicocashincashout.model.airtime.response.AirtimeVendingResponseModel;

public interface AirtimeVerificationContract {

    interface AirtimeVerificationView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void onVendingSuccess(AirtimeVendingResponseModel vendingResponseModel);
    }

    interface AirtimeVerificationListener {

        void onValidateSuccess(Context context, AirtimeVendingResponseModel vendingResponseModel);

        void onValidateFailure(String message);

    }
}
