package com.cico.cicocashincashout.activity.data;

import android.content.Context;

import com.cico.cicocashincashout.model.data.response.DataPlanResponse;

public interface MobileDataContract {

    interface DataVerificationView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void dataPlanSuccess(DataPlanResponse dataPlanResponse);
    }

    interface DataVerificationListener{

        void onSuccess(Context context, DataPlanResponse dataPlanResponse);

        void onFailure(String message);
    }
}
