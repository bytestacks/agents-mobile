package com.cico.cicocashincashout.activity.UpdatePassword;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.user.update_password.request.ChangePasswordRequest;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;

public class CreatePasswordActivity extends AppCompatActivity implements CreatePasswordContract.CreatePasswordView{


    CircularProgressButton proceed_button;
    EditText txt_username,text_password;

    CreatePasswordPresenter createPasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);

        createPasswordPresenter = new CreatePasswordPresenter(this);

        initControls();
    }

    private void initControls() {

        txt_username = (EditText) findViewById(R.id.txt_username);
        text_password = (EditText) findViewById(R.id.text_password);

        proceed_button = (CircularProgressButton) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //new AppNavigator(CreatePasswordActivity.this).navigateToCicoTest();
                String newPassword = txt_username.getText().toString();
                String confirmPassword = text_password.getText().toString();

                ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                changePasswordRequest.setNewPassword(newPassword);
                changePasswordRequest.setConfirmPassword(confirmPassword);
                createPasswordPresenter.changePassword(CreatePasswordActivity.this,changePasswordRequest,
                        Preferences.getAgentToken(CreatePasswordActivity.this));//.handleLoginAction(LoginActivity.this);
            }
        });

    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(CreatePasswordActivity.this, message);
    }

    @Override
    public void showProgress() {
        //Utility.showProgressDialog(CreatePasswordActivity.this,false);
        proceed_button.startAnimation();
    }

    @Override
    public void hideProgress() {
        // Utility.hideProgressDialog(CreatePasswordActivity.this);
        proceed_button.revertAnimation();
    }

    @Override
    public void updateSuccess() {
        new AppNavigator(CreatePasswordActivity.this).navigateToLoginScreen();
    }
}
