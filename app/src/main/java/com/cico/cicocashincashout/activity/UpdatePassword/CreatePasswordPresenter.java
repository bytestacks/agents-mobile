package com.cico.cicocashincashout.activity.UpdatePassword;

import android.content.Context;

import com.cico.cicocashincashout.model.user.update_password.request.ChangePasswordRequest;
import com.cico.cicocashincashout.model.user.update_password.response.ChangePasswordResponse;

public class CreatePasswordPresenter implements CreatePasswordContract.CreatePasswordListener {

    private CreatePasswordContract.CreatePasswordView createPasswordView;
    private CreatePasswordInteractor createPasswordInteractor;

    public CreatePasswordPresenter(CreatePasswordContract.CreatePasswordView createPasswordView){
        this.createPasswordView = createPasswordView;
        createPasswordInteractor = new CreatePasswordInteractor(this);
    }

    @Override
    public void onSuccess(Context context, ChangePasswordResponse changePasswordResponse) {
            createPasswordView.hideProgress();
            createPasswordView.updateSuccess();
    }

    @Override
    public void onFailure(String message) {
        createPasswordView.hideProgress();
        createPasswordView.showToast(message);
    }

    public void changePassword(Context context, ChangePasswordRequest changePasswordRequest, String token){

        createPasswordView.showProgress();
        createPasswordInteractor.changeAgentPassword(context, changePasswordRequest, token);
    }

}
