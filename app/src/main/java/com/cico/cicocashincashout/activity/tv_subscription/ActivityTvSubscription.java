package com.cico.cicocashincashout.activity.tv_subscription;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.PinpadSetupActivity;
import com.cico.cicocashincashout.model.cable_tv.startimes.request.StartimesRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.response.StartimesResponseModel;
import com.cico.cicocashincashout.model.dstv.response.AvailablePricingOption;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListDatum;
import com.cico.cicocashincashout.model.dstv.response.DstvPriceListResponse;
import com.cico.cicocashincashout.model.dstv.validate_number.request.DstvValidateNumberRequest;
import com.cico.cicocashincashout.model.dstv.validate_number.response.DstvValidateNumberResponse;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class ActivityTvSubscription extends AppCompatActivity implements TvSubscriptionContract.TvSubscriptionView {

    ImageView back_btn,imageView;
    EditText meter_number,amount;
    Spinner plan_type,plan_period;
    TextView proceed_button,txt_plan,txt_period;

    String cableCompanyName, planType,period,planCode,metNumber;

    TvSubscriptionPresenter tvSubscriptionPresenter;
    List<DstvPriceListDatum> dstvPriceListDatumList;

    boolean isTouched = false;
    boolean isPlanPeriodTouched  =false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_subs);

        tvSubscriptionPresenter = new TvSubscriptionPresenter(this);

        initControls();
    }

    private void initControls() {

        cableCompanyName = getIntent().getStringExtra("cable_tv");
        int drawableId = getDrawableLogo(cableCompanyName);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(drawableId);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        amount = (EditText) findViewById(R.id.amount);

        plan_type = (Spinner) findViewById(R.id.plan_type);
        plan_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isTouched = true;
                return false;
            }
        });
        plan_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(isTouched){

                    planType = (String) adapterView.getItemAtPosition(i);
                    planCode = getPlanCode(planType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        meter_number = (EditText) findViewById(R.id.meter_number);
        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String header = Preferences.getAgentToken(ActivityTvSubscription.this);
                if(cableCompanyName.equalsIgnoreCase("Startimes")){
                    String meterNumber = meter_number.getText().toString();
                    StartimesRequestModel startimesRequestModel = new StartimesRequestModel();
                    startimesRequestModel.setSmartcard(meterNumber);
                    tvSubscriptionPresenter.validateStartimeNumber(ActivityTvSubscription.this,
                            header,startimesRequestModel);
                }else{
                    metNumber = meter_number.getText().toString();
                    DstvValidateNumberRequest dstvValidateNumberRequest = new DstvValidateNumberRequest();
                    dstvValidateNumberRequest.setService(cableCompanyName);
                    dstvValidateNumberRequest.setSmartcard(metNumber);
                    tvSubscriptionPresenter.validateDstvNumber(ActivityTvSubscription.this,header,
                            dstvValidateNumberRequest);
                }
            }
        });

        txt_plan = (TextView) findViewById(R.id.txt_plan);
        txt_period = (TextView) findViewById(R.id.txt_period);
        plan_period = (Spinner) findViewById(R.id.plan_period);
        plan_period.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isPlanPeriodTouched = true;
                return false;
            }
        });
        plan_period.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(isPlanPeriodTouched){

                    period = (String) adapterView.getItemAtPosition(i);
                    String price = getAvailableOption(planType,period.replace("month","").trim());
                    amount.setText(price);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        switchControls(cableCompanyName);

        loadTvPackages(cableCompanyName, ActivityTvSubscription.this);

    }

    private void loadTvPackages(String cableName, Context context){
        if(cableName.equalsIgnoreCase("Dstv")){
            loadDstvPackages(context);
        }else{
            loadGotvPackages(context);
        }
    }

    private void loadDstvPackages(Context context){
        String dstvPackage = Preferences.getDstvList(context);
        String token = Preferences.getAgentToken(ActivityTvSubscription.this);
        if(TextUtils.isEmpty(dstvPackage)){
            tvSubscriptionPresenter.getDstvPriceList(ActivityTvSubscription.this,token);
        }else {
            loadDstvPackagesFromPrefences(context);
        }
    }

    private void loadDstvPackagesFromPrefences(Context context){

        DstvPriceListResponse dstvPriceListResponse = null;
        Gson gson = new Gson();
        String dstvList = Preferences.getDstvList(context);
        if(!TextUtils.isEmpty(dstvList)){
            dstvPriceListResponse = gson.fromJson(dstvList,DstvPriceListResponse.class);
            List<String> dstvPackagesList = new ArrayList<String>();
            List<DstvPriceListDatum> dstvPriceListDatumList = dstvPriceListResponse.getData();
            for(int i = 0; i<dstvPriceListDatumList.size(); i++){
                DstvPriceListDatum dstvPriceListDatum = dstvPriceListDatumList.get(i);
                String packageName = dstvPriceListDatum.getName();
                dstvPackagesList.add(packageName);
            }
            dstvPackagesList.add(0,"--Select Package--");
            ArrayAdapter planAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,dstvPackagesList);
            plan_type.setAdapter(planAdapter);
        }

    }

    private void loadGotvPackages(Context context){
            String dstvPackage = Preferences.getGotvList(context);
            String token = Preferences.getAgentToken(ActivityTvSubscription.this);
            if(TextUtils.isEmpty(dstvPackage)){
                tvSubscriptionPresenter.getGotvPriceList(ActivityTvSubscription.this,token);
            }else {
                loadGotvPackagesFromPrefences(context);
            }
    }

    private void loadGotvPackagesFromPrefences(Context context){

        DstvPriceListResponse dstvPriceListResponse = null;
        Gson gson = new Gson();
        String dstvList = Preferences.getGotvList(context);
        if(!TextUtils.isEmpty(dstvList)){
            dstvPriceListResponse = gson.fromJson(dstvList,DstvPriceListResponse.class);
            List<String> dstvPackagesList = new ArrayList<String>();
            List<DstvPriceListDatum> dstvPriceListDatumList = dstvPriceListResponse.getData();
            for(int i = 0; i<dstvPriceListDatumList.size(); i++){
                DstvPriceListDatum dstvPriceListDatum = dstvPriceListDatumList.get(i);
                String packageName = dstvPriceListDatum.getName();
                dstvPackagesList.add(packageName);
            }
            dstvPackagesList.add(0,"--Select Package--");
            ArrayAdapter planAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,dstvPackagesList);
            plan_type.setAdapter(planAdapter);
        }

    }

    private int getDrawableLogo(String companyName){
        if(companyName.equalsIgnoreCase("Startimes")){
            return R.drawable.startimes;
        }else if(companyName.equalsIgnoreCase("Dstv")){
            return R.drawable.dstv;
        }else if(companyName.equalsIgnoreCase("Gotv")){
            return R.drawable.gotv;
        }

        return 0;
    }

    @Override
    public void showToast(String message) {

        Utility.shortToast(ActivityTvSubscription.this, message);

    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(ActivityTvSubscription.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(ActivityTvSubscription.this);
    }

    @Override
    public void startimesValidateNumberSuccess(StartimesResponseModel startimesResponseModel) {

        Bundle bundle = new Bundle();
        bundle.putString("cust_name", startimesResponseModel.getData().getCustomerName());
        bundle.putString("meter_num", startimesResponseModel.getData().getSmartCardCode());
        bundle.putString("amount",amount.getText().toString());
        bundle.putString("type","startimes");
        new AppNavigator(ActivityTvSubscription.this).navigateToBillVerification(bundle);
    }

    @Override
    public void dstvValidateNumberSuccess(DstvValidateNumberResponse dstvValidateNumberResponse) {

        Bundle bundle = new Bundle();
        bundle.putString("cust_name", dstvValidateNumberResponse.getData().getStatusDescription().getFirstname() + " " +
                dstvValidateNumberResponse.getData().getStatusDescription().getLastname());
        bundle.putString("meter_num", String.valueOf(dstvValidateNumberResponse.getData().getStatusDescription().getCustomerNo()));
        bundle.putString("amount",amount.getText().toString());
        bundle.putString("type","dstv/gotv");
        bundle.putString("code",planCode);
        bundle.putString("period",period.replace("month","").trim());
        new AppNavigator(ActivityTvSubscription.this).navigateToBillVerification(bundle);
    }

    @Override
    public void dstvPriceListSuccess(DstvPriceListResponse dstvPriceListResponse) {

        List<String> dstvPackagesList = new ArrayList<String>();
        dstvPriceListDatumList = dstvPriceListResponse.getData();
        for(int i = 0; i<dstvPriceListDatumList.size(); i++){
            DstvPriceListDatum dstvPriceListDatum = dstvPriceListDatumList.get(i);
            String packageName = dstvPriceListDatum.getName();
            dstvPackagesList.add(packageName);
        }

        dstvPackagesList.add(0,"--Select Package--");
        ArrayAdapter planAdapter = new ArrayAdapter(this,R.layout.spiner_images_item,dstvPackagesList);
        plan_type.setAdapter(planAdapter);

    }

    private void switchControls(String compName){
        if(compName.equalsIgnoreCase("Startimes")){
            plan_type.setVisibility(View.GONE);
            txt_plan.setVisibility(View.GONE);
            txt_period.setVisibility(View.GONE);
            plan_period.setVisibility(View.GONE);
        }
    }

    private String getPlanCode(String planName){
        String planCode = "";
        if(dstvPriceListDatumList !=null){
            for(int i= 0; i<dstvPriceListDatumList.size(); i++){
                DstvPriceListDatum dstvPriceListDatum = dstvPriceListDatumList.get(i);
                if(dstvPriceListDatum.getName().equalsIgnoreCase(planName)){
                    planCode = dstvPriceListDatum.getCode();
                    break;
                }
            }
        }

        return planCode;
    }

    private String getAvailableOption(String packageName, String month){

        String monthPrice = "";
        if(dstvPriceListDatumList !=null){

           outer : for(int i=0; i<dstvPriceListDatumList.size(); i++){
                DstvPriceListDatum dstvPriceListDatum = dstvPriceListDatumList.get(i);
                if(dstvPriceListDatum.getName().equalsIgnoreCase(packageName)){
                    List<AvailablePricingOption> availablePricingOptionList = dstvPriceListDatum.getAvailablePricingOptions();
                    for(int j=0; j<availablePricingOptionList.size(); j++){
                        AvailablePricingOption availablePricingOption = availablePricingOptionList.get(j);
                        String period = String.valueOf(availablePricingOption.getMonthsPaidFor());
                        if(period.equalsIgnoreCase(month)){
                            monthPrice = String.valueOf(availablePricingOption.getPrice());
                            break outer;
                        }
                    }
                }
            }
        }

        return monthPrice;
    }
}
