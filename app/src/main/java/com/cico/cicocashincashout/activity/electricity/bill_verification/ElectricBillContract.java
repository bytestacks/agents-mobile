package com.cico.cicocashincashout.activity.electricity.bill_verification;

import android.content.Context;

import com.cico.cicocashincashout.model.electricity.vending.response.VendingResponseModel;

public interface ElectricBillContract {

    interface ElectricBillView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void onVendingSuccess(VendingResponseModel vendingResponseModel);

        void onVendingFailed(String token);
    }

    interface ElectricBillListener{

        void onValidateSuccess(Context context, VendingResponseModel vendingResponseModel);

        void onValidateFailure(String message);
    }
}
