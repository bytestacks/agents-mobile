package com.cico.cicocashincashout.activity.agent_withdrawal;

import android.content.Context;

import com.cico.cicocashincashout.model.agent_withdraw.request.AgentWithdrawRequest;
import com.cico.cicocashincashout.model.agent_withdraw.response.AgentWithdrawResponse;

public class AgentWithdrawalPresenter implements AgentWithdrawalContract.AgentWithdrawalListener{

    private AgentWithdrawalContract.AgentWithdrawalView agentWithdrawalView;
    private AgentWithdrawalInteractor agentWithdrawalInteractor;

    public AgentWithdrawalPresenter(AgentWithdrawalContract.AgentWithdrawalView agentWithdrawalView){
        this.agentWithdrawalView = agentWithdrawalView;
        agentWithdrawalInteractor = new AgentWithdrawalInteractor(this);
    }


    @Override
    public void onSuccess(Context context, AgentWithdrawResponse agentWithdrawResponse) {
        agentWithdrawalView.hideProgress();
        agentWithdrawalView.onSuccess(agentWithdrawResponse);
    }

    @Override
    public void onFailure(String message) {
        agentWithdrawalView.hideProgress();
        agentWithdrawalView.onFailure(message);
    }

    public void withdrawnFund(Context context, String header, AgentWithdrawRequest agentWithdrawRequest){

        agentWithdrawalView.showProgress();
        agentWithdrawalInteractor.agentWithdrawal(agentWithdrawRequest, header, context);
    }
}
