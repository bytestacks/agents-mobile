package com.cico.cicocashincashout.activity.data.vending;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.data.vending.request.DataVendingRequest;
import com.cico.cicocashincashout.model.data.vending.response.DataVendingResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class DataVerificationInteractor {

    private DataVerificationContract.DataVerificationListener dataVerificationListener;

    final String VEND_DATA = "data plan";


    public DataVerificationInteractor(DataVerificationContract.DataVerificationListener dataVerificationListener){

        this.dataVerificationListener = dataVerificationListener;
    }

    public void vendData(Context context, String token, DataVendingRequest dataVendingRequest) {
        vendDataPlanObserver(token,dataVendingRequest).subscribeWith(getPaymentObservable(context));
    }

    private Observable vendDataPlanObserver(String token, DataVendingRequest dataVendingRequest) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .vendData("Token " + token, dataVendingRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<DataVendingResponseModel> getPaymentObservable(final Context context){
        return new DisposableObserver<DataVendingResponseModel>() {
            @Override
            public void onNext(DataVendingResponseModel dataVendingResponseModel) {

                Log.d(VEND_DATA, "DATA" + dataVendingResponseModel.getData());
                if(dataVendingResponseModel.getStatus().equalsIgnoreCase("Successful")){
                    dataVerificationListener.onSuccess(context,dataVendingResponseModel);
                }else{
                    dataVerificationListener.onFailure(dataVendingResponseModel.getMessage());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(VEND_DATA,"Error " + e);
                e.printStackTrace();
                dataVerificationListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(VEND_DATA,"Completed");
            }
        };
    }
}
