package com.cico.cicocashincashout.activity.activate_agent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.model.agent.request.AgentActivateRequestModel;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Utility;

public class ActivateAgentActivity extends AppCompatActivity implements ActivateAgentContract.ActivateAgentView {

    EditText txt_username,text_password;
    CircularProgressButton login_button;

    ActivateAgentPresenter agentPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_agent);

        agentPresenter = new ActivateAgentPresenter(this);

        initControls();
    }


    @Override
    public void showToast(String message) {

        Utility.shortToast(ActivateAgentActivity.this,message);
    }

    @Override
    public void showProgress() {
        //Utility.showProgressDialog(ActivateAgentActivity.this,false);
        login_button.startAnimation();
    }

    @Override
    public void hideProgress() {
        //Utility.hideProgressDialog(ActivateAgentActivity.this);
        login_button.revertAnimation();
    }

    @Override
    public void loginSuccess() {

        new AppNavigator(ActivateAgentActivity.this).navigateToLoginScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        agentPresenter.onDestroy();
    }

    private void initControls(){

        text_password = (EditText) findViewById(R.id.text_password);
        txt_username = (EditText) findViewById(R.id.txt_username);

        login_button = (CircularProgressButton) findViewById(R.id.proceed_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = txt_username.getText().toString();
                String code = text_password.getText().toString();
                AgentActivateRequestModel agentActivateRequestModel = new AgentActivateRequestModel();
                agentActivateRequestModel.setPhone(phone);
                agentActivateRequestModel.setCode(code);
                agentPresenter.handleAgentActivateAction(ActivateAgentActivity.this,agentActivateRequestModel);
            }
        });

    }
}
