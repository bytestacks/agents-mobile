package com.cico.cicocashincashout.activity.payment_cash_verf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.activity.payment_verification.PaymentVerificationFinal;
import com.cico.cicocashincashout.model.cashout.request.CashoutRequest;
import com.cico.cicocashincashout.model.cashout.response.CashoutResponse;
import com.cico.cicocashincashout.utils.AppConstants;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.paypad.cardreader.facade.PinpadFacade;
import com.paypad.impl.Paypad;

import java.util.Calendar;

public class PaymentVerificationActivity extends AppCompatActivity implements PaymentVerificationCashContract.PaymentVerificationCashView {

    TextView proceed_button,text_phone_number,text_amount,text_amount_convi,text_amount_total,stamp_duty_text;
    ImageView back_btn;

    PinpadFacade pinpadFacade;
    Handler handler;
    Paypad paypad;
    private MyBroadcastReceiver myBroadcastReceiver;
    private PaymentVerificationCashPresenter paymentVerificationCashPresenter;

    String amount,token,phoneNum,reference,trans_cost,stamp_duty,
            bundle_amount,bundle_terminal_id,bundle_pan,
            bundle_rrn,bundle_reference,bundle_card_no,
            bundle_provider_ref,bundle_holder_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_verification);

        token = Preferences.getAgentToken(PaymentVerificationActivity.this);

        paymentVerificationCashPresenter = new PaymentVerificationCashPresenter(this);

        initControls();
    }

    private void initControls() {

        Bundle bundle = getIntent().getExtras();

        amount = bundle.getString("amount");
        phoneNum = bundle.getString("phone");
        reference = bundle.getString("reference");
        trans_cost = bundle.getString("trans_cost");
        stamp_duty = bundle.getString("stamp_duty");

        text_phone_number = (TextView) findViewById(R.id.text_phone);
        text_phone_number.setText(phoneNum);

        text_amount = (TextView) findViewById(R.id.text_amount);
        text_amount.setText(AppConstants.CURRENCY_NAIRA + amount);

        text_amount_convi = (TextView) findViewById(R.id.text_amount_phone);
        text_amount_convi.setText(AppConstants.CURRENCY_NAIRA + trans_cost);

        text_amount_total = (TextView) findViewById(R.id.text_airtime_network);

        stamp_duty_text = (TextView) findViewById(R.id.stamp_duty_text);
        stamp_duty_text.setText(AppConstants.CURRENCY_NAIRA + stamp_duty);

        int totalAmount = Integer.parseInt(amount) + Integer.parseInt(trans_cost) + Integer.parseInt(stamp_duty);
        text_amount_total.setText(AppConstants.CURRENCY_NAIRA + totalAmount);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        proceed_button = (TextView) findViewById(R.id.proceed_button);
        proceed_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utility.shortToast(PaymentVerificationActivity.this,getString(R.string.please_insert_card));
                Utility.showProgressDialog(PaymentVerificationActivity.this,false);
                paypad.makePayment(amount, "savings");
            }
        });

        handler = new Handler();
        pinpadFacade = new PinpadFacade(this);

        paypad = new Paypad(this);

        myBroadcastReceiver = new MyBroadcastReceiver();

        registerReceiver(myBroadcastReceiver, new IntentFilter(
                "com.esl.paypadlib"));
    }

    @Override
    protected void onPause() {

        super.onPause();
        unregisterReceiver(myBroadcastReceiver);
    }

    @Override
    protected void onResume() {

        super.onResume();
        registerReceiver(myBroadcastReceiver, new IntentFilter(
                "com.esl.paypadlib"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pinpadFacade.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void showToast(String message) {
        Utility.shortToast(PaymentVerificationActivity.this,message);
    }

    @Override
    public void showProgress() {
        Utility.showProgressDialog(PaymentVerificationActivity.this,false);
    }

    @Override
    public void hideProgress() {
        Utility.hideProgressDialog(PaymentVerificationActivity.this);
    }

    @Override
    public void showError(String token) {
        Bundle bundle = new Bundle();
        bundle.putString("result","fail");
        bundle.putString("tran_type","Cash Out");
        bundle.putString("reference", reference);
        bundle.putString("amount",bundle_amount);
        new AppNavigator(PaymentVerificationActivity.this).navigateToSuccess(bundle);
    }

    @Override
    public void onSuccess(CashoutResponse cashoutResponse) {
        Bundle bundle = new Bundle();
        bundle.putString("result","success");
        bundle.putString("amount",bundle_amount);
        bundle.putString("terminal_id",bundle_terminal_id);
        bundle.putString("pan",bundle_pan);
        bundle.putString("rrn",bundle_rrn);
        bundle.putString("reference",bundle_reference);
        bundle.putString("card_no",bundle_card_no);
        bundle.putString("provider_ref",bundle_provider_ref);
        bundle.putString("holder_name",bundle_holder_name);
        new AppNavigator(PaymentVerificationActivity.this).navigateToSuccess(bundle);
    }


    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.getAction() != null) {
                String s = intent.getAction();
                if (s.equals("com.esl.paypadlib")) {

                    String result = intent.getStringExtra("response");
                    String[] resultarray = intent
                            .getStringArrayExtra("responsearray");
                    String reversalResult = intent.getStringExtra("reversalResult");

                    if (result.equals("pinpadProcessing")) {

                    } else if (result.equals("enterPIN")) {
                        Utility.hideProgressDialog(PaymentVerificationActivity.this);
                        Utility.shortToast(PaymentVerificationActivity.this, getString(R.string.please_input_pin));
                        Utility.showProgressDialog(PaymentVerificationActivity.this, false);
                    } else if (result.equals("PINentered")) {

                    } else if (result.equals("nibssProcessing")) {

                    } else if (result.equals("errorTranx")) {
                        Utility.hideProgressDialog(PaymentVerificationActivity.this);
                        Bundle bundle = new Bundle();
                        bundle.putString("result","fail");
                        bundle.putString("tran_type","Cash Out");
                        bundle.putString("reference", reference);
                        bundle.putString("amount",bundle_amount);
                        new AppNavigator(PaymentVerificationActivity.this).navigateToSuccess(bundle);
                    } else if (result.equals("transactionresponse")) {
                        Utility.hideProgressDialog(PaymentVerificationActivity.this);
                        CashoutRequest cashoutRequest = new CashoutRequest();
                        bundle_amount = resultarray[6];
                        bundle_terminal_id = resultarray[3];
                        bundle_pan = resultarray[4];
                        bundle_rrn = resultarray[7];
                        bundle_reference = AppConstants.CICO + Utility.randomNumber(6);
                        bundle_card_no = resultarray[4];
                        bundle_provider_ref = resultarray[7];
                        bundle_holder_name = resultarray[5];

                        cashoutRequest.setAmount(bundle_amount);
                        cashoutRequest.setCardHolderName(bundle_holder_name);
                        cashoutRequest.setTerminalId(bundle_terminal_id);
                        cashoutRequest.setPanNo(bundle_pan);
                        cashoutRequest.setRrn(bundle_rrn);
                        cashoutRequest.setReference(bundle_reference);
                        cashoutRequest.setProviderRef(bundle_provider_ref);
                        cashoutRequest.setCardNo(bundle_card_no);
                        cashoutRequest.setStatus("Success");
                        paymentVerificationCashPresenter.logCashout(PaymentVerificationActivity.this,
                                token,cashoutRequest);

                        /*
                         * The returned String array has the following element
                         * with the indices below 0-Response Code 1-Response
                         * 2-Terminal ID 3-PAN 4-Card holder name 5-Date
                         * 6-Amount 7-Transaction ID 8-RRN
                         */
                        /*for (int i = 0; i < resultarray.length; i++) {
                            Toast.makeText(getApplicationContext(),
                                    resultarray[i], Toast.LENGTH_SHORT).show();
                        }*/
                    } else if (result.equals("reversal")) {
                        Utility.hideProgressDialog(PaymentVerificationActivity.this);

                        Toast.makeText(getApplicationContext(),
                                reversalResult, Toast.LENGTH_SHORT).show();

                    } else if (result.equals("fcmbresponse")) {
                        Utility.hideProgressDialog(PaymentVerificationActivity.this);

//                        byte[] dectryptArray = encryptedMessage.getBytes();
//                        byte[] decarray = Base64.decodeBase64(dectryptArray);
//                        String message = null;
//                        try {
//                            message = new String(decarray,"UTF-8");
//                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
//                        }
//
//                        Log.i("Enc:", encryptedMessage);
//                        Log.i("Msg:", message);
//
//                        Toast.makeText(getApplicationContext(),
//                                message, Toast.LENGTH_LONG).show();

                    }
                }
            }

        }
    }
}
