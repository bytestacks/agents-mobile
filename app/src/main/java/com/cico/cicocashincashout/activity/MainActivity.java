package com.cico.cicocashincashout.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.fragment.home_fragment.HomeFragment;
import com.cico.cicocashincashout.fragment.SettingsFragment;
import com.cico.cicocashincashout.fragment.transaction_history.TransactionHistoryFragment;
import com.cico.cicocashincashout.fragment.wallet_logs.WalletLogsFragment;
import com.cico.cicocashincashout.utils.AppNavigator;
import com.cico.cicocashincashout.utils.Preferences;
import com.cico.cicocashincashout.utils.Utility;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity{

    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawer;
    TextView agent_name, agent_balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,R.id.fund_wallet,R.id.agent_withdraw,R.id.activate_pinpad, R.id.nav_tran_history, R.id.nav_chat,
                R.id.nav_call, R.id.nav_mail, R.id.nav_settings, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this,R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        setControls(MainActivity.this,navigationView);


        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.activate_pinpad:

                    getSupportActionBar().setTitle("Activate Pinpad");
                    new AppNavigator(MainActivity.this).navigateToActivatePinpad();
                    break;

                case R.id.fund_wallet:
                    getSupportActionBar().setTitle("Fund Wallet");
                    new AppNavigator(MainActivity.this).navigateToFundWallet();
                    break;

                case R.id.agent_withdraw:
                    getSupportActionBar().setTitle("Wallet Withdraw");
                    new AppNavigator(MainActivity.this).navigateToWalletWithdraw();
                    break;

                case R.id.nav_chat:
                    String shareBody = "chat with us";
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "hello");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I want to make an Inquiry");
                    startActivity(Intent.createChooser(sharingIntent, shareBody));
                    break;
                case R.id.nav_home :
                    getSupportActionBar().setTitle("Home");
                    HomeFragment homeFragment = new HomeFragment();
                    swtichView(homeFragment);
                    break;
                case R.id.nav_call:
                    Intent i = new Intent(Intent.ACTION_DIAL);
                    i.setData(Uri.parse("tel:07036650669"));
                    if (i.resolveActivity(getPackageManager()) != null) {
                        startActivity(i);
                    }
                    break;

                case R.id.nav_mail:
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, "support@cicomobile.com");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Email from app");
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                    break;

                case R.id.nav_settings:
                    getSupportActionBar().setTitle("Settings");
                    SettingsFragment settingsFragment = new SettingsFragment();
                    swtichView(settingsFragment);
                    break;

                case R.id.nav_tran_history:
                    getSupportActionBar().setTitle("Transaction History");
                    TransactionHistoryFragment transactionHistoryFragment = new TransactionHistoryFragment();
                    swtichView(transactionHistoryFragment);
                    break;

                case R.id.nav_wallet_history:
                    getSupportActionBar().setTitle("Wallet Logs");
                    WalletLogsFragment walletLogsFragment = new WalletLogsFragment();
                    swtichView(walletLogsFragment);
                    break;

                case R.id.nav_password:
                    getSupportActionBar().setTitle("Wallet Logs");
                    new AppNavigator(MainActivity.this).navigateToCreatePassword();
                    break;

                case R.id.nav_logout:
                    clearPreferences(MainActivity.this);
                    new AppNavigator(MainActivity.this).navigateToLoginScreen();
                    break;

                default:
                    //showNews(menuItem);
                    break;
            }

            drawer.closeDrawers();
            return true;
        });

    }

    private void swtichView(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @SuppressLint("NewApi")
    public static void whatsapp(Activity activity, String phone) {
        String formattedNumber = PhoneNumberUtils.formatNumber(phone);
        try{
            Intent sendIntent =new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT,"");
            sendIntent.putExtra("jid", formattedNumber +"@s.whatsapp.net");
            sendIntent.setPackage("com.whatsapp");
            activity.startActivity(sendIntent);
        }
        catch(Exception e)
        {
            Toast.makeText(activity,"No WhatsApp Installed",Toast.LENGTH_SHORT).show();
        }
    }

    private void setControls(Context context, NavigationView navigationView){
        View headerView = navigationView.getHeaderView(0);
        agent_name = (TextView) headerView.findViewById(R.id.agent_name);
        agent_name.setText(Preferences.getAgentName(context));

        agent_balance = (TextView) headerView.findViewById(R.id.agent_balance);
        agent_balance.setText(Utility.formatCurrency(Double.parseDouble(Preferences.getAgentBalance(context))));
    }

    private void clearPreferences(Context context){
        Preferences.setDiscoList(context,"");
        Preferences.setAgentPhone(context,"");
        Preferences.setAgentToken(context,"");
        Preferences.setAgentId(context,"");
        Preferences.setAgentBalance(context,"");
        Preferences.setAgentName(context,"");
        Preferences.setDstvList(context,"");
        Preferences.setGotvList(context,"");
        Preferences.setWalletId(context,"");
        Preferences.setBankList(context,"");
        Preferences.setTransHistory(context,"");
    }
}
