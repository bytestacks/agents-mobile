package com.cico.cicocashincashout.activity.airtime;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.utils.AppNavigator;

public class ActivityAirtimeVending extends AppCompatActivity implements AirtimeContract.AirtimeView {

    ImageView back_btn,img_contact;
    Spinner spinner_bank;
    EditText text_amount,cust_text;
    TextView proceed_buttons;
    String phone,network, telco_code, amount;
    private final int PICK_CONTACT = 666;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_vending);

        initControls();
    }

    private void initControls() {

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        img_contact = (ImageView) findViewById(R.id.img_contact);
        img_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

        spinner_bank = (Spinner) findViewById(R.id.spinner_bank);
        spinner_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                network = spinner_bank.getSelectedItem().toString();
                if(network.equalsIgnoreCase("9 Mobile")){
                    telco_code = "A02E";
                }else if(network.equalsIgnoreCase("Airtel")){
                    telco_code = "A01E";
                }else if(network.equalsIgnoreCase("MTN")){
                    telco_code = "A04E";
                }else if(network.equalsIgnoreCase("Globacom")){
                    telco_code = "A03E";
                }

                // Toast.makeText(ActivityAirtimeVending.this, text, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cust_text = (EditText) findViewById(R.id.cust_text);
        text_amount = (EditText) findViewById(R.id.text_amount);

        proceed_buttons = (TextView) findViewById(R.id.proceed_buttons);
        proceed_buttons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                amount = text_amount.getText().toString().trim();
                phone = cust_text.getText().toString().trim();

                Bundle bundle = new Bundle();
                bundle.putString("amount", amount);
                bundle.putString("phone",phone);
                bundle.putString("telco_code",telco_code);
                bundle.putString("telco",network);

                new AppNavigator(ActivityAirtimeVending.this).navigateToAirtimeVerification(bundle);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor cur =  this.getContentResolver().query(contactData, null, null, null, null);
                    if (cur == null) return;
                    try {
                        if (cur.moveToFirst()) {
                            int phoneIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                            cust_text.setText(cur.getString(phoneIndex));
                        }
                    }
                    finally {
                        cur.close();
                    }
                }
        }
    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void updateSuccess() {

    }
}
