package com.cico.cicocashincashout.activity.electricity;

import android.content.Context;

import com.cico.cicocashincashout.model.electricity.request.ValidateMeterNumberRequest;
import com.cico.cicocashincashout.model.electricity.response.ValidateMeterNumberResponse;

public class ElectricityPresenter implements ElectricityContract.ElectricityListener {

    private ElectricityContract.ElectricityView electricityView;
    private ElectricityInteractor electricityInteractor;

    public ElectricityPresenter(ElectricityContract.ElectricityView electricityView){
        this.electricityView = electricityView;
        electricityInteractor = new ElectricityInteractor(this);
    }

    @Override
    public void onValidateSuccess(Context context, ValidateMeterNumberResponse validateMeterNumberResponse) {
        electricityView.hideProgress();
        electricityView.validateMeterNumberSuccess(validateMeterNumberResponse);
    }

    @Override
    public void onValidateFailure(String message) {
        electricityView.hideProgress();
        electricityView.showToast(message);
    }

    public void validateMeterNumber(Context context, ValidateMeterNumberRequest validateMeterNumberRequest,
                                    String token){
        electricityView.showProgress();
        electricityInteractor.validateUserLoginDetails(context, validateMeterNumberRequest, token);
    }
}
