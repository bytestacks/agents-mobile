package com.cico.cicocashincashout.activity.bills_verification;

import android.content.Context;
import android.util.Log;

import com.cico.cicocashincashout.R;
import com.cico.cicocashincashout.interfaces.NetworkService;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.request.StartimesVendRequestModel;
import com.cico.cicocashincashout.model.cable_tv.startimes.vend.response.StartimesVendResponse;
import com.cico.cicocashincashout.model.dstv.vend.request.DstvVendRequestModel;
import com.cico.cicocashincashout.model.dstv.vend.response.DstvVendResponseModel;
import com.cico.cicocashincashout.services.RetrofitInstance;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ActivityBillsInteractor {

    final String STARTIME_VEND = "startime_vend";

    private ActivityBillsContract.ActivityBillsListener activityBillsListener;

    public ActivityBillsInteractor(ActivityBillsContract.ActivityBillsListener activityBillsListener) {
        this.activityBillsListener = activityBillsListener;
    }

    public void vend(Context context, String header, StartimesVendRequestModel startimesVendRequestModel) {
        validateStratimeNumberObserver(header,startimesVendRequestModel).subscribeWith(validateStratimeNumberObservable(context));
    }

    public Observable<StartimesVendResponse> validateStratimeNumberObserver(String header, StartimesVendRequestModel startimesVendRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .vendStartimes("Token " + header,startimesVendRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<StartimesVendResponse> validateStratimeNumberObservable(final Context context){
        return new DisposableObserver<StartimesVendResponse>() {
            @Override
            public void onNext(StartimesVendResponse discoListResponseModel) {

                Log.d(STARTIME_VEND, "DATA" + discoListResponseModel.getData());
                if(discoListResponseModel.getStatus().equalsIgnoreCase("Error")){
                    activityBillsListener.onFailure(discoListResponseModel.getMessage());
                }else{
                    activityBillsListener.onSuccess(context,discoListResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(STARTIME_VEND,"Error " + e);
                e.printStackTrace();
                activityBillsListener.onFailure(context.getString(R.string.login_error));
            }

            @Override
            public void onComplete() {
                Log.d(STARTIME_VEND,"Completed");
            }
        };
    }

    public void vendDstv(Context context, String header, DstvVendRequestModel dstvVendRequestModel) {

        vendDstvObserver(header,dstvVendRequestModel).subscribeWith(vendDstvObservable(context));
    }

    public Observable<DstvVendResponseModel> vendDstvObserver(String header, DstvVendRequestModel dstvVendRequestModel) {

        return RetrofitInstance.getRetrofitInstance().create(NetworkService.class)
                .dstvVend("Token " + header,dstvVendRequestModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public DisposableObserver<DstvVendResponseModel> vendDstvObservable(final Context context){
        return new DisposableObserver<DstvVendResponseModel>() {
            @Override
            public void onNext(DstvVendResponseModel discoListResponseModel) {

                Log.d(STARTIME_VEND, "DATA" + discoListResponseModel.getData());
                if(discoListResponseModel.getStatus().equalsIgnoreCase("Error")){
                    activityBillsListener.onFailure(discoListResponseModel.getMessage());
                }else{
                    activityBillsListener.onDstvSuccess(context,discoListResponseModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(STARTIME_VEND,"Error " + e);
                e.printStackTrace();
                activityBillsListener.onFailure("Validation Error");
            }

            @Override
            public void onComplete() {
                Log.d(STARTIME_VEND,"Completed");
            }
        };
    }
}
