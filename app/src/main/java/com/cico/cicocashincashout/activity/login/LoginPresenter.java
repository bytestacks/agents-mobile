package com.cico.cicocashincashout.activity.login;

import android.content.Context;

import com.cico.cicocashincashout.model.login.request.LoginRequestModel;
import com.cico.cicocashincashout.model.login.response.LoginResponseModel;
import com.cico.cicocashincashout.utils.Preferences;
import com.google.gson.Gson;

public class LoginPresenter implements LoginContract.LoginListener {

    private LoginContract.LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenter(LoginContract.LoginView loginView){
        this.loginView = loginView;
        loginInteractor = new LoginInteractor(this);
    }

    @Override
    public void onSuccess(Context context,LoginResponseModel loginResponseModel) {
        loginView.hideProgress();
        if(loginResponseModel.getData().getUser().getIsDefault() == 1){
            saveUserDetails(context, loginResponseModel);
            loginView.changePassword();
        }else{
            saveUserDetails(context, loginResponseModel);
            loginView.loginSuccess();
        }

    }

    @Override
    public void onFailure(String message) {
        loginView.hideProgress();
        loginView.showToast(message);
    }

    public void handleLoginAction(Context context, LoginRequestModel loginRequestModel){
        loginView.showProgress();
        loginInteractor.validateUserLoginDetails(context, loginRequestModel);
    }

    public void onDestroy(){
        loginView = null;
    }

    private void saveUserDetails(Context context, LoginResponseModel loginResponseModel){

        String agentFirstName = loginResponseModel.getData().getAgent().getFirstName();
        String agentLastName = loginResponseModel.getData().getAgent().getLastName();
        String agentName = agentFirstName + " " + agentLastName;
        String walletBalance = loginResponseModel.getData().getWallet().getCurrentBal();
        String agentId = loginResponseModel.getData().getAgent().getId().toString();
        String agentToken = loginResponseModel.getData().getToken();
        String agentUUID = loginResponseModel.getData().getAgent().getUuid();
        String agentPhone = loginResponseModel.getData().getAgent().getBusinessPhone();
        String walletId = loginResponseModel.getData().getWallet().getWalletId();
        String activationCode = loginResponseModel.getData().getUser().getActivationCode();
        int rangeOneKey = loginResponseModel.getData().getSettings().getCashout().get(0).getMaximum();
        int rangeOneVal = loginResponseModel.getData().getSettings().getCashout().get(0).getTransactionCost();
        int rangeTwoKey = loginResponseModel.getData().getSettings().getCashout().get(1).getMaximum();
        int rangeTwoVal = loginResponseModel.getData().getSettings().getCashout().get(1).getTransactionCost();
        int rangeThreeKey = loginResponseModel.getData().getSettings().getCashout().get(2).getMaximum();
        int rangeThreeVal = loginResponseModel.getData().getSettings().getCashout().get(2).getTransactionCost();
        int stampDutyOne = loginResponseModel.getData().getSettings().getCashout().get(0).getStampDuty();
        int stampDutyTwo = loginResponseModel.getData().getSettings().getCashout().get(1).getStampDuty();
        int stampDutyThree = loginResponseModel.getData().getSettings().getCashout().get(2).getStampDuty();


        Gson gson = new Gson();
        String transactionList = gson.toJson(loginResponseModel);

        Preferences.setAgentName(context, agentName);
        Preferences.setAgentBalance(context,walletBalance);
        Preferences.setAgentId(context,agentId);
        Preferences.setAgentToken(context, agentToken);
        Preferences.setAgentPhone(context,agentPhone);
        Preferences.setWalletId(context, walletId);
        Preferences.setTransHistory(context, transactionList);
        Preferences.setActivationCode(context,activationCode);
        Preferences.setAgentUUIDCode(context, agentUUID);
        Preferences.setCashoutRangeOne(context,rangeOneKey);
        Preferences.setCashoutRangeTwo(context,rangeTwoKey);
        Preferences.setCashoutRangeThree(context,rangeThreeKey);
        Preferences.setCashoutRangeOneVal(context,rangeOneVal);
        Preferences.setCashoutRangeTwoVal(context,rangeTwoVal);
        Preferences.setCashoutRangeThreeVal(context,rangeThreeVal);
        Preferences.setCashoutStampDutyOne(context,stampDutyOne);
        Preferences.setCashoutStampDutyTwo(context,stampDutyTwo);
        Preferences.setCashoutStampDutyThree(context,stampDutyThree);
    }
}
