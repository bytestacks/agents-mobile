package com.cico.cicocashincashout.activity.airtime;

public interface AirtimeContract {

    interface AirtimeView {

        void showToast(String message);

        void showProgress();

        void hideProgress();

        void updateSuccess();
    }
}
