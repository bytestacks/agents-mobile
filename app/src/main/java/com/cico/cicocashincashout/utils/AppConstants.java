package com.cico.cicocashincashout.utils;

public class AppConstants {

    public static final String USER_TYPE = "agent";

    public static final String CURRENCY_NAIRA =  "₦";

    public static final String CONVINENCE_FEE = "20";

    public static final String CICO = "CICO_";

    // MPOS CONSTANTS
    public static final int REQUEST_DEVICE = 2;
    public static final String PAYPAD_SERVICE = "com.esl.paypadservice";
    public static final String PAYPAD_INTENT_SERVICE = "com.paypad.activities.DeviceActivity";
    public static final String PAYPAD_INIALIZE_INTENT  = "com.paypad.models.messaging.nibss.InitializeIntentService";
    public static final String PAYPAD_ACTIVATE_INTENT = "com.paypad.models.messaging.nibss.ActivationIntentService";
    public static final String PAYPAD_PAY_INTENT = "com.paypad.models.messaging.nibss.PayIntentService";
    public static final String MPOS_RESPONSE = "response";
    public static final String MPOS_RESPONSE_ARRAY = "responsearray";
    public static final String MPOS_STOP_INSERT_START_BUSY = "stopInsertStartBusy";
    public static final String MPOS_STOP_BUSY_START_PROCESSING = "stopBusyStartProcessing";
    public static final String MPOS_STOP = "stop";
    public static final String MPOS_TRANSACTION_RESPONSE = "transactionresponse";
    public static final String MPOS_ACCOUNT_TYPE = "accountType";
    public static final String MPOS_AMOUNT = "amount";
    public static final String MPOS_ACTIVATION_CODE = "activationCode";
    public static final String MPOS_ACTIVATION_COMPLETE = "activatecomplete";
    public static final String MPOS_INVALID_CODE = "invalidcode";
    public static final String MPOS_FAILED_ACTIVATION = "failedactivation";
    public static final String MPOS_INITIALIZE_COMPLETE = "initializecomplete";
    public static final String MPOS_INITIALIZE_NOT_COMPLETE = "initializenotcomplete";
    public static final int MPOS_RESULT_CODE = -1;


    //PAYPAD SUCCESS MESSAGE
    public  static final String MPOS_APPROVED_OR_COMPLETED_SUCCESSFULLY  = "Approved or completed successfully";
    public  static final String MPOS_TRANSACTION_SUCCESSFUL = "TRANSACTION SUCCESSFUL";

    //PAYPAD ERROR TYPE
    public static final String MPOS_UNCOMPLETED_TRANSACTION = "UNCOMPLETED TRANSACTION";
    public static final String MPOS_INVALID_MERCHANT = "Invalid merchant";
    public static final String MPOS_DO_NOT_HONOR = "Do not honor";
    public static final String MPOS_ERROR = "Error";
    public static final String MPOS_INVALID_TRANSACTION = "Invalid transaction";
    public static final String MPOS_INVALID_AMOUNT = "Invalid amount";
    public static final String MPOS_INVALID_CARD_NUMBER = "Invalid card number";
    public static final String MPOS_NO_SUCH_USER = "No such issuer";
    public static final String MPOS_CUSTOMER_CANCELLATION = "Customer cancellation";
    public static final String MPOS_RE_ENTER_TRANSACTION = "Re-enter transaction";
    public static final String MPOS_INVALID_RESPONSE = "Invalid response";
    public static final String MPOS_NO_ACTION_TAKEN = "No action taken";
    public static final String MPOS_SUSPECTED_MALFUNCTION = "Suspected malfunction";
    public static final String MPOS_UNACCEPTED_TRANSAFCTION_FEE = "Unacceptable transaction fee";
    public static final String MPOS_FILE_UPPDATE_NOT_SUPPORTED = "File update not supported";
    public static final String MPOS_UNABLE_TO_LOCATE_RECORD = "Unable to locate record";
    public static final String MPOS_DUPLICATE_RECORD = "Duplicate record";
    public static final String MPOS_FILE_UPDATE_EDIT_ERROR = "File update edit error";
    public static final String MPOS_FILE_UPDATE_LOCKED = "File update file locked";
    public static final String MPOS_FILE_UPDATE_FAILED = "File update failed";
    public static final String MPOS_FORMAT_ERROR = "Format error";
    public static final String MPOS_BANK_NOT_SUPPORTED = "Bank not supported";
    public static final String MPOS_COMPLETED_PARTIALLY = "Completed partially";
    public static final String MPOS_EXPIRED_CARD_PICK_UP = "Expired card, pick-up";
    public static final String MPOS_SUSPECTED_FRAUD_PICK_UP = "Suspected fraud, pick-up";
    public static final String MPOS_CONTACT_ACQUIRER_PICK_UP = "Contact acquirer, pick-up";
    public static final String MPOS_RESTRICTED_CARD_PICK_UP = "Restricted card, pick-up";
    public static final String MPOS_CALL_AQUIRER_SECURITY_PICK_UP = "Call acquirer security, pick-up";
    public static final String MPOS_PIN_TRIES_EXCEEDED_PICK_UP = "PIN tries exceeded, pick-up";
    public static final String MPOS_NO_CREDIT_AMOUNT = "No credit account";
    public static final String MPOS_LOST_CARD = "Lost card";
    public static final String MPOS_FUNCTION_NOT_SUPPORTED = "Function not supported";
    public static final String MPOS_UNIVERSAL_ACCOUNT = "No universal account";
    public static final String MPOS_STOLEN_CARD = "Stolen card";
    public static final String MPOS_NO_INVESTMENT_ACCOUNT = "No investment account";
    public static final String MPOS_NOT_SUFFICIENT_FUNDS = "Not sufficient funds";
    public static final String MPOS_NO_CHECK_ACCOUNT = "No check account";
    public static final String MPOS_NO_SAVINGS_ACCOUNT = "No savings account";
    public static final String MPOS_EXPIRED_CARD = "Expired card";
    public static final String MPOS_INCORRECT_PIN = "Incorrect PIN";
    public static final String MPOS_NO_CARD_RECORD = "No card record";
    public static final String MPOS_TRANSACTION_NOT_PERMITTED_TO_CARDHOLDER = "Transaction not permitted to cardholder";
    public static final String MPOS_TRANSACTION_NOT_PERMITTED_ON_TERMINAL = "Transaction not permitted on terminal";
    public static final String MPOS_SUSPECTED_FRAUD = "Suspected fraud";
    public static final String MPOS_CONTACT_ACQUIRER = "Contact acquirer";
    public static final String MPOS_EXCEEDS_TRANSACTION_LIMIT = "Exceeds withdrawal limit";
    public static final String MPOS_RESTRICTED_CARD = "Restricted card";
    public static final String MPOS_SECURITY_VIOLATION = "Security violation";
    public static final String MPOS_ORIGINAL_AMOUNT_INCORRECT = "Original amount incorrect";
    public static final String MPOS_EXCEEDS_WITHDRAWAL_FREQUENCY = "Exceeds withdrawal frequency";
    public static final String MPOS_CALL_ACQUIRER_SECURITY = "Call acquirer security";
    public static final String MPOS_HARD_CAPTURE = "Hard capture";
    public static final String MPOS_RESPONSE_RECEIVED_TOO_LATE = "Response received too late";
    public static final String MPOS_PIN_TRIES_EXCEEDED = "PIN tries exceeded";
    public static final String MPOS_INTERVENE_BANK_APPROVAL_REQUIRED = "Intervene, bank approval required";
    public static final String MPOS_INTERVENE_BANK_APPROVAL_REQUIRED_FOR_PARTIAL_AMOUNT = "Intervene, bank approval required for partial amount";
    public static final String MPOS_CUT_OFF_IN_PROGRESS = "Cut-off in progress";
    public static final String MPOS_ISSUER_OR_SWITCH_INOPERATIVE = "Issuer or switch inoperative";
    public static final String MPOS_ROUTING_ERROR = "Routing error";
    public static final String MPOS_VIOLATION_OF_LAW = "Violation of law";
    public static final String MPOS_DUPLICATE_TRANSACTION = "Duplicate transaction";
    public static final String MPOS_RECONCILE_ERROR = "Reconcile error";
    public static final String MPOS_SYSTEM_MALFUNCTION = "System malfunction";
    public static final String MPOS_EXCEEDS_LIMIT = "Exceeds cash limit";

    //PAYPAD PAYMENT RETRY
    public static final String RETRY_PAYMENT = "retrypayment";




}
