package com.cico.cicocashincashout.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.Toast;

import com.cico.cicocashincashout.R;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utility {

    public static ProgressDialog pbar;

    public static void shortToast(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public static void longToast(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }


    public static void showProgressDialog(Context context, boolean cancelable) {
        if (pbar == null) {
            pbar = new ProgressDialog(context);
        } else if (pbar != null) {
            pbar.dismiss();
            pbar = new ProgressDialog(context);
        }

        pbar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pbar.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.custom_dialog));
        pbar.setCancelable(cancelable);
        pbar.setIndeterminate(true);
        pbar.show();
        pbar.setContentView(R.layout.layout_custom_dialog);
    }


    public static void showRecentProgressDialog(Context context, boolean cancelable) {
        if (pbar == null) {
            pbar = new ProgressDialog(context);
        } else if (pbar != null) {
            pbar.dismiss();
            pbar = new ProgressDialog(context);
        }

        pbar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pbar.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.custom_dialog));
        pbar.setCancelable(cancelable);
        pbar.setIndeterminate(true);
        pbar.show();
        pbar.setContentView(R.layout.layout_custom_dialog);
    }

    public static void hideRecentProgressDialog(Activity activity) {
        if (pbar != null && !activity.isFinishing())
            pbar.dismiss();
        pbar = null;
    }

    public static void hideProgressDialog(Activity activity) {
        if (pbar != null && !activity.isFinishing())
            pbar.dismiss();
        pbar = null;
    }

    public static String formatCurrency(double price){
        return AppConstants.CURRENCY_NAIRA+ formatCurrencyWithoutSymbol(price);
    }

    public static String formatCurrencyWithoutSymbol(double price) {
        double formattedPrice;
        String nairaPrice;
        try {
            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setDecimalSeparatorAlwaysShown(false);
            if(price - ((int)price) !=0){
                formattedPrice = price;
            } else{
                formattedPrice = (int)price;
            }

            nairaPrice = decimalFormat.format(formattedPrice);
        } catch (final Exception e){
            nairaPrice = ("0.00");
        }

        return nairaPrice;
    }

    public static String randomNumber(int len){
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(len);
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public static String getPresentDate(){

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }
}
