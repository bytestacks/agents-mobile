package com.cico.cicocashincashout.utils;

public class UrlConstants {

    public static final String BASE_URL = "http://api.cico.ng/api/"; //"http://api-dev.cico.ng/api/";//"http://api.cico.ng/api/";

    public static final String ACTIVATE_AGENT_URL = "agents/activate";

    public static final String LOGIN_USER = "users/login";

    public static final String AGENT_WALET_LOGS = "agents/wallets";

    public static final String AGENT_TRANSACTION_HISTORY = "transactions";

    public static final String UPDATE_PASSWORD = "users";

    public static final String VALIDATE_METER_NUMBER = "energy";

    public static final String VEND_AIRTIME = "airtime";

    public static final String DATA_PLAN = "airtime/data/plans";

    public static final String VEND_DATA = "airtime/data";

    public static final String DISCO_LIST = "energy/discos";

    public static final String DSTV_PLAN = "cable/multichoice/dstv";

    public static final String GOTV_PLAN = "cable/multichoice/gotv";

    public static final String DSTV_VALIDATE_PLAN = "cable/multichoice/validate";

    public static final String DSTV_VEND = "cable/multichoice/vend";

    public static final String STARTIMES_VALIDATE = "cable/startimes/validate";

    public static final String STARTIME_VEND = "cable/startimes/vend";

    public static final String BANK_ACCT_VALIDATE = "bank-operations/verify-account";//"recipient/verify";

    public static final String CASHLESS_PAYMENT = "transactions/cashless";

    public static final String BANK_LIST = "bank-operations/banks";//"banks/lists";

    public static final String DISBURSE_FUND = "bank-operations/disburse-fund";

    public static final String CASHOUT_URL = "transactions/cashout";

    public static final String INITIATE_CASHOUT = "transactions/initiate-cashout";

    public static final String AGENT_FUND_WALLET = "agents/fund-wallet";

    public static final String AGENT_WITHDRAW = "agents/wallets/withdraw";

    public static final String AGENT_INFO_URL = "agents/info/{id}";
}
