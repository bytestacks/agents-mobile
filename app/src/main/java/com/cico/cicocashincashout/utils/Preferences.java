package com.cico.cicocashincashout.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    public static final String PREF_NAME = "CICO";

    public static final String AGENT_NAME = "agent_name";

    public static final String AGENT_BALANCE = "agent_balance";

    public static final String AGENT_ID = "agent_id";

    public static final String AGENT_TOKEN = "agent_token";

    public static final String AGENT_ACTIVATED = "agent_activated";

    public static final String AGENT_PHONE  = "agent_phone";

    public static final String DISCO_LIST = "disco_list";

    public static final String DSTV_PRICE = "dstv_price_lisT";

    public static final String GOTV_PRICE = "gotv_price_lisT";

    public static final String DATA_PLAN = "data_plan_lisT";

    public static final String WALLET_ID = "wallet_id";

    public static final String BANK_LIST = "bank_list";

    public static final String TRANS_HISTORY = "trans_history";

    public static final String MPOS_STATUS = "mpos_status";

    public static final String ACTIVATION_CODE = "activation_code";

    public static final String AGENT_UUID_CODE = "agent_uuid_code";

    public static final String CASHOUT_RANGE_ONE_KEY = "cashout_range_one";

    public static final String CASHOUT_RANGE_TWO_KEY = "cashout_range_two";

    public static final String CASHOUT_RANGE_THREE_KEY = "cashout_range_three";

    public static final String CASHOUT_RANGE_ONE_VAL = "cashout_range_one_val";

    public static final String CASHOUT_RANGE_TWO_VAL = "cashout_range_two_val";

    public static final String CASHOUT_RANGE_THREE_VAL = "cashout_range_three_val";

    public static final String CASHOUT_STAMP_DUTY_ONE = "cash_out_stamp_duty_one";

    public static final String CASHOUT_STAMP_DUTY_TWO = "cash_out_stamp_duty_two";

    public static final String CASHOUT_STAMP_DUTY_THREE = "cash_out_stamp_duty_three";


    public static int getCashoutStampDutyOne(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_STAMP_DUTY_ONE,0);
    }

    public static void setCashoutStampDutyOne(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_STAMP_DUTY_ONE, cashoutRange);
        editor.apply();
    }

    public static int getCashoutStampDutyTwo(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_STAMP_DUTY_TWO,0);
    }

    public static void setCashoutStampDutyTwo(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_STAMP_DUTY_TWO, cashoutRange);
        editor.apply();
    }

    public static int getCashoutStampDutyThree(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_STAMP_DUTY_THREE,0);
    }

    public static void setCashoutStampDutyThree(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_STAMP_DUTY_THREE, cashoutRange);
        editor.apply();
    }

    public static int getCashoutRangeOneVal(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_RANGE_ONE_VAL,0);
    }

    public static void setCashoutRangeOneVal(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_RANGE_ONE_VAL, cashoutRange);
        editor.apply();
    }

    public static int getCashoutRangeTwoVal(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_RANGE_TWO_VAL,0);
    }

    public static void setCashoutRangeTwoVal(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_RANGE_TWO_VAL, cashoutRange);
        editor.apply();
    }

    public static int getCashoutRangeThreeVal(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_RANGE_THREE_VAL,0);
    }

    public static void setCashoutRangeThreeVal(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_RANGE_THREE_VAL, cashoutRange);
        editor.apply();
    }

    public static int getCashoutRangeOne(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_RANGE_ONE_KEY,0);
    }

    public static void setCashoutRangeOne(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_RANGE_ONE_KEY, cashoutRange);
        editor.apply();
    }

    public static int getCashoutRangeTwo(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_RANGE_TWO_KEY,0);
    }

    public static void setCashoutRangeTwo(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_RANGE_TWO_KEY, cashoutRange);
        editor.apply();
    }

    public static int getCashoutRangeThree(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getInt(CASHOUT_RANGE_THREE_KEY,0);
    }

    public static void setCashoutRangeThree(Context context, int  cashoutRange){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(CASHOUT_RANGE_THREE_KEY, cashoutRange);
        editor.apply();
    }

    public static String getAgentUUIDCode(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(AGENT_UUID_CODE,"");
    }

    public static void setAgentUUIDCode(Context context, String  agentUUIDCode){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(AGENT_UUID_CODE, agentUUIDCode);
        editor.apply();
    }

    public static String getActivationCode(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(ACTIVATION_CODE,"");
    }

    public static void setActivationCode(Context context, String activationCode){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(ACTIVATION_CODE, activationCode);
        editor.apply();
    }

    public static String getTransHistory(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(TRANS_HISTORY,"");
    }

    public static void setTransHistory(Context context, String tranList){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(TRANS_HISTORY, tranList);
        editor.apply();
    }

    public static String getDataPlan(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(DATA_PLAN,"");
    }

    public static void setDataPlan(Context context, String discoList){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(DATA_PLAN, discoList);
        editor.apply();
    }

    public static void setGotvList(Context context, String discoList){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(GOTV_PRICE, discoList);
        editor.apply();
    }

    public static String getGotvList(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(GOTV_PRICE,"");
    }

    public static void setDstvList(Context context, String discoList){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(DSTV_PRICE, discoList);
        editor.apply();
    }

    public static String getDstvList(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(DSTV_PRICE,"");
    }

    public static void setDiscoList(Context context, String discoList){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(DISCO_LIST, discoList);
        editor.apply();
    }

    public static String getDiscoList(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(DISCO_LIST,"");
    }

    public static void setAgentName(Context context, String agentName){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(AGENT_NAME, agentName);
        editor.apply();
    }

    public static String getAgentName(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(AGENT_NAME,"");
    }

    public static void setAgentPhone(Context context, String agentPhone){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(AGENT_PHONE, agentPhone);
        editor.apply();
    }

    public static String getAgentPhone(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(AGENT_PHONE,"");
    }

    public static void setAgentBalance(Context context, String agentBalance){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(AGENT_BALANCE, agentBalance);
        editor.apply();
    }

    public static String getAgentBalance(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(AGENT_BALANCE,"");
    }

    public static void setAgentActivated(Context context, boolean isActivated){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(AGENT_ACTIVATED, isActivated);
        editor.apply();
    }

    public static boolean isAgentActivated(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getBoolean(AGENT_ACTIVATED,false);
    }

    public static void setAgentId(Context context, String agentId){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(AGENT_ID, agentId);
        editor.apply();
    }

    public static String getAgentId(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(AGENT_ID,"");
    }

    public static void setAgentToken(Context context, String agentToken){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(AGENT_TOKEN, agentToken);
        editor.apply();
    }

    public static String getAgentToken(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(AGENT_TOKEN,"");
    }

    public static void setWalletId(Context context, String walletId) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(WALLET_ID, walletId);
        editor.apply();
    }

    public static String getWalletId(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(WALLET_ID,"");
    }

    public static void setBankList(Context context, String bankList){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(BANK_LIST, bankList);
        editor.apply();
    }

    public static String getBankList(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME,0);
        return sharedPreferences.getString(BANK_LIST,"");
    }
}
