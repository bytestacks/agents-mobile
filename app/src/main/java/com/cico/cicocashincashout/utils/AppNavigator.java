package com.cico.cicocashincashout.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.cico.cicocashincashout.activity.ActivatePinpad;
import com.cico.cicocashincashout.activity.agent_withdrawal.AgentWithdrawalActivity;
import com.cico.cicocashincashout.activity.bills_verification.ActivityBillsVerification;
import com.cico.cicocashincashout.activity.airtime.airtime_verification.ActivityAirtimeVerification;
import com.cico.cicocashincashout.activity.data.vending.ActivityDataVerification;
import com.cico.cicocashincashout.activity.electricity.ActivityElectricPayment;
import com.cico.cicocashincashout.activity.ActivityAgentTransfer;
import com.cico.cicocashincashout.activity.airtime.ActivityAirtimeVending;
import com.cico.cicocashincashout.activity.bills_payment.ActivityBillsPayment;
import com.cico.cicocashincashout.activity.fund_transfer.ActivityFundTransfer;
import com.cico.cicocashincashout.activity.data.ActivityMobileData;
import com.cico.cicocashincashout.activity.fund_wallet.FundWalletActivity;
import com.cico.cicocashincashout.activity.tv_subscription.ActivityTvSubscription;
import com.cico.cicocashincashout.activity.ActivityVerificationCode;
import com.cico.cicocashincashout.activity.AgentFundTransferActivity;
import com.cico.cicocashincashout.activity.AgentFundVerificationActivity;
import com.cico.cicocashincashout.activity.cash_withdrawal.CashWithdrawalActivity;
import com.cico.cicocashincashout.activity.CicoTestActivity;
import com.cico.cicocashincashout.activity.UpdatePassword.CreatePasswordActivity;
import com.cico.cicocashincashout.activity.FundPurseActivity;
import com.cico.cicocashincashout.activity.MainActivity;
import com.cico.cicocashincashout.activity.payment_cash_verf.PaymentVerificationActivity;
import com.cico.cicocashincashout.activity.payment_verification.PaymentVerificationFinal;
import com.cico.cicocashincashout.activity.PinpadProcessActivity;
import com.cico.cicocashincashout.activity.PinpadSetupActivity;
import com.cico.cicocashincashout.activity.SelectSerialActivity;
import com.cico.cicocashincashout.activity.SuccessActivity;
import com.cico.cicocashincashout.activity.activate_agent.ActivateAgentActivity;
import com.cico.cicocashincashout.activity.electricity.bill_verification.ElectricityBillVerification;
import com.cico.cicocashincashout.activity.login.LoginActivity;

public class AppNavigator {

    private final Activity activity;

    public AppNavigator(Activity activity){
        this.activity = activity;
    }

    public void navigateToMainScreen(){
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToLoginScreen(){
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToActivatePinpad(){
        Intent intent = new Intent(activity, ActivatePinpad.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToCreatePassword(){
        Intent intent = new Intent(activity, CreatePasswordActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }
    public void navigateToPinpadSetup(){
        Intent intent = new Intent(activity, PinpadSetupActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToSelectSerial(){
        Intent intent = new Intent(activity, SelectSerialActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToCicoTest(){
        Intent intent = new Intent(activity, CicoTestActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToCashwithdrawal(){
        Intent intent = new Intent(activity, CashWithdrawalActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToPinpadProcess(){
        Intent intent = new Intent(activity, PinpadProcessActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToPaymentVerification(Bundle bundle){
        Intent intent = new Intent(activity, PaymentVerificationActivity.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToSuccess(Bundle bundle){
        Intent intent = new Intent(activity, SuccessActivity.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToFundPurse(){
        Intent intent = new Intent(activity, FundPurseActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToAgentTransfer(){
        Intent intent = new Intent(activity, ActivityAgentTransfer.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToAgentTransferFund(){
        Intent intent = new Intent(activity, AgentFundTransferActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToAgentTransferFundVerification(){
        Intent intent = new Intent(activity, AgentFundVerificationActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToFundTransfer(){
        Intent intent = new Intent(activity, ActivityFundTransfer.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToAirtime() {
        Intent intent = new Intent(activity, ActivityAirtimeVending.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);

    }

    public void navigateToBillsPayment() {

        Intent intent = new Intent(activity, ActivityBillsPayment.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToelectricPayment(Bundle bundle) {

        Intent intent = new Intent(activity, ActivityElectricPayment.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToTvSubs(Bundle bundle) {

        Intent intent = new Intent(activity, ActivityTvSubscription.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToMobileData(Bundle bundle) {

        Intent intent = new Intent(activity, ActivityMobileData.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToBillVerification(Bundle bundle) {

        Intent intent = new Intent(activity, ActivityBillsVerification.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToDataVerification(Bundle bundle) {

        Intent intent = new Intent(activity, ActivityDataVerification.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToAirtimeVerification(Bundle bundle) {

        Intent intent = new Intent(activity, ActivityAirtimeVerification.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToElectricBillVerification(Bundle bundle) {

        Intent intent = new Intent(activity, ElectricityBillVerification.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToFinalPaymentVerification(Bundle bundle) {

        Intent intent = new Intent(activity, PaymentVerificationFinal.class);
        intent.putExtras(bundle);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }


    public void navigateToVerificationCode() {

        Intent intent = new Intent(activity, ActivityVerificationCode.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToActivateAgent() {

        Intent intent = new Intent(activity, ActivateAgentActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToFundWallet() {

        Intent intent = new Intent(activity, FundWalletActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }

    public void navigateToWalletWithdraw() {

        Intent intent = new Intent(activity, AgentWithdrawalActivity.class);
        activity.overridePendingTransition(0,0);
        activity.startActivity(intent);
    }
}
