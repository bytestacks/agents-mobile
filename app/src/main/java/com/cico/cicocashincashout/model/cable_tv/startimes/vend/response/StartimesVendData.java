package com.cico.cicocashincashout.model.cable_tv.startimes.vend.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesVendData {

    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("details")
    @Expose
    private StartimesVendDetails details;
    @SerializedName("transactionNo")
    @Expose
    private Long transactionNo;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public StartimesVendDetails getDetails() {
        return details;
    }

    public void setDetails(StartimesVendDetails details) {
        this.details = details;
    }

    public Long getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(Long transactionNo) {
        this.transactionNo = transactionNo;
    }
}
