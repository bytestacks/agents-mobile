package com.cico.cicocashincashout.model.cashless_withdrawal.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashWithdrawalRequestModel {

    @SerializedName("amount")
    @Expose
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
