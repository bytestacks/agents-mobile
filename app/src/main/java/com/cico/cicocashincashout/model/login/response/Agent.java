package com.cico.cicocashincashout.model.login.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Agent {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_address")
    @Expose
    private String businessAddress;
    @SerializedName("business_phone")
    @Expose
    private String businessPhone;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("local_government_id")
    @Expose
    private Integer localGovernmentId;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("account_name")
    @Expose
    private String accountName;
    @SerializedName("bank_id")
    @Expose
    private Integer bankId;
    @SerializedName("bvn")
    @Expose
    private String bvn;
    @SerializedName("agent_code")
    @Expose
    private String agentCode;
    @SerializedName("identity_type")
    @Expose
    private Object identityType;
    @SerializedName("business_type")
    @Expose
    private String businessType;
    @SerializedName("super_agent_id")
    @Expose
    private Object superAgentId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("wallet_no")
    @Expose
    private String walletNo;
    @SerializedName("terminal_id")
    @Expose
    private Object terminalId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("activation_code")
    @Expose
    private String activationCode;
    @SerializedName("bank_code")
    @Expose
    private String bankCode;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("api_key")
    @Expose
    private String apiKey;
    @SerializedName("commission_value")
    @Expose
    private Object commissionValue;
    @SerializedName("webhook")
    @Expose
    private Object webhook;
    @SerializedName("full_name")
    @Expose
    private String fullName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getLocalGovernmentId() {
        return localGovernmentId;
    }

    public void setLocalGovernmentId(Integer localGovernmentId) {
        this.localGovernmentId = localGovernmentId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBvn() {
        return bvn;
    }

    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Object getIdentityType() {
        return identityType;
    }

    public void setIdentityType(Object identityType) {
        this.identityType = identityType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Object getSuperAgentId() {
        return superAgentId;
    }

    public void setSuperAgentId(Object superAgentId) {
        this.superAgentId = superAgentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(String walletNo) {
        this.walletNo = walletNo;
    }

    public Object getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Object terminalId) {
        this.terminalId = terminalId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Object getCommissionValue() {
        return commissionValue;
    }

    public void setCommissionValue(Object commissionValue) {
        this.commissionValue = commissionValue;
    }

    public Object getWebhook() {
        return webhook;
    }

    public void setWebhook(Object webhook) {
        this.webhook = webhook;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


}
