package com.cico.cicocashincashout.model.cashout.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashoutRequest {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("card_holder_name")
    @Expose
    private String cardHolderName;
    @SerializedName("terminal_id")
    @Expose
    private String terminalId;
    @SerializedName("pan_no")
    @Expose
    private String panNo;
    @SerializedName("rrn")
    @Expose
    private String rrn;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("provider_ref")
    @Expose
    private String providerRef;
    @SerializedName("card_no")
    @Expose
    private String cardNo;
    @SerializedName("status")
    @Expose
    private String status;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getProviderRef() {
        return providerRef;
    }

    public void setProviderRef(String providerRef) {
        this.providerRef = providerRef;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
