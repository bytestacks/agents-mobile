package com.cico.cicocashincashout.model.cable_tv.startimes.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesResponseData {

    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("smartCardCode")
    @Expose
    private String smartCardCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSmartCardCode() {
        return smartCardCode;
    }

    public void setSmartCardCode(String smartCardCode) {
        this.smartCardCode = smartCardCode;
    }
}
