package com.cico.cicocashincashout.model.dstv.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailablePricingOption {

    @SerializedName("monthsPaidFor")
    @Expose
    private Integer monthsPaidFor;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("invoicePeriod")
    @Expose
    private Integer invoicePeriod;

    public Integer getMonthsPaidFor() {
        return monthsPaidFor;
    }

    public void setMonthsPaidFor(Integer monthsPaidFor) {
        this.monthsPaidFor = monthsPaidFor;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getInvoicePeriod() {
        return invoicePeriod;
    }

    public void setInvoicePeriod(Integer invoicePeriod) {
        this.invoicePeriod = invoicePeriod;
    }

}
