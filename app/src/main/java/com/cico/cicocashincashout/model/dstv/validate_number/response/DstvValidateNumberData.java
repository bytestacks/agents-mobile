package com.cico.cicocashincashout.model.dstv.validate_number.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DstvValidateNumberData {

    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusDescription")
    @Expose
    private StatusDescription statusDescription;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public StatusDescription getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(StatusDescription statusDescription) {
        this.statusDescription = statusDescription;
    }
}
