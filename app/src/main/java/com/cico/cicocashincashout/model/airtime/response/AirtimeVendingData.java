package com.cico.cicocashincashout.model.airtime.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirtimeVendingData {

    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("mReference")
    @Expose
    private String mReference;
    @SerializedName("tranxReference")
    @Expose
    private String tranxReference;
    @SerializedName("recipient")
    @Expose
    private String recipient;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("confirmCode")
    @Expose
    private String confirmCode;
    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getMReference() {
        return mReference;
    }

    public void setMReference(String mReference) {
        this.mReference = mReference;
    }

    public String getTranxReference() {
        return tranxReference;
    }

    public void setTranxReference(String tranxReference) {
        this.tranxReference = tranxReference;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public void setConfirmCode(String confirmCode) {
        this.confirmCode = confirmCode;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
}
