package com.cico.cicocashincashout.model.Bank.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClientInfo {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("bank_cbn_code")
    @Expose
    private Object bankCbnCode;
    @SerializedName("bank_name")
    @Expose
    private Object bankName;
    @SerializedName("console_url")
    @Expose
    private Object consoleUrl;
    @SerializedName("js_background_image")
    @Expose
    private Object jsBackgroundImage;
    @SerializedName("css_url")
    @Expose
    private Object cssUrl;
    @SerializedName("logo_url")
    @Expose
    private String logoUrl;
    @SerializedName("footer_text")
    @Expose
    private String footerText;
    @SerializedName("options")
    @Expose
    private List<String> options = null;
    @SerializedName("primary_color")
    @Expose
    private String primaryColor;
    @SerializedName("secondary_color")
    @Expose
    private String secondaryColor;
    @SerializedName("modal_logo_url")
    @Expose
    private Object modalLogoUrl;
    @SerializedName("primary_button_color")
    @Expose
    private String primaryButtonColor;
    @SerializedName("modal_background_color")
    @Expose
    private String modalBackgroundColor;
    @SerializedName("payment_option_color")
    @Expose
    private String paymentOptionColor;
    @SerializedName("payment_option_active_color")
    @Expose
    private String paymentOptionActiveColor;
    @SerializedName("app_color")
    @Expose
    private String appColor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getBankCbnCode() {
        return bankCbnCode;
    }

    public void setBankCbnCode(Object bankCbnCode) {
        this.bankCbnCode = bankCbnCode;
    }

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public Object getConsoleUrl() {
        return consoleUrl;
    }

    public void setConsoleUrl(Object consoleUrl) {
        this.consoleUrl = consoleUrl;
    }

    public Object getJsBackgroundImage() {
        return jsBackgroundImage;
    }

    public void setJsBackgroundImage(Object jsBackgroundImage) {
        this.jsBackgroundImage = jsBackgroundImage;
    }

    public Object getCssUrl() {
        return cssUrl;
    }

    public void setCssUrl(Object cssUrl) {
        this.cssUrl = cssUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getFooterText() {
        return footerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getSecondaryColor() {
        return secondaryColor;
    }

    public void setSecondaryColor(String secondaryColor) {
        this.secondaryColor = secondaryColor;
    }

    public Object getModalLogoUrl() {
        return modalLogoUrl;
    }

    public void setModalLogoUrl(Object modalLogoUrl) {
        this.modalLogoUrl = modalLogoUrl;
    }

    public String getPrimaryButtonColor() {
        return primaryButtonColor;
    }

    public void setPrimaryButtonColor(String primaryButtonColor) {
        this.primaryButtonColor = primaryButtonColor;
    }

    public String getModalBackgroundColor() {
        return modalBackgroundColor;
    }

    public void setModalBackgroundColor(String modalBackgroundColor) {
        this.modalBackgroundColor = modalBackgroundColor;
    }

    public String getPaymentOptionColor() {
        return paymentOptionColor;
    }

    public void setPaymentOptionColor(String paymentOptionColor) {
        this.paymentOptionColor = paymentOptionColor;
    }

    public String getPaymentOptionActiveColor() {
        return paymentOptionActiveColor;
    }

    public void setPaymentOptionActiveColor(String paymentOptionActiveColor) {
        this.paymentOptionActiveColor = paymentOptionActiveColor;
    }

    public String getAppColor() {
        return appColor;
    }

    public void setAppColor(String appColor) {
        this.appColor = appColor;
    }
}
