package com.cico.cicocashincashout.model.cable_tv.startimes.vend.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesVendRequestModel {


    @SerializedName("smartcard")
    @Expose
    private String smartcard;
    @SerializedName("amount")
    @Expose
    private Integer amount;

    public String getSmartcard() {
        return smartcard;
    }

    public void setSmartcard(String smartcard) {
        this.smartcard = smartcard;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
