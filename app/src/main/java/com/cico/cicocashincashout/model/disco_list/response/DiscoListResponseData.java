package com.cico.cicocashincashout.model.disco_list.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscoListResponseData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("buypower_code")
    @Expose
    private String buypowerCode;
    @SerializedName("active")
    @Expose
    private String active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBuypowerCode() {
        return buypowerCode;
    }

    public void setBuypowerCode(String buypowerCode) {
        this.buypowerCode = buypowerCode;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
