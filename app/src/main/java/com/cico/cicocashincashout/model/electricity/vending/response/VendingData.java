package com.cico.cicocashincashout.model.electricity.vending.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendingData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("amountGenerated")
    @Expose
    private String amountGenerated;
    @SerializedName("debtAmount")
    @Expose
    private String debtAmount;
    @SerializedName("disco")
    @Expose
    private String disco;
    @SerializedName("freeUnits")
    @Expose
    private String freeUnits;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("receiptNo")
    @Expose
    private String receiptNo;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("vendTime")
    @Expose
    private String vendTime;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("totalAmountPaid")
    @Expose
    private Integer totalAmountPaid;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("vendAmount")
    @Expose
    private String vendAmount;
    @SerializedName("vendRef")
    @Expose
    private String vendRef;
    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAmountGenerated() {
        return amountGenerated;
    }

    public void setAmountGenerated(String amountGenerated) {
        this.amountGenerated = amountGenerated;
    }

    public String getDebtAmount() {
        return debtAmount;
    }

    public void setDebtAmount(String debtAmount) {
        this.debtAmount = debtAmount;
    }

    public String getDisco() {
        return disco;
    }

    public void setDisco(String disco) {
        this.disco = disco;
    }

    public String getFreeUnits() {
        return freeUnits;
    }

    public void setFreeUnits(String freeUnits) {
        this.freeUnits = freeUnits;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getVendTime() {
        return vendTime;
    }

    public void setVendTime(String vendTime) {
        this.vendTime = vendTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(Integer totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getVendAmount() {
        return vendAmount;
    }

    public void setVendAmount(String vendAmount) {
        this.vendAmount = vendAmount;
    }

    public String getVendRef() {
        return vendRef;
    }

    public void setVendRef(String vendRef) {
        this.vendRef = vendRef;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

}
