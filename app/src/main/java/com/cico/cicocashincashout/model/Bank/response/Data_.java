package com.cico.cicocashincashout.model.Bank.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data_ {
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("provider_response_code")
    @Expose
    private String providerResponseCode;
    @SerializedName("errors")
    @Expose
    private Object errors;
    @SerializedName("error")
    @Expose
    private Object error;
    @SerializedName("account_info")
    @Expose
    private AccountInfo accountInfo;
    @SerializedName("client_info")
    @Expose
    private ClientInfo clientInfo;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderResponseCode() {
        return providerResponseCode;
    }

    public void setProviderResponseCode(String providerResponseCode) {
        this.providerResponseCode = providerResponseCode;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }
}
