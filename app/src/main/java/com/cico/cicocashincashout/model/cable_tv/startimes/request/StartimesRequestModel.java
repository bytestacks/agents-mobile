package com.cico.cicocashincashout.model.cable_tv.startimes.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesRequestModel {

    @SerializedName("smartcard")
    @Expose
    private String smartcard;

    public String getSmartcard() {
        return smartcard;
    }

    public void setSmartcard(String smartcard) {
        this.smartcard = smartcard;
    }
}
