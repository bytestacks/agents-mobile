package com.cico.cicocashincashout.model.Bank.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisburseResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DisburseData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DisburseData getData() {
        return data;
    }

    public void setData(DisburseData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
