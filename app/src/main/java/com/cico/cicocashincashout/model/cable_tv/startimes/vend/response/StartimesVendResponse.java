package com.cico.cicocashincashout.model.cable_tv.startimes.vend.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesVendResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private StartimesVendData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StartimesVendData getData() {
        return data;
    }

    public void setData(StartimesVendData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
