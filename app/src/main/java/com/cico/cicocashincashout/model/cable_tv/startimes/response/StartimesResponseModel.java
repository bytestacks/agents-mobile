package com.cico.cicocashincashout.model.cable_tv.startimes.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesResponseModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private StartimesResponseData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StartimesResponseData getData() {
        return data;
    }

    public void setData(StartimesResponseData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
