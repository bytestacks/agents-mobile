package com.cico.cicocashincashout.model.login.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Settings {
    @SerializedName("energy")
    @Expose
    private List<Object> energy = null;
    @SerializedName("cashout")
    @Expose
    private List<Cashout> cashout = null;
    @SerializedName("deposit")
    @Expose
    private List<Object> deposit = null;
    @SerializedName("airtime")
    @Expose
    private List<Airtime> airtime = null;
    @SerializedName("dstv")
    @Expose
    private List<Dstv> dstv = null;
    @SerializedName("gotv")
    @Expose
    private List<Gotv> gotv = null;
    @SerializedName("transfer")
    @Expose
    private List<Transfer> transfer = null;
    @SerializedName("startimes")
    @Expose
    private List<Startimes> startimes = null;
    @SerializedName("data")
    @Expose
    private List<Data_> data = null;

    public List<Object> getEnergy() {
        return energy;
    }

    public void setEnergy(List<Object> energy) {
        this.energy = energy;
    }

    public List<Cashout> getCashout() {
        return cashout;
    }

    public void setCashout(List<Cashout> cashout) {
        this.cashout = cashout;
    }

    public List<Object> getDeposit() {
        return deposit;
    }

    public void setDeposit(List<Object> deposit) {
        this.deposit = deposit;
    }

    public List<Airtime> getAirtime() {
        return airtime;
    }

    public void setAirtime(List<Airtime> airtime) {
        this.airtime = airtime;
    }

    public List<Dstv> getDstv() {
        return dstv;
    }

    public void setDstv(List<Dstv> dstv) {
        this.dstv = dstv;
    }

    public List<Gotv> getGotv() {
        return gotv;
    }

    public void setGotv(List<Gotv> gotv) {
        this.gotv = gotv;
    }

    public List<Transfer> getTransfer() {
        return transfer;
    }

    public void setTransfer(List<Transfer> transfer) {
        this.transfer = transfer;
    }

    public List<Startimes> getStartimes() {
        return startimes;
    }

    public void setStartimes(List<Startimes> startimes) {
        this.startimes = startimes;
    }

    public List<Data_> getData() {
        return data;
    }

    public void setData(List<Data_> data) {
        this.data = data;
    }


}
