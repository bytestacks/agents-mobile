package com.cico.cicocashincashout.model.cashout.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InitiateCashoutResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private InitiateCashoutData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public InitiateCashoutData getData() {
        return data;
    }

    public void setData(InitiateCashoutData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
