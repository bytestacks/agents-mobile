package com.cico.cicocashincashout.model.Bank.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cost {

    @SerializedName("commission")
    @Expose
    private Integer commission;
    @SerializedName("trans_cost")
    @Expose
    private Integer transCost;
    @SerializedName("super_agent_commission")
    @Expose
    private Float superAgentCommission;
    @SerializedName("cico_commission")
    @Expose
    private Integer cicoCommission;
    @SerializedName("stamp_duty")
    @Expose
    private Integer stampDuty;

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public Integer getTransCost() {
        return transCost;
    }

    public void setTransCost(Integer transCost) {
        this.transCost = transCost;
    }

    public Float getSuperAgentCommission() {
        return superAgentCommission;
    }

    public void setSuperAgentCommission(Float superAgentCommission) {
        this.superAgentCommission = superAgentCommission;
    }

    public Integer getCicoCommission() {
        return cicoCommission;
    }

    public void setCicoCommission(Integer cicoCommission) {
        this.cicoCommission = cicoCommission;
    }

    public Integer getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(Integer stampDuty) {
        this.stampDuty = stampDuty;
    }

}
