package com.cico.cicocashincashout.model.electricity.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MeterNumberData {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("discoCode")
    @Expose
    private String discoCode;
    @SerializedName("vendType")
    @Expose
    private String vendType;
    @SerializedName("meterNo")
    @Expose
    private String meterNo;
    @SerializedName("minVendAmount")
    @Expose
    private Integer minVendAmount;
    @SerializedName("freeUnits")
    @Expose
    private Boolean freeUnits;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("tariff")
    @Expose
    private String tariff;
    @SerializedName("daysLastVend")
    @Expose
    private Integer daysLastVend;
    @SerializedName("maxVendAmount")
    @Expose
    private Integer maxVendAmount;
    @SerializedName("responseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("gateway")
    @Expose
    private String gateway;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getDiscoCode() {
        return discoCode;
    }

    public void setDiscoCode(String discoCode) {
        this.discoCode = discoCode;
    }

    public String getVendType() {
        return vendType;
    }

    public void setVendType(String vendType) {
        this.vendType = vendType;
    }

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public Integer getMinVendAmount() {
        return minVendAmount;
    }

    public void setMinVendAmount(Integer minVendAmount) {
        this.minVendAmount = minVendAmount;
    }

    public Boolean getFreeUnits() {
        return freeUnits;
    }

    public void setFreeUnits(Boolean freeUnits) {
        this.freeUnits = freeUnits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public Integer getDaysLastVend() {
        return daysLastVend;
    }

    public void setDaysLastVend(Integer daysLastVend) {
        this.daysLastVend = daysLastVend;
    }

    public Integer getMaxVendAmount() {
        return maxVendAmount;
    }

    public void setMaxVendAmount(Integer maxVendAmount) {
        this.maxVendAmount = maxVendAmount;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
