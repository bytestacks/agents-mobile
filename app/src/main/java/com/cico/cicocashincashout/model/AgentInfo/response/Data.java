package com.cico.cicocashincashout.model.AgentInfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("agent")
    @Expose
    private Agent agent;
    @SerializedName("wallet")
    @Expose
    private Wallet wallet;
    @SerializedName("transaction")
    @Expose
    private List<Transaction> transaction = null;

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public List<Transaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<Transaction> transaction) {
        this.transaction = transaction;
    }
}
