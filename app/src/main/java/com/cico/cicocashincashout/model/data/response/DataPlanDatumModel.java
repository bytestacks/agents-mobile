package com.cico.cicocashincashout.model.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPlanDatumModel {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("databundle")
    @Expose
    private String databundle;
    @SerializedName("validity")
    @Expose
    private String validity;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDatabundle() {
        return databundle;
    }

    public void setDatabundle(String databundle) {
        this.databundle = databundle;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

}
