package com.cico.cicocashincashout.model.cashout.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InitiateCashoutRequest {

    @SerializedName("amount")
    @Expose
    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
