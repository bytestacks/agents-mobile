package com.cico.cicocashincashout.model.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPlanRequest {

    @SerializedName("telco")
    @Expose
    private String telco;

    public String getTelco() {
        return telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }
}
