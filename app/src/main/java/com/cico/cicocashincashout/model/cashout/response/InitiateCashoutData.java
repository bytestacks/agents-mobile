package com.cico.cicocashincashout.model.cashout.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InitiateCashoutData {


    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("total_amount")
    @Expose
    private Integer totalAmount;
    @SerializedName("trans_cost")
    @Expose
    private Integer transCost;
    @SerializedName("stamp_duty")
    @Expose
    private Integer stampDuty;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTransCost() {
        return transCost;
    }

    public void setTransCost(Integer transCost) {
        this.transCost = transCost;
    }

    public Integer getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(Integer stampDuty) {
        this.stampDuty = stampDuty;
    }
}
