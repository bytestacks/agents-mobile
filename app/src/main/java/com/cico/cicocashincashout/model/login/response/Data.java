package com.cico.cicocashincashout.model.login.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("agent")
    @Expose
    private Agent_ agent;
    @SerializedName("terminal")
    @Expose
    private Object terminal;
    @SerializedName("wallet")
    @Expose
    private Wallet_ wallet;
    @SerializedName("is_default")
    @Expose
    private Boolean isDefault;
    @SerializedName("settings")
    @Expose
    private Settings settings;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Agent_ getAgent() {
        return agent;
    }

    public void setAgent(Agent_ agent) {
        this.agent = agent;
    }

    public Object getTerminal() {
        return terminal;
    }

    public void setTerminal(Object terminal) {
        this.terminal = terminal;
    }

    public Wallet_ getWallet() {
        return wallet;
    }

    public void setWallet(Wallet_ wallet) {
        this.wallet = wallet;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }


}

