package com.cico.cicocashincashout.model.dstv.validate_number.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DstvValidateNumberResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DstvValidateNumberData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DstvValidateNumberData getData() {
        return data;
    }

    public void setData(DstvValidateNumberData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
