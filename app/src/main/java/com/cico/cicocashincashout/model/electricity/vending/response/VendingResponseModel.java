package com.cico.cicocashincashout.model.electricity.vending.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendingResponseModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private VendingData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VendingData getData() {
        return data;
    }

    public void setData(VendingData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
