package com.cico.cicocashincashout.model.dstv.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DstvPriceListDatum {

    @SerializedName("availablePricingOptions")
    @Expose
    private List<AvailablePricingOption> availablePricingOptions = null;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;

    public List<AvailablePricingOption> getAvailablePricingOptions() {
        return availablePricingOptions;
    }

    public void setAvailablePricingOptions(List<AvailablePricingOption> availablePricingOptions) {
        this.availablePricingOptions = availablePricingOptions;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
