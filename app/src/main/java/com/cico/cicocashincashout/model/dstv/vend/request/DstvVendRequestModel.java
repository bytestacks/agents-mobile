package com.cico.cicocashincashout.model.dstv.vend.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DstvVendRequestModel {

    @SerializedName("smartcard")
    @Expose
    private String smartcard;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("period")
    @Expose
    private Integer period;
    @SerializedName("service")
    @Expose
    private String service;

    public String getSmartcard() {
        return smartcard;
    }

    public void setSmartcard(String smartcard) {
        this.smartcard = smartcard;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

}
