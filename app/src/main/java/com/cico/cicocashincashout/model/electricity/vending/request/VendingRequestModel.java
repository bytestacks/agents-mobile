package com.cico.cicocashincashout.model.electricity.vending.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendingRequestModel {

    @SerializedName("meter_number")
    @Expose
    private String meterNumber;
    @SerializedName("disco")
    @Expose
    private String disco;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("phone")
    @Expose
    private String phone;

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getDisco() {
        return disco;
    }

    public void setDisco(String disco) {
        this.disco = disco;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
