package com.cico.cicocashincashout.model.data.vending.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataVendingRequest {

    @SerializedName("telco")
    @Expose
    private String telco;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("phone")
    @Expose
    private String phone;

    public String getTelco() {
        return telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
