package com.cico.cicocashincashout.model.AgentInfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wallet {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("agent_id")
    @Expose
    private Integer agentId;
    @SerializedName("wallet_id")
    @Expose
    private String walletId;
    @SerializedName("current_bal")
    @Expose
    private String currentBal;
    @SerializedName("previous_bal")
    @Expose
    private String previousBal;
    @SerializedName("current_comm")
    @Expose
    private float currentComm;
    @SerializedName("previous_comm")
    @Expose
    private float previousComm;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getCurrentBal() {
        return currentBal;
    }

    public void setCurrentBal(String currentBal) {
        this.currentBal = currentBal;
    }

    public String getPreviousBal() {
        return previousBal;
    }

    public void setPreviousBal(String previousBal) {
        this.previousBal = previousBal;
    }

    public float getCurrentComm() {
        return currentComm;
    }

    public void setCurrentComm(float currentComm) {
        this.currentComm = currentComm;
    }

    public float getPreviousComm() {
        return previousComm;
    }

    public void setPreviousComm(float previousComm) {
        this.previousComm = previousComm;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
