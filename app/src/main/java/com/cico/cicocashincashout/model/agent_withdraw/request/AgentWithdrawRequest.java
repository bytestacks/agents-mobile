package com.cico.cicocashincashout.model.agent_withdraw.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentWithdrawRequest {


    @SerializedName("amount")
    @Expose
    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
