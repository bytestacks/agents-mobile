package com.cico.cicocashincashout.model.wallets.wallet_logs.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletlogsDatum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("wallet_id")
    @Expose
    private String walletId;
    @SerializedName("agent_id")
    @Expose
    private String agentId;
    @SerializedName("current_bal")
    @Expose
    private Integer currentBal;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("previous_bal")
    @Expose
    private Integer previousBal;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Integer getCurrentBal() {
        return currentBal;
    }

    public void setCurrentBal(Integer currentBal) {
        this.currentBal = currentBal;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPreviousBal() {
        return previousBal;
    }

    public void setPreviousBal(Integer previousBal) {
        this.previousBal = previousBal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
