package com.cico.cicocashincashout.model.electricity.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateMeterNumberRequest {

    @SerializedName("meter_number")
    @Expose
    private String meterNumber;
    @SerializedName("disco")
    @Expose
    private String disco;
    @SerializedName("type")
    @Expose
    private String type;

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getDisco() {
        return disco;
    }

    public void setDisco(String disco) {
        this.disco = disco;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
