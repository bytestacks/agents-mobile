package com.cico.cicocashincashout.model.login.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cashout {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("commission")
    @Expose
    private Integer commission;
    @SerializedName("minimum")
    @Expose
    private Integer minimum;
    @SerializedName("maximum")
    @Expose
    private Integer maximum;
    @SerializedName("transaction_type_id")
    @Expose
    private Integer transactionTypeId;
    @SerializedName("transaction_cost")
    @Expose
    private Integer transactionCost;
    @SerializedName("transaction_type")
    @Expose
    private String transactionType;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("super_agent_commission")
    @Expose
    private Float superAgentCommission;
    @SerializedName("cico_commission")
    @Expose
    private Integer cicoCommission;
    @SerializedName("stamp_duty")
    @Expose
    private Integer stampDuty;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("type")
    @Expose
    private Object type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public Integer getMaximum() {
        return maximum;
    }

    public void setMaximum(Integer maximum) {
        this.maximum = maximum;
    }

    public Integer getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Integer transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public Integer getTransactionCost() {
        return transactionCost;
    }

    public void setTransactionCost(Integer transactionCost) {
        this.transactionCost = transactionCost;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Float getSuperAgentCommission() {
        return superAgentCommission;
    }

    public void setSuperAgentCommission(Float superAgentCommission) {
        this.superAgentCommission = superAgentCommission;
    }

    public Integer getCicoCommission() {
        return cicoCommission;
    }

    public void setCicoCommission(Integer cicoCommission) {
        this.cicoCommission = cicoCommission;
    }

    public Integer getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(Integer stampDuty) {
        this.stampDuty = stampDuty;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

}
