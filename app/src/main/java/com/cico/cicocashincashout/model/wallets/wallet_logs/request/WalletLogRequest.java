package com.cico.cicocashincashout.model.wallets.wallet_logs.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletLogRequest {

    @SerializedName("agent_id")
    @Expose
    private Integer agentId;

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }
}
