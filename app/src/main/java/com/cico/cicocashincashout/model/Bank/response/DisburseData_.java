package com.cico.cicocashincashout.model.Bank.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisburseData_ {

    @SerializedName("provider_response_code")
    @Expose
    private String providerResponseCode;
    @SerializedName("charge_status")
    @Expose
    private Object chargeStatus;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("provider_response")
    @Expose
    private Object providerResponse;
    @SerializedName("errors")
    @Expose
    private Object errors;
    @SerializedName("error")
    @Expose
    private Object error;
    @SerializedName("charge_token")
    @Expose
    private Object chargeToken;
    @SerializedName("paymentoptions")
    @Expose
    private Object paymentoptions;
    @SerializedName("client_info")
    @Expose
    private ClientInfo clientInfo;

    public String getProviderResponseCode() {
        return providerResponseCode;
    }

    public void setProviderResponseCode(String providerResponseCode) {
        this.providerResponseCode = providerResponseCode;
    }

    public Object getChargeStatus() {
        return chargeStatus;
    }

    public void setChargeStatus(Object chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Object getProviderResponse() {
        return providerResponse;
    }

    public void setProviderResponse(Object providerResponse) {
        this.providerResponse = providerResponse;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public Object getChargeToken() {
        return chargeToken;
    }

    public void setChargeToken(Object chargeToken) {
        this.chargeToken = chargeToken;
    }

    public Object getPaymentoptions() {
        return paymentoptions;
    }

    public void setPaymentoptions(Object paymentoptions) {
        this.paymentoptions = paymentoptions;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }
}
