package com.cico.cicocashincashout.model.dstv.validate_number.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusDescription {

    @SerializedName("customerNo")
    @Expose
    private Long customerNo;
    @SerializedName("accountStatus")
    @Expose
    private String accountStatus;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("customerType")
    @Expose
    private String customerType;
    @SerializedName("invoicePeriod")
    @Expose
    private Integer invoicePeriod;
    @SerializedName("dueDate")
    @Expose
    private String dueDate;

    public Long getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(Long customerNo) {
        this.customerNo = customerNo;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public Integer getInvoicePeriod() {
        return invoicePeriod;
    }

    public void setInvoicePeriod(Integer invoicePeriod) {
        this.invoicePeriod = invoicePeriod;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
}
