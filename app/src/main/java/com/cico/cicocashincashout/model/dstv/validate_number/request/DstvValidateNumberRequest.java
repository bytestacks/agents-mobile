package com.cico.cicocashincashout.model.dstv.validate_number.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DstvValidateNumberRequest {

    @SerializedName("smartcard")
    @Expose
    private String smartcard;
    @SerializedName("service")
    @Expose
    private String service;

    public String getSmartcard() {
        return smartcard;
    }

    public void setSmartcard(String smartcard) {
        this.smartcard = smartcard;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
