package com.cico.cicocashincashout.model.transaction_history.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionHistoryRequestModel {

    @SerializedName("agent_id")
    @Expose
    private Integer agentId;

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }
}
