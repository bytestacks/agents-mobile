package com.cico.cicocashincashout.model.AgentInfo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("provider_reference")
    @Expose
    private Object providerReference;
    @SerializedName("agent_id")
    @Expose
    private Integer agentId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("completed_at")
    @Expose
    private Object completedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("retrieval_reference")
    @Expose
    private Object retrievalReference;
    @SerializedName("customer_info")
    @Expose
    private String customerInfo;
    @SerializedName("short_code")
    @Expose
    private Object shortCode;
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("transaction_cost")
    @Expose
    private Integer transactionCost;
    @SerializedName("agent_commission")
    @Expose
    private Integer agentCommission;
    @SerializedName("super_agent_commission")
    @Expose
    private Float superAgentCommission;
    @SerializedName("cico_commission")
    @Expose
    private Integer cicoCommission;
    @SerializedName("stamp_duty")
    @Expose
    private Integer stampDuty;
    @SerializedName("net_amount")
    @Expose
    private Integer netAmount;
    @SerializedName("card_holder_name")
    @Expose
    private Object cardHolderName;
    @SerializedName("terminal_id")
    @Expose
    private Object terminalId;
    @SerializedName("pan_no")
    @Expose
    private Object panNo;
    @SerializedName("rrn")
    @Expose
    private Object rrn;
    @SerializedName("provider_ref")
    @Expose
    private Object providerRef;
    @SerializedName("card_no")
    @Expose
    private Object cardNo;
    @SerializedName("stan")
    @Expose
    private Object stan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Object getProviderReference() {
        return providerReference;
    }

    public void setProviderReference(Object providerReference) {
        this.providerReference = providerReference;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public Object getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Object completedAt) {
        this.completedAt = completedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getRetrievalReference() {
        return retrievalReference;
    }

    public void setRetrievalReference(Object retrievalReference) {
        this.retrievalReference = retrievalReference;
    }

    public String getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(String customerInfo) {
        this.customerInfo = customerInfo;
    }

    public Object getShortCode() {
        return shortCode;
    }

    public void setShortCode(Object shortCode) {
        this.shortCode = shortCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Integer getTransactionCost() {
        return transactionCost;
    }

    public void setTransactionCost(Integer transactionCost) {
        this.transactionCost = transactionCost;
    }

    public Integer getAgentCommission() {
        return agentCommission;
    }

    public void setAgentCommission(Integer agentCommission) {
        this.agentCommission = agentCommission;
    }

    public Float getSuperAgentCommission() {
        return superAgentCommission;
    }

    public void setSuperAgentCommission(Float superAgentCommission) {
        this.superAgentCommission = superAgentCommission;
    }

    public Integer getCicoCommission() {
        return cicoCommission;
    }

    public void setCicoCommission(Integer cicoCommission) {
        this.cicoCommission = cicoCommission;
    }

    public Integer getStampDuty() {
        return stampDuty;
    }

    public void setStampDuty(Integer stampDuty) {
        this.stampDuty = stampDuty;
    }

    public Integer getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Integer netAmount) {
        this.netAmount = netAmount;
    }

    public Object getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(Object cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public Object getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Object terminalId) {
        this.terminalId = terminalId;
    }

    public Object getPanNo() {
        return panNo;
    }

    public void setPanNo(Object panNo) {
        this.panNo = panNo;
    }

    public Object getRrn() {
        return rrn;
    }

    public void setRrn(Object rrn) {
        this.rrn = rrn;
    }

    public Object getProviderRef() {
        return providerRef;
    }

    public void setProviderRef(Object providerRef) {
        this.providerRef = providerRef;
    }

    public Object getCardNo() {
        return cardNo;
    }

    public void setCardNo(Object cardNo) {
        this.cardNo = cardNo;
    }

    public Object getStan() {
        return stan;
    }

    public void setStan(Object stan) {
        this.stan = stan;
    }

}
