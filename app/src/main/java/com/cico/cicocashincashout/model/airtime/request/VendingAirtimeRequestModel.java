package com.cico.cicocashincashout.model.airtime.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendingAirtimeRequestModel {
    @SerializedName("telco")
    @Expose
    private String telco;

    @SerializedName("amount")
    @Expose
    private float amount;
    @SerializedName("phone")
    @Expose
    private String phone;


    // Getter Methods

    public String getTelco() {
        return telco;
    }

    public float getAmount() {
        return amount;
    }

    public String getPhone() {
        return phone;
    }

    // Setter Methods

    public void setTelco( String telco ) {
        this.telco = telco;
    }

    public void setAmount( float amount ) {
        this.amount = amount;
    }

    public void setPhone( String phone ) {
        this.phone = phone;
    }
}
