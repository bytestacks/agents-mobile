package com.cico.cicocashincashout.model.fund_wallet.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FundWalletResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private FundWalletData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FundWalletData getData() {
        return data;
    }

    public void setData(FundWalletData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
