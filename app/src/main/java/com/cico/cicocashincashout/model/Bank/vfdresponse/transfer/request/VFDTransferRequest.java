package com.cico.cicocashincashout.model.Bank.vfdresponse.transfer.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VFDTransferRequest {

    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("amount")
    @Expose
    private String amount;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
