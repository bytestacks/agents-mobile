package com.cico.cicocashincashout.model.fund_wallet.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FundWalletRequest {

    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("reference")
    @Expose
    private String reference;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
