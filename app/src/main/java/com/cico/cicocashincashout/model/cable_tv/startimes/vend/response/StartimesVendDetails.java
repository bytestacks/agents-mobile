package com.cico.cicocashincashout.model.cable_tv.startimes.vend.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartimesVendDetails {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("accountId")
    @Expose
    private String accountId;
    @SerializedName("providerRef")
    @Expose
    private String providerRef;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getProviderRef() {
        return providerRef;
    }

    public void setProviderRef(String providerRef) {
        this.providerRef = providerRef;
    }

}
