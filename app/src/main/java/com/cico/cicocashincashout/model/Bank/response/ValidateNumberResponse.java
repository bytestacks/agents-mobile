package com.cico.cicocashincashout.model.Bank.response;

import com.cico.cicocashincashout.model.agent.response.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateNumberResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private BankData data;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BankData getData() {
        return data;
    }

    public void setData(BankData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
